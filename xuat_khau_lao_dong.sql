-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: database:3306
-- Generation Time: Jan 23, 2019 at 04:48 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xuat_khau_lao_dong`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Một người bình luận WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-01-05 01:42:18', '2019-01-05 01:42:18', 'Xin chào, đây là một bình luận\nĐể bắt đầu với quản trị bình luận, chỉnh sửa hoặc xóa bình luận, vui lòng truy cập vào khu vực Bình luận trong trang quản trị.\nAvatar của người bình luận sử dụng <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_customer`
--

CREATE TABLE `wp_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `product` text COLLATE utf8mb4_unicode_ci,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_customer`
--

INSERT INTO `wp_customer` (`id`, `name`, `phone`, `address`, `note`, `product`, `time`) VALUES
(7, 'Nhã đan', '0973810503', 'Khương Thượng\r\nĐống Đa', 'Không có ghi chú nào', '{\\\"237\\\":\\\"2\\\",\\\"236\\\":\\\"5\\\"}', '2019-01-06 05:02:28'),
(8, 'thiet', '0898732276', 'Khương Thượng\r\nĐống Đa', 'b', '{\\\"235\\\":1}', '2019-01-06 05:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://xuatkhaulaodong99.vn', 'yes'),
(2, 'home', 'http://xuatkhaulaodong99.vn', 'yes'),
(3, 'blogname', 'CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX', 'yes'),
(4, 'blogdescription', 'Một trang web mới sử dụng WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'nvthiet95@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:167:{s:41:\"(thong-tin-dich-vu)/(them|xoa)/([0-9]+)/?\";s:68:\"index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]\";s:41:\"(gio-hang)/(them|xoa)/([0-9]+)/([0-9]+)/?\";s:68:\"index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]\";s:32:\"(gio-hang)/(them|xoa)/([0-9]+)/?\";s:89:\"index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]&quantity=$matches[4]\";s:21:\"(gio-hang)/(xoahet)/?\";s:49:\"index.php?pagename=$matches[1]&action=$matches[2]\";s:18:\"(gio-hang)/(sua)/?\";s:49:\"index.php?pagename=$matches[1]&action=$matches[2]\";s:27:\"(thong-tin-dich-vu)/(sua)/?\";s:49:\"index.php?pagename=$matches[1]&action=$matches[2]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:26:\"group_5c301f04a3371\\.html$\";s:30:\"index.php?=group_5c301f04a3371\";s:26:\"group_5c3225ff29b10\\.html$\";s:30:\"index.php?=group_5c3225ff29b10\";s:26:\"field_5c301f13c5fd9\\.html$\";s:30:\"index.php?=field_5c301f13c5fd9\";s:26:\"field_5c301f66c5fda\\.html$\";s:30:\"index.php?=field_5c301f66c5fda\";s:26:\"field_5c301f88c5fdb\\.html$\";s:30:\"index.php?=field_5c301f88c5fdb\";s:26:\"field_5c301faac5fdc\\.html$\";s:30:\"index.php?=field_5c301faac5fdc\";s:26:\"field_5c301fc9c5fdd\\.html$\";s:30:\"index.php?=field_5c301fc9c5fdd\";s:26:\"field_5c3020eaf9b60\\.html$\";s:30:\"index.php?=field_5c3020eaf9b60\";s:26:\"field_5c3023b72b6d1\\.html$\";s:30:\"index.php?=field_5c3023b72b6d1\";s:26:\"field_5c32262e42a8b\\.html$\";s:30:\"index.php?=field_5c32262e42a8b\";s:26:\"field_5c415aab38641\\.html$\";s:30:\"index.php?=field_5c415aab38641\";s:43:\"can-tuyen-nam-lam-che-bien-thuc-pham\\.html$\";s:55:\"index.php?don_hang=can-tuyen-nam-lam-che-bien-thuc-pham\";s:40:\"can-tuyen-lao-dong-nu-lam-com-hop\\.html$\";s:52:\"index.php?don_hang=can-tuyen-lao-dong-nu-lam-com-hop\";s:35:\"can-tuyen-nu-lam-nong-nghiep\\.html$\";s:47:\"index.php?don_hang=can-tuyen-nu-lam-nong-nghiep\";s:34:\"can-tuyen-20-nu-lam-may-mac\\.html$\";s:46:\"index.php?don_hang=can-tuyen-20-nu-lam-may-mac\";s:32:\"tuyen-gap-nam-lam-banh-my\\.html$\";s:44:\"index.php?don_hang=tuyen-gap-nam-lam-banh-my\";s:41:\"can-tuyen-nu-lam-che-bien-xuc-xich\\.html$\";s:53:\"index.php?don_hang=can-tuyen-nu-lam-che-bien-xuc-xich\";s:31:\"can-tuyen-nu-lam-giat-la\\.html$\";s:43:\"index.php?don_hang=can-tuyen-nu-lam-giat-la\";s:38:\"thong-bao-cac-don-hang-nhat-ban\\.html$\";s:50:\"index.php?don_hang=thong-bao-cac-don-hang-nhat-ban\";s:14:\"banner1\\.html$\";s:24:\"index.php?banner=banner1\";s:14:\"banner2\\.html$\";s:24:\"index.php?banner=banner2\";s:67:\"lam-gi-de-co-thu-nhap-cao-khi-di-xuat-khau-lao-dong-nhat-ban\\.html$\";s:78:\"index.php?tin_tuc=lam-gi-de-co-thu-nhap-cao-khi-di-xuat-khau-lao-dong-nhat-ban\";s:81:\"bi-quyet-thi-dau-khi-tham-gia-thi-tuyen-cho-cac-ban-thuc-tap-sinh-nhat-ban\\.html$\";s:92:\"index.php?tin_tuc=bi-quyet-thi-dau-khi-tham-gia-thi-tuyen-cho-cac-ban-thuc-tap-sinh-nhat-ban\";s:54:\"muc-phi-kham-suc-khoe-khi-di-xuat-khau-lao-dong\\.html$\";s:65:\"index.php?tin_tuc=muc-phi-kham-suc-khoe-khi-di-xuat-khau-lao-dong\";s:54:\"tim-hieu-nhu-cau-to-10-000-yen-tang-manh-o-nhat\\.html$\";s:65:\"index.php?tin_tuc=tim-hieu-nhu-cau-to-10-000-yen-tang-manh-o-nhat\";s:77:\"cach-thuc-cho-cac-thuc-tap-sinh-nhat-ban-ve-nuoc-va-muon-quay-lai-nhat\\.html$\";s:88:\"index.php?tin_tuc=cach-thuc-cho-cac-thuc-tap-sinh-nhat-ban-ve-nuoc-va-muon-quay-lai-nhat\";s:61:\"ho-tro-viec-lam-cho-du-hoc-sinh-jellyfish-tai-nhat-ban\\.html$\";s:72:\"index.php?tin_tuc=ho-tro-viec-lam-cho-du-hoc-sinh-jellyfish-tai-nhat-ban\";s:52:\"(tin-moi-nhat)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:35:\"(tin-moi-nhat)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"(tin-moi-nhat)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:51:\"(tin-noi-bat)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:34:\"(tin-noi-bat)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:16:\"(tin-noi-bat)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"don_hang/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"don_hang/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"don_hang/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"don_hang/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"don_hang/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"don_hang/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"don_hang/([^/]+)/embed/?$\";s:41:\"index.php?don_hang=$matches[1]&embed=true\";s:29:\"don_hang/([^/]+)/trackback/?$\";s:35:\"index.php?don_hang=$matches[1]&tb=1\";s:37:\"don_hang/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?don_hang=$matches[1]&paged=$matches[2]\";s:44:\"don_hang/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?don_hang=$matches[1]&cpage=$matches[2]\";s:33:\"don_hang/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?don_hang=$matches[1]&page=$matches[2]\";s:25:\"don_hang/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"don_hang/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"don_hang/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"don_hang/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"don_hang/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"don_hang/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"banner/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"banner/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"banner/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"banner/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"banner/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"banner/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"banner/([^/]+)/embed/?$\";s:39:\"index.php?banner=$matches[1]&embed=true\";s:27:\"banner/([^/]+)/trackback/?$\";s:33:\"index.php?banner=$matches[1]&tb=1\";s:35:\"banner/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?banner=$matches[1]&paged=$matches[2]\";s:42:\"banner/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?banner=$matches[1]&cpage=$matches[2]\";s:31:\"banner/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?banner=$matches[1]&page=$matches[2]\";s:23:\"banner/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"banner/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"banner/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"banner/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"banner/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"banner/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"tin_tuc/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"tin_tuc/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"tin_tuc/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"tin_tuc/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"tin_tuc/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"tin_tuc/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"tin_tuc/([^/]+)/embed/?$\";s:40:\"index.php?tin_tuc=$matches[1]&embed=true\";s:28:\"tin_tuc/([^/]+)/trackback/?$\";s:34:\"index.php?tin_tuc=$matches[1]&tb=1\";s:36:\"tin_tuc/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?tin_tuc=$matches[1]&paged=$matches[2]\";s:43:\"tin_tuc/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?tin_tuc=$matches[1]&cpage=$matches[2]\";s:32:\"tin_tuc/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?tin_tuc=$matches[1]&page=$matches[2]\";s:24:\"tin_tuc/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"tin_tuc/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"tin_tuc/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"tin_tuc/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"tin_tuc/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"tin_tuc/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:2;s:31:\"customer-list/customer-list.php\";i:3;s:66:\"remove-slug-from-custom-post-type/remove-slug-custom-post-type.php\";i:4;s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";i:5;s:30:\"simple-post-gallery/plugin.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:66:\"/var/www/html/wp-content/themes/twentyseventeen-child/gio-hang.php\";i:1;s:67:\"/var/www/html/wp-content/themes/twentyseventeen-child/functions.php\";i:2;s:63:\"/var/www/html/wp-content/themes/twentyseventeen-child/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'twentyseventeen', 'yes'),
(41, 'stylesheet', 'twentyseventeen-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:47:\"participants-database/participants-database.php\";a:2:{i:0;s:8:\"PDb_Init\";i:1;s:12:\"on_uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"mfpd_settings_page\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"mfpd_settings_page\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:3:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;s:18:\"mfpd_settings_page\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'vi', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1548222138;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1548250938;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1548294151;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1548294283;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1546652559;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(117, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:62:\"https://downloads.wordpress.org/release/vi/wordpress-5.0.3.zip\";s:6:\"locale\";s:2:\"vi\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:62:\"https://downloads.wordpress.org/release/vi/wordpress-5.0.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.3\";s:7:\"version\";s:5:\"5.0.3\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.0.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.0.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.0.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.0.2\";s:7:\"version\";s:5:\"5.0.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1548216713;s:15:\"version_checked\";s:5:\"4.9.9\";s:12:\"translations\";a:0:{}}', 'no'),
(126, 'can_compress_scripts', '0', 'no'),
(141, 'current_theme', 'Theme XKLD', 'yes'),
(142, 'theme_mods_twentyseventeen-child', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(143, 'theme_switched', '', 'yes'),
(145, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(153, 'recently_activated', 'a:2:{s:36:\"contact-form-7/wp-contact-form-7.php\";i:1546740583;s:47:\"participants-database/participants-database.php\";i:1546739727;}', 'yes'),
(154, 'acf_version', '5.7.9', 'yes'),
(159, 'cptui_new_install', 'false', 'yes'),
(160, 'cptui_post_types', 'a:3:{s:8:\"don_hang\";a:29:{s:4:\"name\";s:8:\"don_hang\";s:5:\"label\";s:11:\"Đơn hàng\";s:14:\"singular_label\";s:11:\"Đơn hàng\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:1:{i:0;s:8:\"category\";}s:6:\"labels\";a:24:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";s:14:\"name_admin_bar\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:6:\"banner\";a:29:{s:4:\"name\";s:6:\"banner\";s:5:\"label\";s:6:\"Banner\";s:14:\"singular_label\";s:6:\"Banner\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:24:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";s:14:\"name_admin_bar\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:7:\"tin_tuc\";a:29:{s:4:\"name\";s:7:\"tin_tuc\";s:5:\"label\";s:9:\"Tin tức\";s:14:\"singular_label\";s:9:\"Tin tức\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:1:{i:0;s:8:\"category\";}s:6:\"labels\";a:24:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";s:14:\"name_admin_bar\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(167, 'uwt_permalink_customtype_suffix', 'html', 'yes'),
(170, 'remove_taxonomy_base_slug_settings_what_taxonomies', 'a:2:{i:0;s:8:\"category\";i:1;s:8:\"post_tag\";}', 'yes'),
(226, 'model_post_gallery', '{\"can_enqueue\":0,\"types\":[\"don_hang\",\"tin_tuc\"],\"metabox_context\":\"advanced\",\"metabox_priority\":\"default\",\"ID\":\"post_gallery\"}', 'yes'),
(312, 'pdb_admin_notices', 'a:0:{}', 'yes'),
(313, 'pdb-option_version', '9', 'yes'),
(317, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1546740387;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(357, '_site_transient_timeout_browser_ecef517c4b094d3936cd323f5e45b98b', '1548388734', 'no'),
(358, '_site_transient_browser_ecef517c4b094d3936cd323f5e45b98b', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"71.0.3578.98\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(372, 'cptui_taxonomies', 'a:0:{}', 'yes'),
(400, 'category_children', 'a:0:{}', 'yes'),
(409, '_site_transient_timeout_theme_roots', '1548218515', 'no'),
(410, '_site_transient_theme_roots', 'a:2:{s:21:\"twentyseventeen-child\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";}', 'no'),
(411, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1548216717;s:7:\"checked\";a:2:{s:21:\"twentyseventeen-child\";s:3:\"0.1\";s:15:\"twentyseventeen\";s:3:\"1.7\";}s:8:\"response\";a:1:{s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.0.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(412, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1548216719;s:7:\"checked\";a:7:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.7.9\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.6.1\";s:31:\"customer-list/customer-list.php\";s:3:\"1.0\";s:30:\"simple-post-gallery/plugin.php\";s:5:\"2.3.0\";s:66:\"remove-slug-from-custom-post-type/remove-slug-custom-post-type.php\";s:57:\"1.0.4 Author: Joakim Ling & Jon Bishop Edit hy HUNGIT.NET\";s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";s:3:\"2.1\";}s:8:\"response\";a:2:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"5.7.10\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.7.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.9\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:30:\"simple-post-gallery/plugin.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/simple-post-gallery\";s:4:\"slug\";s:19:\"simple-post-gallery\";s:6:\"plugin\";s:30:\"simple-post-gallery/plugin.php\";s:11:\"new_version\";s:5:\"2.3.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/simple-post-gallery/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/simple-post-gallery.2.3.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:72:\"https://ps.w.org/simple-post-gallery/assets/icon-256x256.png?rev=1847872\";s:2:\"1x\";s:64:\"https://ps.w.org/simple-post-gallery/assets/icon.svg?rev=1847872\";s:3:\"svg\";s:64:\"https://ps.w.org/simple-post-gallery/assets/icon.svg?rev=1847872\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/simple-post-gallery/assets/banner-1544x500.jpg?rev=1847872\";s:2:\"1x\";s:74:\"https://ps.w.org/simple-post-gallery/assets/banner-772x250.jpg?rev=1847872\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.0.3\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.6.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.6.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:66:\"remove-slug-from-custom-post-type/remove-slug-custom-post-type.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:47:\"w.org/plugins/remove-slug-from-custom-post-type\";s:4:\"slug\";s:33:\"remove-slug-from-custom-post-type\";s:6:\"plugin\";s:66:\"remove-slug-from-custom-post-type/remove-slug-custom-post-type.php\";s:11:\"new_version\";s:5:\"1.0.3\";s:3:\"url\";s:64:\"https://wordpress.org/plugins/remove-slug-from-custom-post-type/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/remove-slug-from-custom-post-type.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:77:\"https://s.w.org/plugins/geopattern-icon/remove-slug-from-custom-post-type.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/remove-taxonomy-base-slug\";s:4:\"slug\";s:25:\"remove-taxonomy-base-slug\";s:6:\"plugin\";s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/remove-taxonomy-base-slug/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/remove-taxonomy-base-slug.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:69:\"https://s.w.org/plugins/geopattern-icon/remove-taxonomy-base-slug.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 2, '_wp_trash_meta_status', 'publish'),
(4, 2, '_wp_trash_meta_time', '1546652584'),
(5, 2, '_wp_desired_post_slug', 'Trang mẫu'),
(6, 6, '_edit_last', '1'),
(7, 6, '_edit_lock', '1546652573:1'),
(8, 6, '_wp_page_template', 'category.php'),
(9, 8, '_edit_last', '1'),
(10, 8, '_edit_lock', '1546652600:1'),
(11, 8, '_wp_page_template', 'list-product.php'),
(12, 10, '_edit_last', '1'),
(13, 10, '_edit_lock', '1546652619:1'),
(14, 10, '_wp_page_template', 'category.php'),
(15, 12, '_edit_last', '1'),
(16, 12, '_edit_lock', '1546652634:1'),
(17, 12, '_wp_page_template', 'category.php'),
(54, 18, '_edit_last', '1'),
(55, 18, '_edit_lock', '1546652683:1'),
(56, 18, '_wp_page_template', 'category.php'),
(57, 20, '_menu_item_type', 'post_type'),
(58, 20, '_menu_item_menu_item_parent', '0'),
(59, 20, '_menu_item_object_id', '18'),
(60, 20, '_menu_item_object', 'page'),
(61, 20, '_menu_item_target', ''),
(62, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(63, 20, '_menu_item_xfn', ''),
(64, 20, '_menu_item_url', ''),
(65, 20, '_menu_item_orphaned', '1546652841'),
(66, 21, '_menu_item_type', 'post_type'),
(67, 21, '_menu_item_menu_item_parent', '0'),
(68, 21, '_menu_item_object_id', '12'),
(69, 21, '_menu_item_object', 'page'),
(70, 21, '_menu_item_target', ''),
(71, 21, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(72, 21, '_menu_item_xfn', ''),
(73, 21, '_menu_item_url', ''),
(74, 21, '_menu_item_orphaned', '1546652842'),
(75, 22, '_menu_item_type', 'post_type'),
(76, 22, '_menu_item_menu_item_parent', '0'),
(77, 22, '_menu_item_object_id', '10'),
(78, 22, '_menu_item_object', 'page'),
(79, 22, '_menu_item_target', ''),
(80, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(81, 22, '_menu_item_xfn', ''),
(82, 22, '_menu_item_url', ''),
(83, 22, '_menu_item_orphaned', '1546652842'),
(84, 23, '_menu_item_type', 'post_type'),
(85, 23, '_menu_item_menu_item_parent', '0'),
(86, 23, '_menu_item_object_id', '8'),
(87, 23, '_menu_item_object', 'page'),
(88, 23, '_menu_item_target', ''),
(89, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(90, 23, '_menu_item_xfn', ''),
(91, 23, '_menu_item_url', ''),
(92, 23, '_menu_item_orphaned', '1546652842'),
(93, 24, '_menu_item_type', 'post_type'),
(94, 24, '_menu_item_menu_item_parent', '0'),
(95, 24, '_menu_item_object_id', '6'),
(96, 24, '_menu_item_object', 'page'),
(97, 24, '_menu_item_target', ''),
(98, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(99, 24, '_menu_item_xfn', ''),
(100, 24, '_menu_item_url', ''),
(101, 24, '_menu_item_orphaned', '1546652842'),
(135, 101, '_edit_last', '1'),
(136, 101, '_edit_lock', '1547786881:1'),
(137, 109, '_wp_attached_file', '2019/01/2.jpg'),
(138, 109, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:960;s:4:\"file\";s:13:\"2019/01/2.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"2-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(139, 110, '_wp_attached_file', '2019/01/3.jpg'),
(140, 110, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1130;s:6:\"height\";i:849;s:4:\"file\";s:13:\"2019/01/3.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:13:\"3-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:14:\"3-1024x769.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:769;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(141, 111, '_wp_attached_file', '2019/01/4.jpg'),
(142, 111, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:720;s:4:\"file\";s:13:\"2019/01/4.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"4-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(143, 112, '_wp_attached_file', '2019/01/5.jpg'),
(144, 112, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:552;s:6:\"height\";i:690;s:4:\"file\";s:13:\"2019/01/5.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"5-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"5-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(145, 113, '_wp_attached_file', '2019/01/6.jpg'),
(146, 113, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:552;s:6:\"height\";i:828;s:4:\"file\";s:13:\"2019/01/6.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"6-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(147, 114, '_wp_attached_file', '2019/01/7.jpg'),
(148, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:720;s:4:\"file\";s:13:\"2019/01/7.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"7-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(149, 115, '_wp_attached_file', '2019/01/8.jpg'),
(150, 115, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:532;s:4:\"file\";s:13:\"2019/01/8.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"8-300x296.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:296;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"8-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(151, 116, '_wp_attached_file', '2019/01/9.jpg'),
(152, 116, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:192;s:6:\"height\";i:240;s:4:\"file\";s:13:\"2019/01/9.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"9-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:13:\"9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(153, 117, '_wp_attached_file', '2019/01/10.jpg'),
(154, 117, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:772;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/10.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"10-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"10-241x300.jpg\";s:5:\"width\";i:241;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"10-768x955.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:955;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"10-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(155, 118, '_wp_attached_file', '2019/01/11.jpg'),
(156, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1064;s:6:\"height\";i:1891;s:4:\"file\";s:14:\"2019/01/11.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"11-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"11-169x300.jpg\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"11-768x1365.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"11-576x1024.jpg\";s:5:\"width\";i:576;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"11-1064x1200.jpg\";s:5:\"width\";i:1064;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(157, 119, '_wp_attached_file', '2019/01/12.jpg'),
(158, 119, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/12.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"12-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"12-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"12-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(159, 120, '_wp_attached_file', '2019/01/13.jpg'),
(160, 120, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:426;s:4:\"file\";s:14:\"2019/01/13.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"13-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"13-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"13-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(161, 121, '_wp_attached_file', '2019/01/14.jpg'),
(162, 121, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:1350;s:4:\"file\";s:14:\"2019/01/14.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"14-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"14-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"14-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"14-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"14-1080x1200.jpg\";s:5:\"width\";i:1080;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"14-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(163, 122, '_wp_attached_file', '2019/01/15.jpg'),
(164, 122, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1638;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/15.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"15-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"15-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"15-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"15-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"15-1638x1200.jpg\";s:5:\"width\";i:1638;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"15-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 123, '_wp_attached_file', '2019/01/16.jpg'),
(166, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1172;s:6:\"height\";i:1465;s:4:\"file\";s:14:\"2019/01/16.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"16-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"16-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"16-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"16-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"16-1172x1200.jpg\";s:5:\"width\";i:1172;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"16-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(167, 124, '_wp_attached_file', '2019/01/17.jpg'),
(168, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/17.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"17-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"17-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"17-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"17-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(169, 125, '_wp_attached_file', '2019/01/18.jpg'),
(170, 125, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:721;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/18.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"18-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"18-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"18-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 126, '_wp_attached_file', '2019/01/19.jpg'),
(172, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:657;s:4:\"file\";s:14:\"2019/01/19.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"19-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"19-247x300.jpg\";s:5:\"width\";i:247;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"19-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(173, 127, '_wp_attached_file', '2019/01/20.jpg'),
(174, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/20.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"20-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"20-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"20-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(175, 128, '_wp_attached_file', '2019/01/21.jpg'),
(176, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/21.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"21-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"21-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"21-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(177, 129, '_wp_attached_file', '2019/01/22.jpg'),
(178, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/22.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"22-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"22-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"22-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(179, 130, '_wp_attached_file', '2019/01/23.jpg'),
(180, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:710;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/23.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"23-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"23-222x300.jpg\";s:5:\"width\";i:222;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"23-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(181, 131, '_wp_attached_file', '2019/01/24.jpg'),
(182, 131, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1538;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/24.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"24-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"24-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"24-768x1023.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1023;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"24-769x1024.jpg\";s:5:\"width\";i:769;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"24-1538x1200.jpg\";s:5:\"width\";i:1538;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"24-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(183, 132, '_wp_attached_file', '2019/01/25.jpg'),
(184, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1538;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/25.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"25-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"25-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"25-768x1023.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1023;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"25-769x1024.jpg\";s:5:\"width\";i:769;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"25-1538x1200.jpg\";s:5:\"width\";i:1538;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"25-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(185, 133, '_wp_attached_file', '2019/01/26.jpg'),
(186, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1538;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/26.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"26-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"26-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"26-768x1023.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1023;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"26-769x1024.jpg\";s:5:\"width\";i:769;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"26-1538x1200.jpg\";s:5:\"width\";i:1538;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"26-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(187, 134, '_wp_attached_file', '2019/01/27.jpg'),
(188, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/27.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"27-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"27-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"27-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"27-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(189, 135, '_wp_attached_file', '2019/01/28.jpg'),
(190, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:615;s:4:\"file\";s:14:\"2019/01/28.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"28-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"28-300x288.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"28-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(191, 136, '_wp_attached_file', '2019/01/29.jpg'),
(192, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:563;s:6:\"height\";i:561;s:4:\"file\";s:14:\"2019/01/29.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"29-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"29-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"29-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(193, 137, '_wp_attached_file', '2019/01/30.jpg'),
(194, 137, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/30.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"30-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"30-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"30-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"30-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(195, 138, '_wp_attached_file', '2019/01/31.jpg'),
(196, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/31.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"31-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"31-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"31-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"31-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(197, 139, '_wp_attached_file', '2019/01/32.jpg'),
(198, 139, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/32.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"32-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"32-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"32-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(199, 140, '_wp_attached_file', '2019/01/33.jpg'),
(200, 140, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:952;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/33.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"33-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"33-298x300.jpg\";s:5:\"width\";i:298;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"33-768x774.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:774;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"33-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(201, 141, '_wp_attached_file', '2019/01/34.jpg'),
(202, 141, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1536;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/34.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"34-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"34-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"34-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"34-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"34-1536x1200.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"34-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(203, 142, '_wp_attached_file', '2019/01/35.jpg'),
(204, 142, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:800;s:4:\"file\";s:14:\"2019/01/35.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"35-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"35-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"35-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 143, '_wp_attached_file', '2019/01/36.jpg'),
(206, 143, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1120;s:6:\"height\";i:1680;s:4:\"file\";s:14:\"2019/01/36.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"36-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"36-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"36-768x1152.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"36-683x1024.jpg\";s:5:\"width\";i:683;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"36-1120x1200.jpg\";s:5:\"width\";i:1120;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"36-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(207, 144, '_wp_attached_file', '2019/01/37.jpg'),
(208, 144, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:824;s:4:\"file\";s:14:\"2019/01/37.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"37-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"37-233x300.jpg\";s:5:\"width\";i:233;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"37-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(209, 145, '_wp_attached_file', '2019/01/38.jpg'),
(210, 145, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1552;s:4:\"file\";s:14:\"2019/01/38.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"38-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"38-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"38-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"38-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"38-1242x1200.jpg\";s:5:\"width\";i:1242;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"38-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(211, 146, '_wp_attached_file', '2019/01/39.jpg'),
(212, 146, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:552;s:6:\"height\";i:690;s:4:\"file\";s:14:\"2019/01/39.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"39-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"39-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"39-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 147, '_wp_attached_file', '2019/01/40.jpg'),
(214, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/40.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"40-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"40-169x300.jpg\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"40-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(215, 148, '_wp_attached_file', '2019/01/41.jpg'),
(216, 148, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/41.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"41-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"41-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"41-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 149, '_wp_attached_file', '2019/01/42.jpg'),
(218, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:721;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/42.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"42-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"42-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"42-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(219, 150, '_wp_attached_file', '2019/01/43.jpg'),
(220, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:657;s:4:\"file\";s:14:\"2019/01/43.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"43-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"43-247x300.jpg\";s:5:\"width\";i:247;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"43-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(221, 151, '_wp_attached_file', '2019/01/44.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(222, 151, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/44.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"44-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"44-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"44-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 152, '_wp_attached_file', '2019/01/45.jpg'),
(224, 152, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/45.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"45-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"45-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"45-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"45-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(225, 153, '_wp_attached_file', '2019/01/46.jpg'),
(226, 153, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/46.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"46-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"46-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"46-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(227, 154, '_wp_attached_file', '2019/01/47.jpg'),
(228, 154, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/47.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"47-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"47-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"47-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 155, '_wp_attached_file', '2019/01/48.jpg'),
(230, 155, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1081;s:6:\"height\";i:720;s:4:\"file\";s:14:\"2019/01/48.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"48-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"48-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"48-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"48-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"48-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(231, 156, '_wp_attached_file', '2019/01/49.jpg'),
(232, 156, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:843;s:6:\"height\";i:1124;s:4:\"file\";s:14:\"2019/01/49.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"49-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"49-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"49-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"49-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"49-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(233, 157, '_wp_attached_file', '2019/01/50.jpg'),
(234, 157, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/50.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"50-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"50-169x300.jpg\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"50-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(235, 158, '_wp_attached_file', '2019/01/51.jpg'),
(236, 158, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:526;s:4:\"file\";s:14:\"2019/01/51.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"51-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"51-228x300.jpg\";s:5:\"width\";i:228;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"51-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(237, 159, '_wp_attached_file', '2019/01/52.jpg'),
(238, 159, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:776;s:4:\"file\";s:14:\"2019/01/52.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"52-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"52-255x300.jpg\";s:5:\"width\";i:255;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"52-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(239, 160, '_wp_attached_file', '2019/01/53.jpg'),
(240, 160, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:793;s:4:\"file\";s:14:\"2019/01/53.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"53-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"53-250x300.jpg\";s:5:\"width\";i:250;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"53-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(241, 161, '_wp_attached_file', '2019/01/54.jpg'),
(242, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:664;s:4:\"file\";s:14:\"2019/01/54.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"54-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"54-244x300.jpg\";s:5:\"width\";i:244;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"54-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(243, 162, '_wp_attached_file', '2019/01/55.jpg'),
(244, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/55.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"55-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"55-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"55-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"55-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(245, 163, '_wp_attached_file', '2019/01/56.jpg'),
(246, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:650;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/56.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"56-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"56-203x300.jpg\";s:5:\"width\";i:203;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"56-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(247, 164, '_wp_attached_file', '2019/01/57.jpg'),
(248, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:737;s:4:\"file\";s:14:\"2019/01/57.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"57-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"57-300x295.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"57-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(249, 165, '_wp_attached_file', '2019/01/58.jpg'),
(250, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:941;s:4:\"file\";s:14:\"2019/01/58.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"58-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"58-300x294.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:294;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"58-768x753.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:753;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"58-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(251, 166, '_wp_attached_file', '2019/01/59.jpg'),
(252, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:720;s:4:\"file\";s:14:\"2019/01/59.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"59-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"59-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"59-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(253, 167, '_wp_attached_file', '2019/01/60.jpg'),
(254, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:752;s:4:\"file\";s:14:\"2019/01/60.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"60-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"60-263x300.jpg\";s:5:\"width\";i:263;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"60-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(255, 168, '_wp_attached_file', '2019/01/61.jpg'),
(256, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:825;s:4:\"file\";s:14:\"2019/01/61.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"61-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"61-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"61-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(257, 169, '_wp_attached_file', '2019/01/62.jpg'),
(258, 169, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:825;s:4:\"file\";s:14:\"2019/01/62.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"62-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"62-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"62-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(259, 170, '_wp_attached_file', '2019/01/63.jpg'),
(260, 170, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:825;s:4:\"file\";s:14:\"2019/01/63.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"63-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"63-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"63-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(261, 171, '_wp_attached_file', '2019/01/64.jpg'),
(262, 171, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:794;s:4:\"file\";s:14:\"2019/01/64.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"64-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"64-249x300.jpg\";s:5:\"width\";i:249;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"64-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(263, 172, '_wp_attached_file', '2019/01/65.jpg'),
(264, 172, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/65.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"65-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"65-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"65-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(265, 173, '_wp_attached_file', '2019/01/66.jpg'),
(266, 173, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/66.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"66-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"66-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"66-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(267, 174, '_wp_attached_file', '2019/01/67.jpg'),
(268, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/67.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"67-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"67-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"67-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(269, 175, '_wp_attached_file', '2019/01/68.jpg'),
(270, 175, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/68.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"68-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"68-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"68-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(271, 176, '_wp_attached_file', '2019/01/69.jpg'),
(272, 176, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:768;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/69.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"69-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"69-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"69-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"69-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(273, 177, '_wp_attached_file', '2019/01/70.jpg'),
(274, 177, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:720;s:4:\"file\";s:14:\"2019/01/70.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"70-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"70-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"70-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(275, 178, '_wp_attached_file', '2019/01/71.jpg'),
(276, 178, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:533;s:4:\"file\";s:14:\"2019/01/71.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"71-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"71-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"71-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(277, 179, '_wp_attached_file', '2019/01/72.jpg'),
(278, 179, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/72.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"72-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"72-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"72-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"72-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(279, 180, '_wp_attached_file', '2019/01/73.jpg'),
(280, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:640;s:4:\"file\";s:14:\"2019/01/73.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"73-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"73-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"73-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(281, 181, '_wp_attached_file', '2019/01/74.jpg'),
(282, 181, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1539;s:6:\"height\";i:2048;s:4:\"file\";s:14:\"2019/01/74.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"74-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"74-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"74-768x1022.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1022;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"74-770x1024.jpg\";s:5:\"width\";i:770;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"74-1539x1200.jpg\";s:5:\"width\";i:1539;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"74-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(283, 182, '_wp_attached_file', '2019/01/75.jpg'),
(284, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/75.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"75-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"75-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"75-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(285, 183, '_wp_attached_file', '2019/01/76.jpg'),
(286, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1395;s:6:\"height\";i:709;s:4:\"file\";s:14:\"2019/01/76.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"76-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"76-300x152.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:152;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"76-768x390.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:390;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"76-1024x520.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:520;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"76-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(287, 184, '_wp_attached_file', '2019/01/77.jpg'),
(288, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:540;s:6:\"height\";i:960;s:4:\"file\";s:14:\"2019/01/77.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"77-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"77-169x300.jpg\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"77-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(289, 185, '_wp_attached_file', '2019/01/78.jpg'),
(290, 185, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:1350;s:4:\"file\";s:14:\"2019/01/78.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"78-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"78-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"78-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"78-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"78-1080x1200.jpg\";s:5:\"width\";i:1080;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"78-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(291, 186, '_wp_attached_file', '2019/01/79.jpg'),
(292, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:1350;s:4:\"file\";s:14:\"2019/01/79.jpg\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"79-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"79-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"79-768x960.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:15:\"79-819x1024.jpg\";s:5:\"width\";i:819;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:16:\"79-1080x1200.jpg\";s:5:\"width\";i:1080;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"79-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(293, 187, '_wp_attached_file', '2019/01/80.jpg'),
(294, 187, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:482;s:6:\"height\";i:558;s:4:\"file\";s:14:\"2019/01/80.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"80-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"80-259x300.jpg\";s:5:\"width\";i:259;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"80-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:5:\"Hieup\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(295, 188, '_wp_attached_file', '2019/01/81.jpg'),
(296, 188, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:660;s:6:\"height\";i:825;s:4:\"file\";s:14:\"2019/01/81.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"81-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"81-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"81-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(297, 189, '_wp_attached_file', '2019/01/82.jpg'),
(298, 189, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:875;s:4:\"file\";s:14:\"2019/01/82.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"82-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"82-240x300.jpg\";s:5:\"width\";i:240;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:14:\"82-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(301, 207, '_edit_last', '1'),
(302, 207, '_edit_lock', '1546662741:1'),
(303, 207, 'ma_don_hang', 'NTD001'),
(304, 207, '_ma_don_hang', 'field_5c301f13c5fd9'),
(305, 207, 'luong', '29 triêu'),
(306, 207, '_luong', 'field_5c301f66c5fda'),
(307, 207, 'noi_lam_viec', 'Osaka'),
(308, 207, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(309, 207, 'tinh_trang', 'Có sẵn đơn'),
(310, 207, '_tinh_trang', 'field_5c301faac5fdc'),
(311, 207, 'so_dien_thoai_tu_van', '0898732276'),
(312, 207, '_so_dien_thoai_tu_van', 'field_5c301fc9c5fdd'),
(313, 207, 'gioi_tinh', '30 nam, 20 nữ'),
(314, 207, '_gioi_tinh', 'field_5c3020eaf9b60'),
(315, 207, 'thoi_gian_tuyen', '20190201'),
(316, 207, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(317, 207, '_gallery', 'a:5:{i:0;s:3:\"188\";i:1;s:3:\"189\";i:2;s:3:\"187\";i:3;s:3:\"186\";i:4;s:3:\"184\";}'),
(318, 207, '_gallery_format', 'default'),
(319, 207, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(320, 207, '_thumbnail_id', '188'),
(321, 207, 'so_dien_thoai', '0898732276'),
(322, 207, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(323, 223, '_wp_attached_file', '2019/01/banner-xuat-khau-lao-dong-nhat-ban1.jpg'),
(324, 223, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:380;s:4:\"file\";s:47:\"2019/01/banner-xuat-khau-lao-dong-nhat-ban1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"banner-xuat-khau-lao-dong-nhat-ban1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"banner-xuat-khau-lao-dong-nhat-ban1-300x83.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:83;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:47:\"banner-xuat-khau-lao-dong-nhat-ban1-768x214.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:48:\"banner-xuat-khau-lao-dong-nhat-ban1-1024x285.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:47:\"banner-xuat-khau-lao-dong-nhat-ban1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(325, 224, '_wp_attached_file', '2019/01/banner-xuat-khau-lao-dong.jpg'),
(326, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:380;s:4:\"file\";s:37:\"2019/01/banner-xuat-khau-lao-dong.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"banner-xuat-khau-lao-dong-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"banner-xuat-khau-lao-dong-300x83.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:83;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"banner-xuat-khau-lao-dong-768x214.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"banner-xuat-khau-lao-dong-1024x285.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:285;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:37:\"banner-xuat-khau-lao-dong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(327, 222, '_edit_last', '1'),
(328, 222, '_thumbnail_id', '224'),
(329, 222, '_edit_lock', '1546790566:1'),
(330, 225, '_edit_last', '1'),
(331, 225, '_edit_lock', '1546792927:1'),
(332, 225, '_thumbnail_id', '223'),
(333, 226, '_wp_attached_file', '2019/01/back-top.png'),
(334, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:32;s:6:\"height\";i:32;s:4:\"file\";s:20:\"2019/01/back-top.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(341, 227, '_edit_last', '1'),
(342, 227, '_thumbnail_id', '186'),
(343, 227, 'ma_don_hang', 'NTD001'),
(344, 227, '_ma_don_hang', 'field_5c301f13c5fd9'),
(345, 227, 'luong', '29 triêu'),
(346, 227, '_luong', 'field_5c301f66c5fda'),
(347, 227, 'noi_lam_viec', 'Osaka'),
(348, 227, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(349, 227, 'tinh_trang', 'Có sẵn đơn'),
(350, 227, '_tinh_trang', 'field_5c301faac5fdc'),
(351, 227, 'so_dien_thoai', '0898732276'),
(352, 227, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(353, 227, 'gioi_tinh', '30 nam, 20 nữ'),
(354, 227, '_gioi_tinh', 'field_5c3020eaf9b60'),
(355, 227, 'thoi_gian_tuyen', '20190105'),
(356, 227, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(357, 227, '_gallery', 'a:2:{i:0;s:3:\"181\";i:1;s:3:\"179\";}'),
(358, 227, '_gallery_format', 'default'),
(359, 227, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(360, 227, '_edit_lock', '1546664086:1'),
(361, 228, '_edit_last', '1'),
(362, 228, '_thumbnail_id', '182'),
(363, 228, 'ma_don_hang', 'NTD001'),
(364, 228, '_ma_don_hang', 'field_5c301f13c5fd9'),
(365, 228, 'luong', '29 triêu'),
(366, 228, '_luong', 'field_5c301f66c5fda'),
(367, 228, 'noi_lam_viec', 'Osaka'),
(368, 228, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(369, 228, 'tinh_trang', 'Có sẵn đơn'),
(370, 228, '_tinh_trang', 'field_5c301faac5fdc'),
(371, 228, 'so_dien_thoai', '0898732276'),
(372, 228, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(373, 228, 'gioi_tinh', '30 nam, 20 nữ'),
(374, 228, '_gioi_tinh', 'field_5c3020eaf9b60'),
(375, 228, 'thoi_gian_tuyen', '20190105'),
(376, 228, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(377, 228, '_gallery', 'a:0:{}'),
(378, 228, '_gallery_format', 'default'),
(379, 228, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(380, 228, '_edit_lock', '1546664147:1'),
(381, 229, '_edit_last', '1'),
(382, 229, '_edit_lock', '1546674401:1'),
(383, 229, '_thumbnail_id', '180'),
(384, 229, 'ma_don_hang', 'NTD001'),
(385, 229, '_ma_don_hang', 'field_5c301f13c5fd9'),
(386, 229, 'luong', '29 triệu'),
(387, 229, '_luong', 'field_5c301f66c5fda'),
(388, 229, 'noi_lam_viec', 'Osaka'),
(389, 229, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(390, 229, 'tinh_trang', 'Có sẵn đơn'),
(391, 229, '_tinh_trang', 'field_5c301faac5fdc'),
(392, 229, 'so_dien_thoai', '0898732276'),
(393, 229, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(394, 229, 'gioi_tinh', '30 nam, 20 nữ'),
(395, 229, '_gioi_tinh', 'field_5c3020eaf9b60'),
(396, 229, 'thoi_gian_tuyen', '20190105'),
(397, 229, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(398, 229, '_gallery', 'a:0:{}'),
(399, 229, '_gallery_format', 'default'),
(400, 229, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(401, 230, '_edit_last', '1'),
(402, 230, '_edit_lock', '1546674380:1'),
(403, 230, '_thumbnail_id', '175'),
(404, 230, 'ma_don_hang', 'NTD001'),
(405, 230, '_ma_don_hang', 'field_5c301f13c5fd9'),
(406, 230, 'luong', '29 triệu'),
(407, 230, '_luong', 'field_5c301f66c5fda'),
(408, 230, 'noi_lam_viec', 'Osaka'),
(409, 230, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(410, 230, 'tinh_trang', 'Có sẵn đơn'),
(411, 230, '_tinh_trang', 'field_5c301faac5fdc'),
(412, 230, 'so_dien_thoai', '0898732276'),
(413, 230, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(414, 230, 'gioi_tinh', '30 nam, 20 nữ'),
(415, 230, '_gioi_tinh', 'field_5c3020eaf9b60'),
(416, 230, 'thoi_gian_tuyen', '20190112'),
(417, 230, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(418, 230, '_gallery', 'a:0:{}'),
(419, 230, '_gallery_format', 'default'),
(420, 230, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(421, 235, '_edit_last', '1'),
(422, 235, '_edit_lock', '1546674491:1'),
(423, 235, '_thumbnail_id', '177'),
(424, 235, 'ma_don_hang', 'NTD001'),
(425, 235, '_ma_don_hang', 'field_5c301f13c5fd9'),
(426, 235, 'luong', '29 triệu'),
(427, 235, '_luong', 'field_5c301f66c5fda'),
(428, 235, 'noi_lam_viec', 'Osaka');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(429, 235, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(430, 235, 'tinh_trang', 'Có sẵn đơn'),
(431, 235, '_tinh_trang', 'field_5c301faac5fdc'),
(432, 235, 'so_dien_thoai', '0898732276'),
(433, 235, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(434, 235, 'gioi_tinh', '30 nam, 20 nữ'),
(435, 235, '_gioi_tinh', 'field_5c3020eaf9b60'),
(436, 235, 'thoi_gian_tuyen', '20190105'),
(437, 235, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(438, 235, '_gallery', 'a:0:{}'),
(439, 235, '_gallery_format', 'default'),
(440, 235, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(441, 236, '_edit_last', '1'),
(442, 236, '_edit_lock', '1546674540:1'),
(443, 236, '_thumbnail_id', '176'),
(444, 236, 'ma_don_hang', 'NTD001'),
(445, 236, '_ma_don_hang', 'field_5c301f13c5fd9'),
(446, 236, 'luong', '29 triệu'),
(447, 236, '_luong', 'field_5c301f66c5fda'),
(448, 236, 'noi_lam_viec', 'Osaka'),
(449, 236, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(450, 236, 'tinh_trang', 'Có sẵn đơn'),
(451, 236, '_tinh_trang', 'field_5c301faac5fdc'),
(452, 236, 'so_dien_thoai', '0898732276'),
(453, 236, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(454, 236, 'gioi_tinh', '30 nam, 20 nữ'),
(455, 236, '_gioi_tinh', 'field_5c3020eaf9b60'),
(456, 236, 'thoi_gian_tuyen', '20190105'),
(457, 236, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(458, 236, '_gallery', 'a:0:{}'),
(459, 236, '_gallery_format', 'default'),
(460, 236, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(461, 237, '_edit_last', '1'),
(462, 237, '_thumbnail_id', '172'),
(463, 237, 'ma_don_hang', 'NTD001'),
(464, 237, '_ma_don_hang', 'field_5c301f13c5fd9'),
(465, 237, 'luong', '29 triệu'),
(466, 237, '_luong', 'field_5c301f66c5fda'),
(467, 237, 'noi_lam_viec', 'Osaka'),
(468, 237, '_noi_lam_viec', 'field_5c301f88c5fdb'),
(469, 237, 'tinh_trang', 'Có sẵn đơn'),
(470, 237, '_tinh_trang', 'field_5c301faac5fdc'),
(471, 237, 'so_dien_thoai', '0898732276'),
(472, 237, '_so_dien_thoai', 'field_5c301fc9c5fdd'),
(473, 237, 'gioi_tinh', '30 nam, 20 nữ'),
(474, 237, '_gioi_tinh', 'field_5c3020eaf9b60'),
(475, 237, 'thoi_gian_tuyen', '20190105'),
(476, 237, '_thoi_gian_tuyen', 'field_5c3023b72b6d1'),
(477, 237, '_gallery', 'a:0:{}'),
(478, 237, '_gallery_format', 'default'),
(479, 237, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(480, 237, '_edit_lock', '1546681186:1'),
(485, 239, '_edit_last', '1'),
(486, 239, '_edit_lock', '1546674918:1'),
(487, 239, '_thumbnail_id', '170'),
(488, 239, '_gallery', 'a:0:{}'),
(489, 239, '_gallery_format', 'default'),
(490, 239, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(491, 240, '_edit_last', '1'),
(492, 240, '_thumbnail_id', '171'),
(493, 240, '_gallery', 'a:0:{}'),
(494, 240, '_gallery_format', 'default'),
(495, 240, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(496, 240, '_edit_lock', '1547861073:1'),
(497, 241, '_edit_last', '1'),
(498, 241, '_edit_lock', '1547861064:1'),
(499, 241, '_thumbnail_id', '168'),
(500, 241, '_gallery', 'a:0:{}'),
(501, 241, '_gallery_format', 'default'),
(502, 241, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(503, 242, '_edit_last', '1'),
(504, 242, '_thumbnail_id', '166'),
(505, 242, '_gallery', 'a:0:{}'),
(506, 242, '_gallery_format', 'default'),
(507, 242, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(508, 242, '_edit_lock', '1547861054:1'),
(509, 243, '_edit_last', '1'),
(510, 243, '_edit_lock', '1547861047:1'),
(511, 243, '_thumbnail_id', '167'),
(512, 243, '_gallery', 'a:0:{}'),
(513, 243, '_gallery_format', 'default'),
(514, 243, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(515, 244, '_edit_last', '1'),
(516, 244, '_thumbnail_id', '164'),
(517, 244, '_gallery', 'a:0:{}'),
(518, 244, '_gallery_format', 'default'),
(519, 244, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(520, 244, '_edit_lock', '1547861036:1'),
(524, 245, '_edit_last', '1'),
(525, 245, '_edit_lock', '1546682622:1'),
(526, 245, '_wp_page_template', 'thong-tin-dich-vu.php'),
(527, 248, '_edit_last', '1'),
(528, 248, '_wp_page_template', 'category.php'),
(529, 248, '_edit_lock', '1546679743:1'),
(530, 250, '_edit_last', '1'),
(531, 250, '_edit_lock', '1546679770:1'),
(532, 250, '_wp_page_template', 'category.php'),
(533, 252, '_edit_last', '1'),
(534, 252, '_wp_page_template', 'category.php'),
(535, 252, '_edit_lock', '1546679794:1'),
(536, 254, '_edit_last', '1'),
(537, 254, '_wp_page_template', 'category.php'),
(538, 254, '_edit_lock', '1546679808:1'),
(539, 256, '_edit_last', '1'),
(540, 256, '_wp_page_template', 'category.php'),
(541, 256, '_edit_lock', '1546679823:1'),
(542, 258, '_edit_last', '1'),
(543, 258, '_edit_lock', '1546679841:1'),
(544, 258, '_wp_page_template', 'category.php'),
(545, 260, '_edit_last', '1'),
(546, 260, '_wp_page_template', 'category.php'),
(547, 260, '_edit_lock', '1546679858:1'),
(548, 262, '_edit_last', '1'),
(549, 262, '_wp_page_template', 'category.php'),
(550, 262, '_edit_lock', '1546679870:1'),
(551, 264, '_edit_last', '1'),
(552, 264, '_wp_page_template', 'category.php'),
(553, 264, '_edit_lock', '1546679883:1'),
(554, 266, '_edit_last', '1'),
(555, 266, '_wp_page_template', 'category.php'),
(556, 266, '_edit_lock', '1546679895:1'),
(557, 268, '_edit_last', '1'),
(558, 268, '_wp_page_template', 'category.php'),
(559, 268, '_edit_lock', '1546679913:1'),
(560, 270, '_edit_last', '1'),
(561, 270, '_edit_lock', '1546679931:1'),
(562, 270, '_wp_page_template', 'category.php'),
(563, 272, '_edit_last', '1'),
(564, 272, '_wp_page_template', 'category.php'),
(565, 272, '_edit_lock', '1546679947:1'),
(566, 274, '_edit_last', '1'),
(567, 274, '_wp_page_template', 'category.php'),
(568, 274, '_edit_lock', '1546679966:1'),
(569, 276, '_edit_last', '1'),
(570, 276, '_wp_page_template', 'category.php'),
(571, 276, '_edit_lock', '1546679981:1'),
(572, 278, '_edit_last', '1'),
(573, 278, '_wp_page_template', 'category.php'),
(574, 278, '_edit_lock', '1546679995:1'),
(575, 280, '_edit_last', '1'),
(576, 280, '_wp_page_template', 'category.php'),
(577, 280, '_edit_lock', '1546680019:1'),
(578, 282, '_edit_last', '1'),
(579, 282, '_edit_lock', '1548218894:1'),
(580, 282, '_wp_page_template', 'news.php'),
(581, 284, '_edit_last', '1'),
(582, 284, '_wp_page_template', 'category.php'),
(583, 284, '_edit_lock', '1546680045:1'),
(584, 286, '_menu_item_type', 'post_type'),
(585, 286, '_menu_item_menu_item_parent', '0'),
(586, 286, '_menu_item_object_id', '284'),
(587, 286, '_menu_item_object', 'page'),
(588, 286, '_menu_item_target', ''),
(589, 286, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(590, 286, '_menu_item_xfn', ''),
(591, 286, '_menu_item_url', ''),
(593, 287, '_menu_item_type', 'post_type'),
(594, 287, '_menu_item_menu_item_parent', '0'),
(595, 287, '_menu_item_object_id', '282'),
(596, 287, '_menu_item_object', 'page'),
(597, 287, '_menu_item_target', ''),
(598, 287, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(599, 287, '_menu_item_xfn', ''),
(600, 287, '_menu_item_url', ''),
(602, 288, '_menu_item_type', 'post_type'),
(603, 288, '_menu_item_menu_item_parent', '0'),
(604, 288, '_menu_item_object_id', '280'),
(605, 288, '_menu_item_object', 'page'),
(606, 288, '_menu_item_target', ''),
(607, 288, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(608, 288, '_menu_item_xfn', ''),
(609, 288, '_menu_item_url', ''),
(611, 289, '_menu_item_type', 'post_type'),
(612, 289, '_menu_item_menu_item_parent', '0'),
(613, 289, '_menu_item_object_id', '278'),
(614, 289, '_menu_item_object', 'page'),
(615, 289, '_menu_item_target', ''),
(616, 289, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(617, 289, '_menu_item_xfn', ''),
(618, 289, '_menu_item_url', ''),
(620, 290, '_menu_item_type', 'post_type'),
(621, 290, '_menu_item_menu_item_parent', '0'),
(622, 290, '_menu_item_object_id', '276'),
(623, 290, '_menu_item_object', 'page'),
(624, 290, '_menu_item_target', ''),
(625, 290, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(626, 290, '_menu_item_xfn', ''),
(627, 290, '_menu_item_url', ''),
(629, 291, '_menu_item_type', 'post_type'),
(630, 291, '_menu_item_menu_item_parent', '292'),
(631, 291, '_menu_item_object_id', '274'),
(632, 291, '_menu_item_object', 'page'),
(633, 291, '_menu_item_target', ''),
(634, 291, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(635, 291, '_menu_item_xfn', ''),
(636, 291, '_menu_item_url', ''),
(638, 292, '_menu_item_type', 'post_type'),
(639, 292, '_menu_item_menu_item_parent', '0'),
(640, 292, '_menu_item_object_id', '272'),
(641, 292, '_menu_item_object', 'page'),
(642, 292, '_menu_item_target', ''),
(643, 292, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(644, 292, '_menu_item_xfn', ''),
(645, 292, '_menu_item_url', ''),
(647, 293, '_menu_item_type', 'post_type'),
(648, 293, '_menu_item_menu_item_parent', '0'),
(649, 293, '_menu_item_object_id', '270'),
(650, 293, '_menu_item_object', 'page'),
(651, 293, '_menu_item_target', ''),
(652, 293, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(653, 293, '_menu_item_xfn', ''),
(654, 293, '_menu_item_url', ''),
(656, 294, '_menu_item_type', 'post_type'),
(657, 294, '_menu_item_menu_item_parent', '298'),
(658, 294, '_menu_item_object_id', '268'),
(659, 294, '_menu_item_object', 'page'),
(660, 294, '_menu_item_target', ''),
(661, 294, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(662, 294, '_menu_item_xfn', ''),
(663, 294, '_menu_item_url', ''),
(665, 295, '_menu_item_type', 'post_type'),
(666, 295, '_menu_item_menu_item_parent', '298'),
(667, 295, '_menu_item_object_id', '266'),
(668, 295, '_menu_item_object', 'page'),
(669, 295, '_menu_item_target', ''),
(670, 295, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(671, 295, '_menu_item_xfn', ''),
(672, 295, '_menu_item_url', ''),
(674, 296, '_menu_item_type', 'post_type'),
(675, 296, '_menu_item_menu_item_parent', '298'),
(676, 296, '_menu_item_object_id', '264'),
(677, 296, '_menu_item_object', 'page'),
(678, 296, '_menu_item_target', ''),
(679, 296, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(680, 296, '_menu_item_xfn', ''),
(681, 296, '_menu_item_url', ''),
(683, 297, '_menu_item_type', 'post_type'),
(684, 297, '_menu_item_menu_item_parent', '298'),
(685, 297, '_menu_item_object_id', '262'),
(686, 297, '_menu_item_object', 'page'),
(687, 297, '_menu_item_target', ''),
(688, 297, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(689, 297, '_menu_item_xfn', ''),
(690, 297, '_menu_item_url', ''),
(692, 298, '_menu_item_type', 'post_type'),
(693, 298, '_menu_item_menu_item_parent', '0'),
(694, 298, '_menu_item_object_id', '260'),
(695, 298, '_menu_item_object', 'page'),
(696, 298, '_menu_item_target', ''),
(697, 298, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(698, 298, '_menu_item_xfn', ''),
(699, 298, '_menu_item_url', ''),
(701, 299, '_menu_item_type', 'post_type'),
(702, 299, '_menu_item_menu_item_parent', '301'),
(703, 299, '_menu_item_object_id', '258'),
(704, 299, '_menu_item_object', 'page'),
(705, 299, '_menu_item_target', ''),
(706, 299, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(707, 299, '_menu_item_xfn', ''),
(708, 299, '_menu_item_url', ''),
(710, 300, '_menu_item_type', 'post_type'),
(711, 300, '_menu_item_menu_item_parent', '301'),
(712, 300, '_menu_item_object_id', '256'),
(713, 300, '_menu_item_object', 'page'),
(714, 300, '_menu_item_target', ''),
(715, 300, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(716, 300, '_menu_item_xfn', ''),
(717, 300, '_menu_item_url', ''),
(719, 301, '_menu_item_type', 'post_type'),
(720, 301, '_menu_item_menu_item_parent', '0'),
(721, 301, '_menu_item_object_id', '248'),
(722, 301, '_menu_item_object', 'page'),
(723, 301, '_menu_item_target', ''),
(724, 301, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(725, 301, '_menu_item_xfn', ''),
(726, 301, '_menu_item_url', ''),
(728, 302, '_menu_item_type', 'post_type'),
(729, 302, '_menu_item_menu_item_parent', '301'),
(730, 302, '_menu_item_object_id', '254'),
(731, 302, '_menu_item_object', 'page'),
(732, 302, '_menu_item_target', ''),
(733, 302, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(734, 302, '_menu_item_xfn', ''),
(735, 302, '_menu_item_url', ''),
(737, 303, '_menu_item_type', 'post_type'),
(738, 303, '_menu_item_menu_item_parent', '301'),
(739, 303, '_menu_item_object_id', '250'),
(740, 303, '_menu_item_object', 'page'),
(741, 303, '_menu_item_target', ''),
(742, 303, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(743, 303, '_menu_item_xfn', ''),
(744, 303, '_menu_item_url', ''),
(746, 304, '_menu_item_type', 'post_type'),
(747, 304, '_menu_item_menu_item_parent', '301'),
(748, 304, '_menu_item_object_id', '252'),
(749, 304, '_menu_item_object', 'page'),
(750, 304, '_menu_item_target', ''),
(751, 304, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(752, 304, '_menu_item_xfn', ''),
(753, 304, '_menu_item_url', ''),
(755, 305, '_menu_item_type', 'post_type'),
(756, 305, '_menu_item_menu_item_parent', '309'),
(757, 305, '_menu_item_object_id', '12'),
(758, 305, '_menu_item_object', 'page'),
(759, 305, '_menu_item_target', ''),
(760, 305, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(761, 305, '_menu_item_xfn', ''),
(762, 305, '_menu_item_url', ''),
(764, 306, '_menu_item_type', 'post_type'),
(765, 306, '_menu_item_menu_item_parent', '309'),
(766, 306, '_menu_item_object_id', '8'),
(767, 306, '_menu_item_object', 'page'),
(768, 306, '_menu_item_target', ''),
(769, 306, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(770, 306, '_menu_item_xfn', ''),
(771, 306, '_menu_item_url', ''),
(773, 307, '_menu_item_type', 'post_type'),
(774, 307, '_menu_item_menu_item_parent', '0'),
(775, 307, '_menu_item_object_id', '6'),
(776, 307, '_menu_item_object', 'page'),
(777, 307, '_menu_item_target', ''),
(778, 307, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(779, 307, '_menu_item_xfn', ''),
(780, 307, '_menu_item_url', ''),
(782, 308, '_menu_item_type', 'post_type'),
(783, 308, '_menu_item_menu_item_parent', '309'),
(784, 308, '_menu_item_object_id', '10'),
(785, 308, '_menu_item_object', 'page'),
(786, 308, '_menu_item_target', ''),
(787, 308, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(788, 308, '_menu_item_xfn', ''),
(789, 308, '_menu_item_url', ''),
(791, 309, '_menu_item_type', 'post_type'),
(792, 309, '_menu_item_menu_item_parent', '0'),
(793, 309, '_menu_item_object_id', '18'),
(794, 309, '_menu_item_object', 'page'),
(795, 309, '_menu_item_target', ''),
(796, 309, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(797, 309, '_menu_item_xfn', ''),
(798, 309, '_menu_item_url', ''),
(800, 310, '_edit_last', '1'),
(801, 310, '_edit_lock', '1546682409:1'),
(802, 311, '_edit_last', '1'),
(803, 311, '_wp_page_template', 'gio-hang.php'),
(804, 311, '_edit_lock', '1546682604:1'),
(805, 310, '_wp_trash_meta_status', 'draft'),
(806, 310, '_wp_trash_meta_time', '1546682699'),
(807, 310, '_wp_desired_post_slug', ''),
(810, 315, '_form', '<label> Tên của bạn (bắt buộc)\n    [text* your-name] </label>\n\n<label> Địa chỉ Email (bắt buộc)\n    [email* your-email] </label>\n\n<label> Tiêu đề:\n    [text your-subject] </label>\n\n<label> Thông điệp\n    [textarea your-message] </label>\n\n[submit \"Gửi đi\"]'),
(811, 315, '_mail', 'a:8:{s:7:\"subject\";s:66:\"CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX \"[your-subject]\"\";s:6:\"sender\";s:82:\"CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX <wordpress@xuatkhaulaodong99.vn>\";s:4:\"body\";s:269:\"Gửi đến từ: [your-name] <[your-email]>\nTiêu đề: [your-subject]\n\nNội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX (http://xuatkhaulaodong99.vn)\";s:9:\"recipient\";s:19:\"nvthiet95@gmail.com\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(812, 315, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:66:\"CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX \"[your-subject]\"\";s:6:\"sender\";s:82:\"CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX <wordpress@xuatkhaulaodong99.vn>\";s:4:\"body\";s:194:\"Nội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX (http://xuatkhaulaodong99.vn)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:29:\"Reply-To: nvthiet95@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(813, 315, '_messages', 'a:8:{s:12:\"mail_sent_ok\";s:53:\"Xin cảm ơn, form đã được gửi thành công.\";s:12:\"mail_sent_ng\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:16:\"validation_error\";s:86:\"Có một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\";s:4:\"spam\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:12:\"accept_terms\";s:67:\"Bạn phải chấp nhận điều khoản trước khi gửi form.\";s:16:\"invalid_required\";s:28:\"Mục này là bắt buộc.\";s:16:\"invalid_too_long\";s:36:\"Nhập quá số kí tự cho phép.\";s:17:\"invalid_too_short\";s:44:\"Nhập ít hơn số kí tự tối thiểu.\";}'),
(814, 315, '_additional_settings', NULL),
(815, 315, '_locale', 'vi'),
(816, 1, '_edit_lock', '1546741600:1'),
(817, 317, '_edit_last', '1'),
(818, 317, '_edit_lock', '1546749995:1'),
(819, 317, '_wp_page_template', 'cam-on.php'),
(940, 1, '_wp_trash_meta_status', 'publish'),
(941, 1, '_wp_trash_meta_time', '1546761081'),
(942, 1, '_wp_desired_post_slug', 'chao-moi-nguoi'),
(943, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(944, 340, '_edit_lock', '1546792160:1'),
(945, 340, '_edit_last', '1'),
(946, 222, 'lien_ket', 'https://fb.com/goku.5895'),
(947, 222, '_lien_ket', 'field_5c32262e42a8b'),
(948, 225, 'lien_ket', 'https://youtube.com'),
(949, 225, '_lien_ket', 'field_5c32262e42a8b'),
(950, 344, '_edit_lock', '1547786343:1'),
(951, 345, '_edit_lock', '1547786746:1'),
(952, 346, '_edit_lock', '1547786872:1'),
(953, 348, '_edit_lock', '1547787039:1'),
(954, 348, '_edit_last', '1'),
(955, 349, '_edit_lock', '1547787074:1'),
(956, 349, '_edit_last', '1'),
(957, 349, '_gallery', 'a:0:{}'),
(958, 349, '_gallery_format', 'default'),
(959, 349, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(960, 350, '_edit_lock', '1547859353:1'),
(961, 354, '_edit_lock', '1547859478:1'),
(962, 358, '_edit_lock', '1547859873:1'),
(963, 360, '_edit_lock', '1547859957:1'),
(964, 360, '_edit_last', '1'),
(965, 360, '_gallery', 'a:0:{}'),
(966, 360, '_gallery_format', 'default'),
(967, 360, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(968, 361, '_edit_lock', '1547861009:1'),
(969, 361, '_edit_last', '1'),
(970, 361, '_thumbnail_id', '182'),
(971, 361, '_gallery', 'a:0:{}'),
(972, 361, '_gallery_format', 'default'),
(973, 361, '_gallery_format_data', 'a:1:{s:7:\"default\";a:0:{}}'),
(974, 361, '_wp_trash_meta_status', 'publish'),
(975, 361, '_wp_trash_meta_time', '1547861163'),
(976, 361, '_wp_desired_post_slug', 'hihi'),
(977, 360, '_wp_trash_meta_status', 'publish'),
(978, 360, '_wp_trash_meta_time', '1547861163'),
(979, 360, '_wp_desired_post_slug', 'hello-mot-hai'),
(980, 349, '_wp_trash_meta_status', 'publish'),
(981, 349, '_wp_trash_meta_time', '1547861163'),
(982, 349, '_wp_desired_post_slug', 'test'),
(983, 363, '_edit_lock', '1547861509:1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-01-05 01:42:18', '2019-01-05 01:42:18', 'Chúc mừng đến với WordPress. Đây là bài viết đầu tiên của bạn. Hãy chỉnh sửa hay xóa bài viết này, và bắt đầu viết blog!', 'Chào tất cả mọi người!', '', 'trash', 'open', 'open', '', 'chao-moi-nguoi__trashed', '', '', '2019-01-06 07:51:21', '2019-01-06 07:51:21', '', 0, 'http://xuatkhaulaodong99.vn/?p=1', 0, 'post', '', 1),
(2, 1, '2019-01-05 01:42:18', '2019-01-05 01:42:18', 'Đây là một trang mẫu. Nó khác với một bài blog bởi vì nó sẽ là một trang tĩnh và sẽ được thêm vào thanh menu của trang web của bạn (trong hầu hết theme). Mọi người thường bắt đầu bằng một trang Giới thiệu để giới thiệu bản thân đến người dùng tiềm năng. Bạn có thể viết như sau:\n\n<blockquote>Xin chào! Tôi là người giao thư bằng xe đạp vào ban ngày, một diễn viên đầy tham vọng vào ban đêm, và đây là trang web của tôi. Tôi sống ở Los Angeles, có một chú cho tuyệt vời tên là Jack, và tôi thích uống cocktail.</blockquote>\n\n...hay như thế này:\n\n<blockquote>Công ty XYZ Doohickey được thành lập vào năm 1971, và đã cung cấp đồ dùng chất lượng cho công chúng kể từ đó. Nằm ở thành phố Gotham, XYZ tạo việc làm cho hơn 2.000 người và làm tất cả những điều tuyệt vời cho cộng đồng Gotham.</blockquote>\n\nLà người dùng WordPress mới, bạn nên truy cập <a href=\"http://xuatkhaulaodong99.vn/wp-admin/\">trang quản trị</a> để xóa trang này và tạo các trang mới cho nội dung của bạn. Chúc vui vẻ!', 'Trang Mẫu', '', 'trash', 'closed', 'open', '', 'Trang mẫu__trashed', '', '', '2019-01-05 01:43:04', '2019-01-05 01:43:04', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-01-05 01:42:18', '2019-01-05 01:42:18', '<h2>Chúng tôi là ai</h2><p>Địa chỉ website là: http://xuatkhaulaodong99.vn.</p><h2>Thông tin cá nhân nào bị thu thập và tại sao thu thập</h2><h3>Bình luận</h3><p>Khi khách truy cập để lại bình luận trên trang web, chúng tôi thu thập dữ liệu được hiển thị trong biểu mẫu bình luận và cũng là địa chỉ IP của người truy cập và chuỗi user agent của người dùng trình duyệt để giúp phát hiện spam</p><p>Một chuỗi ẩn danh được tạo từ địa chỉ email của bạn (còn được gọi là hash) có thể được cung cấp cho dịch vụ Gravatar để xem bạn có đang sử dụng nó hay không. Chính sách bảo mật của dịch vụ Gravatar có tại đây: https://automattic.com/privacy/. Sau khi chấp nhận bình luận của bạn, ảnh tiểu sử của bạn được hiển thị công khai trong ngữ cảnh bình luận của bạn.</p><h3>Thư viện</h3><p>Nếu bạn tải hình ảnh lên trang web, bạn nên tránh tải lên hình ảnh có dữ liệu vị trí được nhúng (EXIF GPS) đi kèm. Khách truy cập vào trang web có thể tải xuống và giải nén bất kỳ dữ liệu vị trí nào từ hình ảnh trên trang web.</p><h3>Thông tin liên hệ</h3><h3>Cookies</h3><p>Nếu bạn viết bình luận trong website, bạn có thể cung cấp cần nhập tên, email địa chỉ website trong cookie. Các thông tin này nhằm giúp bạn không cần nhập thông tin nhiều lần khi viết bình luận khác. Cookie này sẽ được lưu giữ trong một năm.</p><p>Nếu bạn có tài khoản và đăng nhập và website, chúng tôi sẽ thiết lập một cookie tạm thời để xác định nếu trình duyệt cho phép sử dụng cookie. Cookie này không bao gồm thông tin cá nhân và sẽ được gỡ bỏ khi bạn đóng trình duyệt.</p><p>Khi bạn đăng nhập, chúng tôi sẽ thiết lập một vài cookie để lưu thông tin đăng nhập và lựa chọn hiển thị. Thông tin đăng nhập gần nhất lưu trong hai ngày, và lựa chọn hiển thị gần nhất lưu trong một năm. Nếu bạn chọn &quot;Nhớ tôi&quot;, thông tin đăng nhập sẽ được lưu trong hai tuần. Nếu bạn thoát tài khoản, thông tin cookie đăng nhập sẽ bị xoá.</p><p>Nếu bạn sửa hoặc công bố bài viết, một bản cookie bổ sung sẽ được lưu trong trình duyệt. Cookie này không chứa thông tin cá nhân và chỉ đơn giản bao gồm ID của bài viết bạn đã sửa. Nó tự động hết hạn sau 1 ngày.</p><h3>Nội dung nhúng từ website khác</h3><p>Các bài viết trên trang web này có thể bao gồm nội dung được nhúng (ví dụ: video, hình ảnh, bài viết, v.v.). Nội dung được nhúng từ các trang web khác hoạt động theo cùng một cách chính xác như khi khách truy cập đã truy cập trang web khác.</p><p>Những website này có thể thu thập dữ liệu về bạn, sử dụng cookie, nhúng các trình theo dõi của bên thứ ba và giám sát tương tác của bạn với nội dung được nhúng đó, bao gồm theo dõi tương tác của bạn với nội dung được nhúng nếu bạn có tài khoản và đã đăng nhập vào trang web đó.</p><h3>Phân tích</h3><h2>Chúng tôi chia sẻ dữ liệu của bạn với ai</h2><h2>Dữ liệu của bạn tồn tại bao lâu</h2><p>Nếu bạn để lại bình luận, bình luận và siêu dữ liệu của nó sẽ được giữ lại vô thời hạn. Điều này là để chúng tôi có thể tự động nhận ra và chấp nhận bất kỳ bình luận nào thay vì giữ chúng trong khu vực đợi kiểm duyệt.</p><p>Đối với người dùng đăng ký trên trang web của chúng tôi (nếu có), chúng tôi cũng lưu trữ thông tin cá nhân mà họ cung cấp trong hồ sơ người dùng của họ. Tất cả người dùng có thể xem, chỉnh sửa hoặc xóa thông tin cá nhân của họ bất kỳ lúc nào (ngoại trừ họ không thể thay đổi tên người dùng của họ). Quản trị viên trang web cũng có thể xem và chỉnh sửa thông tin đó.</p><h2>Các quyền nào của bạn với dữ liệu của mình</h2><p>Nếu bạn có tài khoản trên trang web này hoặc đã để lại nhận xét, bạn có thể yêu cầu nhận tệp xuất dữ liệu cá nhân mà chúng tôi lưu giữ về bạn, bao gồm mọi dữ liệu bạn đã cung cấp cho chúng tôi. Bạn cũng có thể yêu cầu chúng tôi xóa mọi dữ liệu cá nhân mà chúng tôi lưu giữ về bạn. Điều này không bao gồm bất kỳ dữ liệu nào chúng tôi có nghĩa vụ giữ cho các mục đích hành chính, pháp lý hoặc bảo mật.</p><h2>Các dữ liệu của bạn được gửi tới đâu</h2><p>Các bình luận của khách (không phải là thành viên) có thể được kiểm tra thông qua dịch vụ tự động phát hiện spam.</p><h2>Thông tin liên hệ của bạn</h2><h2>Thông tin bổ sung</h2><h3>Cách chúng tôi bảo vệ dữ liệu của bạn</h3><h3>Các quá trình tiết lộ dữ liệu mà chúng tôi thực hiện</h3><h3>Những bên thứ ba chúng tôi nhận dữ liệu từ đó</h3><h3>Việc quyết định và/hoặc thu thập thông tin tự động mà chúng tôi áp dụng với dữ liệu người dùng</h3><h3>Các yêu cầu công bố thông tin được quản lý</h3>', 'Chính sách bảo mật', '', 'draft', 'closed', 'open', '', 'chinh-sach-bao-mat', '', '', '2019-01-05 01:42:18', '2019-01-05 01:42:18', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-01-05 01:43:04', '2019-01-05 01:43:04', 'Đây là một trang mẫu. Nó khác với một bài blog bởi vì nó sẽ là một trang tĩnh và sẽ được thêm vào thanh menu của trang web của bạn (trong hầu hết theme). Mọi người thường bắt đầu bằng một trang Giới thiệu để giới thiệu bản thân đến người dùng tiềm năng. Bạn có thể viết như sau:\n\n<blockquote>Xin chào! Tôi là người giao thư bằng xe đạp vào ban ngày, một diễn viên đầy tham vọng vào ban đêm, và đây là trang web của tôi. Tôi sống ở Los Angeles, có một chú cho tuyệt vời tên là Jack, và tôi thích uống cocktail.</blockquote>\n\n...hay như thế này:\n\n<blockquote>Công ty XYZ Doohickey được thành lập vào năm 1971, và đã cung cấp đồ dùng chất lượng cho công chúng kể từ đó. Nằm ở thành phố Gotham, XYZ tạo việc làm cho hơn 2.000 người và làm tất cả những điều tuyệt vời cho cộng đồng Gotham.</blockquote>\n\nLà người dùng WordPress mới, bạn nên truy cập <a href=\"http://xuatkhaulaodong99.vn/wp-admin/\">trang quản trị</a> để xóa trang này và tạo các trang mới cho nội dung của bạn. Chúc vui vẻ!', 'Trang Mẫu', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-01-05 01:43:04', '2019-01-05 01:43:04', '', 2, 'http://xuatkhaulaodong99.vn/2019/01/05/2-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2019-01-05 01:45:04', '2019-01-05 01:45:04', 'Đây là trang giới thiệu', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2019-01-05 01:45:04', '2019-01-05 01:45:04', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-01-05 01:45:04', '2019-01-05 01:45:04', 'Đây là trang giới thiệu', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-01-05 01:45:04', '2019-01-05 01:45:04', '', 6, 'http://xuatkhaulaodong99.vn/2019/01/05/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-01-05 01:45:33', '2019-01-05 01:45:33', '', 'Đơn hàng xuất khẩu lao động', '', 'publish', 'closed', 'closed', '', 'don-hang-xuat-khau-lao-dong', '', '', '2019-01-05 01:45:42', '2019-01-05 01:45:42', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-01-05 01:45:33', '2019-01-05 01:45:33', '', 'Đơn hàng xuất khẩu lao động', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-01-05 01:45:33', '2019-01-05 01:45:33', '', 8, 'http://xuatkhaulaodong99.vn/2019/01/05/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-01-05 01:45:59', '2019-01-05 01:45:59', '', 'Tu nghiệp sinh', '', 'publish', 'closed', 'closed', '', 'tu-nghiep-sinh', '', '', '2019-01-05 01:45:59', '2019-01-05 01:45:59', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=10', 0, 'page', '', 0),
(11, 1, '2019-01-05 01:45:59', '2019-01-05 01:45:59', '', 'Tu nghiệp sinh', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2019-01-05 01:45:59', '2019-01-05 01:45:59', '', 10, 'http://xuatkhaulaodong99.vn/2019/01/05/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2019-01-05 01:46:13', '2019-01-05 01:46:13', '', 'Chương trình kỹ sư', '', 'publish', 'closed', 'closed', '', 'chuong-trinh-ky-su', '', '', '2019-01-05 01:46:13', '2019-01-05 01:46:13', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=12', 0, 'page', '', 0),
(13, 1, '2019-01-05 01:46:13', '2019-01-05 01:46:13', '', 'Chương trình kỹ sư', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-01-05 01:46:13', '2019-01-05 01:46:13', '', 12, 'http://xuatkhaulaodong99.vn/2019/01/05/12-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-01-05 01:47:06', '2019-01-05 01:47:06', '', 'Xuất khẩu lao động', '', 'publish', 'closed', 'closed', '', 'xuat-khau-lao-dong', '', '', '2019-01-05 01:47:06', '2019-01-05 01:47:06', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=18', 0, 'page', '', 0),
(19, 1, '2019-01-05 01:47:06', '2019-01-05 01:47:06', '', 'Xuất khẩu lao động', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-01-05 01:47:06', '2019-01-05 01:47:06', '', 18, 'http://xuatkhaulaodong99.vn/2019/01/05/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-01-05 01:47:21', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-05 01:47:21', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=20', 1, 'nav_menu_item', '', 0),
(21, 1, '2019-01-05 01:47:21', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-05 01:47:21', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=21', 1, 'nav_menu_item', '', 0),
(22, 1, '2019-01-05 01:47:22', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-05 01:47:22', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=22', 1, 'nav_menu_item', '', 0),
(23, 1, '2019-01-05 01:47:22', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-05 01:47:22', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2019-01-05 01:47:22', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-01-05 01:47:22', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=24', 1, 'nav_menu_item', '', 0),
(101, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"don_hang\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:4:\"left\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Chi tiết đơn hàng', 'chi-tiet-don-hang', 'publish', 'closed', 'closed', '', 'group_5c301f04a3371', '', '', '2019-01-18 04:50:18', '2019-01-18 04:50:18', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=acf-field-group&#038;p=10.html', 0, 'acf-field-group', '', 0),
(102, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:15:\"Ví dụ: NTD01\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mã đơn hàng', 'ma_don_hang', 'publish', 'closed', 'closed', '', 'field_5c301f13c5fd9', '', '', '2019-01-05 03:10:17', '2019-01-05 03:10:17', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&p=10.html', 0, 'acf-field', '', 0),
(103, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:20:\"Ví dụ: 29 triệu\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Lương', 'luong', 'publish', 'closed', 'closed', '', 'field_5c301f66c5fda', '', '', '2019-01-05 03:10:17', '2019-01-05 03:10:17', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&p=10.html', 1, 'acf-field', '', 0),
(104, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:15:\"Ví dụ: Osaka\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nơi làm việc', 'noi_lam_viec', 'publish', 'closed', 'closed', '', 'field_5c301f88c5fdb', '', '', '2019-01-05 03:10:17', '2019-01-05 03:10:17', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&p=10.html', 2, 'acf-field', '', 0),
(105, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:15:\"Có sẵn đơn\";s:11:\"placeholder\";s:25:\"Ví dụ: Có sẵn đơn\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tình trạng', 'tinh_trang', 'publish', 'closed', 'closed', '', 'field_5c301faac5fdc', '', '', '2019-01-05 03:10:17', '2019-01-05 03:10:17', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&p=10.html', 3, 'acf-field', '', 0),
(106, 1, '2019-01-05 03:10:17', '2019-01-05 03:10:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:20:\"Ví dụ: 0898732276\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Số điện thoại tư vấn', 'so_dien_thoai', 'publish', 'closed', 'closed', '', 'field_5c301fc9c5fdd', '', '', '2019-01-05 03:35:48', '2019-01-05 03:35:48', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&#038;p=10.html', 4, 'acf-field', '', 0),
(109, 1, '2019-01-05 03:11:50', '2019-01-05 03:11:50', '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', '2019-01-05 03:11:50', '2019-01-05 03:11:50', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/2.jpg', 0, 'attachment', 'image/jpeg', 0),
(110, 1, '2019-01-05 03:12:15', '2019-01-05 03:12:15', '', '3', '', 'inherit', 'open', 'closed', '', '3', '', '', '2019-01-05 03:12:15', '2019-01-05 03:12:15', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/3.jpg', 0, 'attachment', 'image/jpeg', 0),
(111, 1, '2019-01-05 03:12:15', '2019-01-05 03:12:15', '', '4', '', 'inherit', 'open', 'closed', '', '4', '', '', '2019-01-05 03:12:15', '2019-01-05 03:12:15', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/4.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', '5', '', 'inherit', 'open', 'closed', '', '5', '', '', '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/5.jpg', 0, 'attachment', 'image/jpeg', 0),
(113, 1, '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', '6', '', 'inherit', 'open', 'closed', '', '6', '', '', '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/6.jpg', 0, 'attachment', 'image/jpeg', 0),
(114, 1, '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', '7', '', 'inherit', 'open', 'closed', '', '7', '', '', '2019-01-05 03:12:16', '2019-01-05 03:12:16', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/7.jpg', 0, 'attachment', 'image/jpeg', 0),
(115, 1, '2019-01-05 03:12:17', '2019-01-05 03:12:17', '', '8', '', 'inherit', 'open', 'closed', '', '8', '', '', '2019-01-05 03:12:17', '2019-01-05 03:12:17', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/8.jpg', 0, 'attachment', 'image/jpeg', 0),
(116, 1, '2019-01-05 03:12:17', '2019-01-05 03:12:17', '', '9', '', 'inherit', 'open', 'closed', '', '9', '', '', '2019-01-05 03:12:17', '2019-01-05 03:12:17', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/9.jpg', 0, 'attachment', 'image/jpeg', 0),
(117, 1, '2019-01-05 03:12:18', '2019-01-05 03:12:18', '', '10', '', 'inherit', 'open', 'closed', '', '10', '', '', '2019-01-05 03:12:18', '2019-01-05 03:12:18', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/10.jpg', 0, 'attachment', 'image/jpeg', 0),
(118, 1, '2019-01-05 03:12:18', '2019-01-05 03:12:18', '', '11', '', 'inherit', 'open', 'closed', '', '11', '', '', '2019-01-05 03:12:18', '2019-01-05 03:12:18', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/11.jpg', 0, 'attachment', 'image/jpeg', 0),
(119, 1, '2019-01-05 03:12:19', '2019-01-05 03:12:19', '', '12', '', 'inherit', 'open', 'closed', '', '12', '', '', '2019-01-05 03:12:19', '2019-01-05 03:12:19', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/12.jpg', 0, 'attachment', 'image/jpeg', 0),
(120, 1, '2019-01-05 03:12:19', '2019-01-05 03:12:19', '', '13', '', 'inherit', 'open', 'closed', '', '13', '', '', '2019-01-05 03:12:19', '2019-01-05 03:12:19', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/13.jpg', 0, 'attachment', 'image/jpeg', 0),
(121, 1, '2019-01-05 03:12:20', '2019-01-05 03:12:20', '', '14', '', 'inherit', 'open', 'closed', '', '14-2', '', '', '2019-01-05 03:12:20', '2019-01-05 03:12:20', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/14.jpg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2019-01-05 03:12:21', '2019-01-05 03:12:21', '', '15', '', 'inherit', 'open', 'closed', '', '15-2', '', '', '2019-01-05 03:12:21', '2019-01-05 03:12:21', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/15.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2019-01-05 03:12:22', '2019-01-05 03:12:22', '', '16', '', 'inherit', 'open', 'closed', '', '16-2', '', '', '2019-01-05 03:12:22', '2019-01-05 03:12:22', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/16.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2019-01-05 03:12:23', '2019-01-05 03:12:23', '', '17', '', 'inherit', 'open', 'closed', '', '17-2', '', '', '2019-01-05 03:12:23', '2019-01-05 03:12:23', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/17.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2019-01-05 03:12:24', '2019-01-05 03:12:24', '', '18', '', 'inherit', 'open', 'closed', '', '18', '', '', '2019-01-05 03:12:24', '2019-01-05 03:12:24', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/18.jpg', 0, 'attachment', 'image/jpeg', 0),
(126, 1, '2019-01-05 03:12:24', '2019-01-05 03:12:24', '', '19', '', 'inherit', 'open', 'closed', '', '19', '', '', '2019-01-05 03:12:24', '2019-01-05 03:12:24', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/19.jpg', 0, 'attachment', 'image/jpeg', 0),
(127, 1, '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', '20', '', 'inherit', 'open', 'closed', '', '20', '', '', '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/20.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', '21', '', 'inherit', 'open', 'closed', '', '21', '', '', '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/21.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', '22', '', 'inherit', 'open', 'closed', '', '22', '', '', '2019-01-05 03:12:25', '2019-01-05 03:12:25', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/22.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 1, '2019-01-05 03:12:26', '2019-01-05 03:12:26', '', '23', '', 'inherit', 'open', 'closed', '', '23', '', '', '2019-01-05 03:12:26', '2019-01-05 03:12:26', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/23.jpg', 0, 'attachment', 'image/jpeg', 0),
(131, 1, '2019-01-05 03:12:26', '2019-01-05 03:12:26', '', '24', '', 'inherit', 'open', 'closed', '', '24', '', '', '2019-01-05 03:12:26', '2019-01-05 03:12:26', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/24.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2019-01-05 03:12:27', '2019-01-05 03:12:27', '', '25', '', 'inherit', 'open', 'closed', '', '25-2', '', '', '2019-01-05 03:12:27', '2019-01-05 03:12:27', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/25.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 1, '2019-01-05 03:12:28', '2019-01-05 03:12:28', '', '26', '', 'inherit', 'open', 'closed', '', '26', '', '', '2019-01-05 03:12:28', '2019-01-05 03:12:28', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/26.jpg', 0, 'attachment', 'image/jpeg', 0),
(134, 1, '2019-01-05 03:12:29', '2019-01-05 03:12:29', '', '27', '', 'inherit', 'open', 'closed', '', '27', '', '', '2019-01-05 03:12:29', '2019-01-05 03:12:29', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/27.jpg', 0, 'attachment', 'image/jpeg', 0),
(135, 1, '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', '28', '', 'inherit', 'open', 'closed', '', '28', '', '', '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/28.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', '29', '', 'inherit', 'open', 'closed', '', '29', '', '', '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/29.jpg', 0, 'attachment', 'image/jpeg', 0),
(137, 1, '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', '30', '', 'inherit', 'open', 'closed', '', '30', '', '', '2019-01-05 03:12:30', '2019-01-05 03:12:30', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/30.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2019-01-05 03:12:31', '2019-01-05 03:12:31', '', '31', '', 'inherit', 'open', 'closed', '', '31', '', '', '2019-01-05 03:12:31', '2019-01-05 03:12:31', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/31.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2019-01-05 03:12:31', '2019-01-05 03:12:31', '', '32', '', 'inherit', 'open', 'closed', '', '32', '', '', '2019-01-05 03:12:31', '2019-01-05 03:12:31', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/32.jpg', 0, 'attachment', 'image/jpeg', 0),
(140, 1, '2019-01-05 03:12:32', '2019-01-05 03:12:32', '', '33', '', 'inherit', 'open', 'closed', '', '33', '', '', '2019-01-05 03:12:32', '2019-01-05 03:12:32', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/33.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2019-01-05 03:12:32', '2019-01-05 03:12:32', '', '34', '', 'inherit', 'open', 'closed', '', '34', '', '', '2019-01-05 03:12:32', '2019-01-05 03:12:32', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/34.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2019-01-05 03:12:33', '2019-01-05 03:12:33', '', '35', '', 'inherit', 'open', 'closed', '', '35', '', '', '2019-01-05 03:12:33', '2019-01-05 03:12:33', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/35.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2019-01-05 03:12:34', '2019-01-05 03:12:34', '', '36', '', 'inherit', 'open', 'closed', '', '36', '', '', '2019-01-05 03:12:34', '2019-01-05 03:12:34', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/36.jpg', 0, 'attachment', 'image/jpeg', 0),
(144, 1, '2019-01-05 03:12:35', '2019-01-05 03:12:35', '', '37', '', 'inherit', 'open', 'closed', '', '37', '', '', '2019-01-05 03:12:35', '2019-01-05 03:12:35', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/37.jpg', 0, 'attachment', 'image/jpeg', 0),
(145, 1, '2019-01-05 03:12:35', '2019-01-05 03:12:35', '', '38', '', 'inherit', 'open', 'closed', '', '38', '', '', '2019-01-05 03:12:35', '2019-01-05 03:12:35', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/38.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 1, '2019-01-05 03:12:36', '2019-01-05 03:12:36', '', '39', '', 'inherit', 'open', 'closed', '', '39', '', '', '2019-01-05 03:12:36', '2019-01-05 03:12:36', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/39.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 1, '2019-01-05 03:12:37', '2019-01-05 03:12:37', '', '40', '', 'inherit', 'open', 'closed', '', '40', '', '', '2019-01-05 03:12:37', '2019-01-05 03:12:37', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/40.jpg', 0, 'attachment', 'image/jpeg', 0),
(148, 1, '2019-01-05 03:12:37', '2019-01-05 03:12:37', '', '41', '', 'inherit', 'open', 'closed', '', '41', '', '', '2019-01-05 03:12:37', '2019-01-05 03:12:37', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/41.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', '42', '', 'inherit', 'open', 'closed', '', '42', '', '', '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/42.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 1, '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', '43', '', 'inherit', 'open', 'closed', '', '43', '', '', '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/43.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', '44', '', 'inherit', 'open', 'closed', '', '44', '', '', '2019-01-05 03:12:38', '2019-01-05 03:12:38', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/44.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2019-01-05 03:12:39', '2019-01-05 03:12:39', '', '45', '', 'inherit', 'open', 'closed', '', '45', '', '', '2019-01-05 03:12:39', '2019-01-05 03:12:39', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/45.jpg', 0, 'attachment', 'image/jpeg', 0),
(153, 1, '2019-01-05 03:12:40', '2019-01-05 03:12:40', '', '46', '', 'inherit', 'open', 'closed', '', '46', '', '', '2019-01-05 03:12:40', '2019-01-05 03:12:40', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/46.jpg', 0, 'attachment', 'image/jpeg', 0),
(154, 1, '2019-01-05 03:12:40', '2019-01-05 03:12:40', '', '47', '', 'inherit', 'open', 'closed', '', '47', '', '', '2019-01-05 03:12:40', '2019-01-05 03:12:40', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/47.jpg', 0, 'attachment', 'image/jpeg', 0),
(155, 1, '2019-01-05 03:12:41', '2019-01-05 03:12:41', '', '48', '', 'inherit', 'open', 'closed', '', '48', '', '', '2019-01-05 03:12:41', '2019-01-05 03:12:41', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/48.jpg', 0, 'attachment', 'image/jpeg', 0),
(156, 1, '2019-01-05 03:12:41', '2019-01-05 03:12:41', '', '49', '', 'inherit', 'open', 'closed', '', '49', '', '', '2019-01-05 03:12:41', '2019-01-05 03:12:41', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/49.jpg', 0, 'attachment', 'image/jpeg', 0),
(157, 1, '2019-01-05 03:12:42', '2019-01-05 03:12:42', '', '50', '', 'inherit', 'open', 'closed', '', '50', '', '', '2019-01-05 03:12:42', '2019-01-05 03:12:42', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/50.jpg', 0, 'attachment', 'image/jpeg', 0),
(158, 1, '2019-01-05 03:12:42', '2019-01-05 03:12:42', '', '51', '', 'inherit', 'open', 'closed', '', '51', '', '', '2019-01-05 03:12:42', '2019-01-05 03:12:42', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/51.jpg', 0, 'attachment', 'image/jpeg', 0),
(159, 1, '2019-01-05 03:12:43', '2019-01-05 03:12:43', '', '52', '', 'inherit', 'open', 'closed', '', '52', '', '', '2019-01-05 03:12:43', '2019-01-05 03:12:43', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/52.jpg', 0, 'attachment', 'image/jpeg', 0),
(160, 1, '2019-01-05 03:12:43', '2019-01-05 03:12:43', '', '53', '', 'inherit', 'open', 'closed', '', '53', '', '', '2019-01-05 03:12:43', '2019-01-05 03:12:43', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/53.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 1, '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', '54', '', 'inherit', 'open', 'closed', '', '54', '', '', '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/54.jpg', 0, 'attachment', 'image/jpeg', 0),
(162, 1, '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', '55', '', 'inherit', 'open', 'closed', '', '55', '', '', '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/55.jpg', 0, 'attachment', 'image/jpeg', 0),
(163, 1, '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', '56', '', 'inherit', 'open', 'closed', '', '56', '', '', '2019-01-05 03:12:44', '2019-01-05 03:12:44', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/56.jpg', 0, 'attachment', 'image/jpeg', 0),
(164, 1, '2019-01-05 03:12:45', '2019-01-05 03:12:45', '', '57', '', 'inherit', 'open', 'closed', '', '57', '', '', '2019-01-05 03:12:45', '2019-01-05 03:12:45', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/57.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 1, '2019-01-05 03:12:45', '2019-01-05 03:12:45', '', '58', '', 'inherit', 'open', 'closed', '', '58', '', '', '2019-01-05 03:12:45', '2019-01-05 03:12:45', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/58.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2019-01-05 03:12:46', '2019-01-05 03:12:46', '', '59', '', 'inherit', 'open', 'closed', '', '59', '', '', '2019-01-05 03:12:46', '2019-01-05 03:12:46', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/59.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2019-01-05 03:12:46', '2019-01-05 03:12:46', '', '60', '', 'inherit', 'open', 'closed', '', '60', '', '', '2019-01-05 03:12:46', '2019-01-05 03:12:46', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/60.jpg', 0, 'attachment', 'image/jpeg', 0),
(168, 1, '2019-01-05 03:12:47', '2019-01-05 03:12:47', '', '61', '', 'inherit', 'open', 'closed', '', '61', '', '', '2019-01-05 03:12:47', '2019-01-05 03:12:47', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/61.jpg', 0, 'attachment', 'image/jpeg', 0),
(169, 1, '2019-01-05 03:12:47', '2019-01-05 03:12:47', '', '62', '', 'inherit', 'open', 'closed', '', '62', '', '', '2019-01-05 03:12:47', '2019-01-05 03:12:47', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/62.jpg', 0, 'attachment', 'image/jpeg', 0),
(170, 1, '2019-01-05 03:12:48', '2019-01-05 03:12:48', '', '63', '', 'inherit', 'open', 'closed', '', '63', '', '', '2019-01-05 03:12:48', '2019-01-05 03:12:48', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/63.jpg', 0, 'attachment', 'image/jpeg', 0),
(171, 1, '2019-01-05 03:12:48', '2019-01-05 03:12:48', '', '64', '', 'inherit', 'open', 'closed', '', '64', '', '', '2019-01-05 03:12:48', '2019-01-05 03:12:48', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/64.jpg', 0, 'attachment', 'image/jpeg', 0),
(172, 1, '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', '65', '', 'inherit', 'open', 'closed', '', '65', '', '', '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/65.jpg', 0, 'attachment', 'image/jpeg', 0),
(173, 1, '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', '66', '', 'inherit', 'open', 'closed', '', '66', '', '', '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/66.jpg', 0, 'attachment', 'image/jpeg', 0),
(174, 1, '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', '67', '', 'inherit', 'open', 'closed', '', '67', '', '', '2019-01-05 03:12:49', '2019-01-05 03:12:49', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/67.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2019-01-05 03:12:50', '2019-01-05 03:12:50', '', '68', '', 'inherit', 'open', 'closed', '', '68', '', '', '2019-01-05 03:12:50', '2019-01-05 03:12:50', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/68.jpg', 0, 'attachment', 'image/jpeg', 0),
(176, 1, '2019-01-05 03:12:50', '2019-01-05 03:12:50', '', '69', '', 'inherit', 'open', 'closed', '', '69', '', '', '2019-01-05 03:12:50', '2019-01-05 03:12:50', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/69.jpg', 0, 'attachment', 'image/jpeg', 0),
(177, 1, '2019-01-05 03:12:51', '2019-01-05 03:12:51', '', '70', '', 'inherit', 'open', 'closed', '', '70', '', '', '2019-01-05 03:12:51', '2019-01-05 03:12:51', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/70.jpg', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2019-01-05 03:12:51', '2019-01-05 03:12:51', '', '71', '', 'inherit', 'open', 'closed', '', '71', '', '', '2019-01-05 03:12:51', '2019-01-05 03:12:51', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/71.jpg', 0, 'attachment', 'image/jpeg', 0),
(179, 1, '2019-01-05 03:12:52', '2019-01-05 03:12:52', '', '72', '', 'inherit', 'open', 'closed', '', '72', '', '', '2019-01-05 03:12:52', '2019-01-05 03:12:52', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/72.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2019-01-05 03:12:52', '2019-01-05 03:12:52', '', '73', '', 'inherit', 'open', 'closed', '', '73', '', '', '2019-01-05 03:12:52', '2019-01-05 03:12:52', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/73.jpg', 0, 'attachment', 'image/jpeg', 0),
(181, 1, '2019-01-05 03:12:53', '2019-01-05 03:12:53', '', '74', '', 'inherit', 'open', 'closed', '', '74', '', '', '2019-01-05 03:12:53', '2019-01-05 03:12:53', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/74.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2019-01-05 03:12:54', '2019-01-05 03:12:54', '', '75', '', 'inherit', 'open', 'closed', '', '75', '', '', '2019-01-05 03:12:54', '2019-01-05 03:12:54', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/75.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 1, '2019-01-05 03:12:54', '2019-01-05 03:12:54', '', '76', '', 'inherit', 'open', 'closed', '', '76', '', '', '2019-01-05 03:12:54', '2019-01-05 03:12:54', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/76.jpg', 0, 'attachment', 'image/jpeg', 0),
(184, 1, '2019-01-05 03:12:55', '2019-01-05 03:12:55', '', '77', '', 'inherit', 'open', 'closed', '', '77', '', '', '2019-01-05 03:12:55', '2019-01-05 03:12:55', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/77.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2019-01-05 03:12:55', '2019-01-05 03:12:55', '', '78', '', 'inherit', 'open', 'closed', '', '78', '', '', '2019-01-05 03:12:55', '2019-01-05 03:12:55', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/78.jpg', 0, 'attachment', 'image/jpeg', 0),
(186, 1, '2019-01-05 03:12:56', '2019-01-05 03:12:56', '', '79', '', 'inherit', 'open', 'closed', '', '79', '', '', '2019-01-05 03:12:56', '2019-01-05 03:12:56', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/79.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2019-01-05 03:12:57', '2019-01-05 03:12:57', '', '80', '', 'inherit', 'open', 'closed', '', '80', '', '', '2019-01-05 03:12:57', '2019-01-05 03:12:57', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/80.jpg', 0, 'attachment', 'image/jpeg', 0),
(188, 1, '2019-01-05 03:12:58', '2019-01-05 03:12:58', '', '81', '', 'inherit', 'open', 'closed', '', '81', '', '', '2019-01-05 03:12:58', '2019-01-05 03:12:58', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/81.jpg', 0, 'attachment', 'image/jpeg', 0),
(189, 1, '2019-01-05 03:12:58', '2019-01-05 03:12:58', '', '82', '', 'inherit', 'open', 'closed', '', '82', '', '', '2019-01-05 03:12:58', '2019-01-05 03:12:58', '', 0, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/82.jpg', 0, 'attachment', 'image/jpeg', 0),
(190, 1, '2019-01-05 03:14:19', '2019-01-05 03:14:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:25:\"Ví dụ: 30 nam, 20 nữ\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Giới tính', 'gioi_tinh', 'publish', 'closed', 'closed', '', 'field_5c3020eaf9b60', '', '', '2019-01-05 03:27:05', '2019-01-05 03:27:05', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&#038;p=19.html', 5, 'acf-field', '', 0),
(205, 1, '2019-01-05 03:27:05', '2019-01-05 03:27:05', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:4:\"F, Y\";s:13:\"return_format\";s:4:\"F, Y\";s:9:\"first_day\";i:1;}', 'Thời gian tuyển', 'thoi_gian_tuyen', 'publish', 'closed', 'closed', '', 'field_5c3023b72b6d1', '', '', '2019-01-05 03:27:52', '2019-01-05 03:27:52', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&#038;p=20.html', 6, 'acf-field', '', 0),
(207, 1, '2019-01-05 03:30:25', '2019-01-05 03:30:25', '', 'CẦN TUYỂN NAM LÀM CHẾ BIẾN THỰC PHẨM', '', 'publish', 'closed', 'closed', '', 'can-tuyen-nam-lam-che-bien-thuc-pham', '', '', '2019-01-05 03:36:05', '2019-01-05 03:36:05', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=20.html', 0, 'don_hang', '', 0),
(222, 1, '2019-01-05 04:43:46', '2019-01-05 04:43:46', '', 'Banner1', '', 'publish', 'closed', 'closed', '', 'banner1', '', '', '2019-01-06 16:05:00', '2019-01-06 16:05:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=banner&#038;p=22.html', 0, 'banner', '', 0),
(223, 1, '2019-01-05 04:43:29', '2019-01-05 04:43:29', '', 'banner-xuat-khau-lao-dong-nhat-ban1', '', 'inherit', 'open', 'closed', '', 'banner-xuat-khau-lao-dong-nhat-ban1', '', '', '2019-01-05 04:43:29', '2019-01-05 04:43:29', '', 222, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/banner-xuat-khau-lao-dong-nhat-ban1.jpg', 0, 'attachment', 'image/jpeg', 0),
(224, 1, '2019-01-05 04:43:36', '2019-01-05 04:43:36', '', 'banner-xuat-khau-lao-dong', '', 'inherit', 'open', 'closed', '', 'banner-xuat-khau-lao-dong', '', '', '2019-01-05 04:43:36', '2019-01-05 04:43:36', '', 222, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/banner-xuat-khau-lao-dong.jpg', 0, 'attachment', 'image/jpeg', 0),
(225, 1, '2019-01-05 04:44:06', '2019-01-05 04:44:06', '', 'Banner2', '', 'publish', 'closed', 'closed', '', 'banner2', '', '', '2019-01-06 16:05:29', '2019-01-06 16:05:29', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=banner&#038;p=22.html', 0, 'banner', '', 0),
(226, 1, '2019-01-05 04:45:01', '2019-01-05 04:45:01', '', 'back-top', '', 'inherit', 'open', 'closed', '', 'back-top', '', '', '2019-01-05 04:45:01', '2019-01-05 04:45:01', '', 225, 'http://xuatkhaulaodong99.vn/wp-content/uploads/2019/01/back-top.png', 0, 'attachment', 'image/png', 0),
(227, 1, '2019-01-05 04:56:48', '2019-01-05 04:56:48', '', 'CẦN TUYỂN LAO ĐỘNG NỮ LÀM CƠM HỘP', '', 'publish', 'closed', 'closed', '', 'can-tuyen-lao-dong-nu-lam-com-hop', '', '', '2019-01-05 04:56:49', '2019-01-05 04:56:49', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=22.html', 0, 'don_hang', '', 0),
(228, 1, '2019-01-05 04:57:37', '2019-01-05 04:57:37', '', 'CẦN TUYỂN NỮ LÀM NÔNG NGHIỆP', '', 'publish', 'closed', 'closed', '', 'can-tuyen-nu-lam-nong-nghiep', '', '', '2019-01-05 04:57:37', '2019-01-05 04:57:37', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=22.html', 0, 'don_hang', '', 0),
(229, 1, '2019-01-05 04:58:25', '2019-01-05 04:58:25', '', 'CẦN TUYỂN 20 NỮ LÀM MAY MẶC', '', 'publish', 'closed', 'closed', '', 'can-tuyen-20-nu-lam-may-mac', '', '', '2019-01-05 07:49:04', '2019-01-05 07:49:04', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=22.html', 0, 'don_hang', '', 0),
(230, 1, '2019-01-05 04:58:56', '2019-01-05 04:58:56', '', 'TUYỂN GẤP NAM LÀM BÁNH MỲ', '', 'publish', 'closed', 'closed', '', 'tuyen-gap-nam-lam-banh-my', '', '', '2019-01-05 07:48:42', '2019-01-05 07:48:42', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=23.html', 0, 'don_hang', '', 0),
(235, 1, '2019-01-05 07:50:32', '2019-01-05 07:50:32', '', 'CẦN TUYỂN NỮ LÀM CHẾ BIẾN XÚC XÍCH', '', 'publish', 'closed', 'closed', '', 'can-tuyen-nu-lam-che-bien-xuc-xich', '', '', '2019-01-05 07:50:32', '2019-01-05 07:50:32', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=23.html', 0, 'don_hang', '', 0),
(236, 1, '2019-01-05 07:51:11', '2019-01-05 07:51:11', '', 'CẦN TUYỂN NỮ LÀM GIẶT LÀ', '', 'publish', 'closed', 'closed', '', 'can-tuyen-nu-lam-giat-la', '', '', '2019-01-05 07:51:11', '2019-01-05 07:51:11', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=23.html', 0, 'don_hang', '', 0),
(237, 1, '2019-01-05 07:52:13', '2019-01-05 07:52:13', 'Nội dung đơn hàng', 'THÔNG BÁO CÁC ĐƠN HÀNG NHẬT BẢN', '', 'publish', 'closed', 'closed', '', 'thong-bao-cac-don-hang-nhat-ban', '', '', '2019-01-05 09:32:03', '2019-01-05 09:32:03', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&#038;p=23.html', 0, 'don_hang', '', 0),
(239, 1, '2019-01-05 07:57:39', '2019-01-05 07:57:39', '', 'Làm gì để có thu nhập cao khi đi Xuất Khẩu Lao Động Nhật Bản', '', 'publish', 'closed', 'closed', '', 'lam-gi-de-co-thu-nhap-cao-khi-di-xuat-khau-lao-dong-nhat-ban', '', '', '2019-01-05 07:57:40', '2019-01-05 07:57:40', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=23.html', 0, 'tin_tuc', '', 0),
(240, 1, '2019-01-05 07:58:15', '2019-01-05 07:58:15', '', 'Bí quyết thi đậu khi tham gia thi tuyển cho các bạn thực tập sinh Nhật Bản', '', 'publish', 'closed', 'closed', '', 'bi-quyet-thi-dau-khi-tham-gia-thi-tuyen-cho-cac-ban-thuc-tap-sinh-nhat-ban', '', '', '2019-01-19 01:26:55', '2019-01-19 01:26:55', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=24.html', 0, 'tin_tuc', '', 0),
(241, 1, '2019-01-05 07:59:07', '2019-01-05 07:59:07', '', 'Mức phí  khám  sức khỏe khi đi xuất khẩu lao động', '', 'publish', 'closed', 'closed', '', 'muc-phi-kham-suc-khoe-khi-di-xuat-khau-lao-dong', '', '', '2019-01-19 01:26:46', '2019-01-19 01:26:46', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=24.html', 0, 'tin_tuc', '', 0),
(242, 1, '2019-01-05 07:59:45', '2019-01-05 07:59:45', '', 'Tìm hiểu nhu cầu tờ 10.000 Yên tăng mạnh ở Nhật', '', 'publish', 'closed', 'closed', '', 'tim-hieu-nhu-cau-to-10-000-yen-tang-manh-o-nhat', '', '', '2019-01-19 01:26:37', '2019-01-19 01:26:37', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=24.html', 0, 'tin_tuc', '', 0),
(243, 1, '2019-01-05 08:00:19', '2019-01-05 08:00:19', '', 'Cách thức cho các thực tập sinh Nhật Bản về nước và muốn quay lại Nhật', '', 'publish', 'closed', 'closed', '', 'cach-thuc-cho-cac-thuc-tap-sinh-nhat-ban-ve-nuoc-va-muon-quay-lai-nhat', '', '', '2019-01-19 01:26:28', '2019-01-19 01:26:28', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=24.html', 0, 'tin_tuc', '', 0),
(244, 1, '2019-01-05 08:00:57', '2019-01-05 08:00:57', 'Nội dung', 'Hỗ trợ việc làm cho du học sinh Jellyfish tại Nhật Bản', 'Mô tả', 'publish', 'closed', 'closed', '', 'ho-tro-viec-lam-cho-du-hoc-sinh-jellyfish-tai-nhat-ban', '', '', '2019-01-19 01:26:11', '2019-01-19 01:26:11', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=24.html', 0, 'tin_tuc', '', 0),
(245, 1, '2019-01-05 08:33:39', '2019-01-05 08:33:39', '', 'Thông tin dịch vụ', '', 'publish', 'closed', 'closed', '', 'thong-tin-dich-vu', '', '', '2019-01-05 10:06:03', '2019-01-05 10:06:03', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=245', 0, 'page', '', 0),
(246, 1, '2019-01-05 08:33:39', '2019-01-05 08:33:39', '', 'Giỏ hàng', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-01-05 08:33:39', '2019-01-05 08:33:39', '', 245, 'http://xuatkhaulaodong99.vn/245-revision-v1/', 0, 'revision', '', 0),
(247, 1, '2019-01-05 08:42:21', '2019-01-05 08:42:21', '', 'Thông tin dịch vụ', '', 'inherit', 'closed', 'closed', '', '245-revision-v1', '', '', '2019-01-05 08:42:21', '2019-01-05 08:42:21', '', 245, 'http://xuatkhaulaodong99.vn/245-revision-v1/', 0, 'revision', '', 0),
(248, 1, '2019-01-05 09:18:03', '2019-01-05 09:18:03', '', 'Du học', '', 'publish', 'closed', 'closed', '', 'du-hoc', '', '', '2019-01-05 09:18:03', '2019-01-05 09:18:03', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=248', 0, 'page', '', 0),
(249, 1, '2019-01-05 09:18:03', '2019-01-05 09:18:03', '', 'Du học', '', 'inherit', 'closed', 'closed', '', '248-revision-v1', '', '', '2019-01-05 09:18:03', '2019-01-05 09:18:03', '', 248, 'http://xuatkhaulaodong99.vn/248-revision-v1/', 0, 'revision', '', 0),
(250, 1, '2019-01-05 09:18:26', '2019-01-05 09:18:26', '', 'Thủ tục hồ sơ du học', '', 'publish', 'closed', 'closed', '', 'thu-tuc-ho-so-du-hoc', '', '', '2019-01-05 09:18:26', '2019-01-05 09:18:26', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=250', 0, 'page', '', 0),
(251, 1, '2019-01-05 09:18:26', '2019-01-05 09:18:26', '', 'Thủ tục hồ sơ du học', '', 'inherit', 'closed', 'closed', '', '250-revision-v1', '', '', '2019-01-05 09:18:26', '2019-01-05 09:18:26', '', 250, 'http://xuatkhaulaodong99.vn/250-revision-v1/', 0, 'revision', '', 0),
(252, 1, '2019-01-05 09:18:50', '2019-01-05 09:18:50', '', 'Tuyển sinh', '', 'publish', 'closed', 'closed', '', 'tuyen-sinh', '', '', '2019-01-05 09:18:50', '2019-01-05 09:18:50', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=252', 0, 'page', '', 0),
(253, 1, '2019-01-05 09:18:50', '2019-01-05 09:18:50', '', 'Tuyển sinh', '', 'inherit', 'closed', 'closed', '', '252-revision-v1', '', '', '2019-01-05 09:18:50', '2019-01-05 09:18:50', '', 252, 'http://xuatkhaulaodong99.vn/252-revision-v1/', 0, 'revision', '', 0),
(254, 1, '2019-01-05 09:19:05', '2019-01-05 09:19:05', '', 'Đối tác', '', 'publish', 'closed', 'closed', '', 'doi-tac', '', '', '2019-01-05 09:19:05', '2019-01-05 09:19:05', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=254', 0, 'page', '', 0),
(255, 1, '2019-01-05 09:19:05', '2019-01-05 09:19:05', '', 'Đối tác', '', 'inherit', 'closed', 'closed', '', '254-revision-v1', '', '', '2019-01-05 09:19:05', '2019-01-05 09:19:05', '', 254, 'http://xuatkhaulaodong99.vn/254-revision-v1/', 0, 'revision', '', 0),
(256, 1, '2019-01-05 09:19:19', '2019-01-05 09:19:19', '', 'Các trường đại học ở Nhật', '', 'publish', 'closed', 'closed', '', 'cac-truong-dai-hoc-o-nhat', '', '', '2019-01-05 09:19:19', '2019-01-05 09:19:19', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=256', 0, 'page', '', 0),
(257, 1, '2019-01-05 09:19:19', '2019-01-05 09:19:19', '', 'Các trường đại học ở Nhật', '', 'inherit', 'closed', 'closed', '', '256-revision-v1', '', '', '2019-01-05 09:19:19', '2019-01-05 09:19:19', '', 256, 'http://xuatkhaulaodong99.vn/256-revision-v1/', 0, 'revision', '', 0),
(258, 1, '2019-01-05 09:19:34', '2019-01-05 09:19:34', '', 'Dịch vụ điện thoại', '', 'publish', 'closed', 'closed', '', 'dich-vu-dien-thoai', '', '', '2019-01-05 09:19:34', '2019-01-05 09:19:34', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=258', 0, 'page', '', 0),
(259, 1, '2019-01-05 09:19:34', '2019-01-05 09:19:34', '', 'Dịch vụ điện thoại', '', 'inherit', 'closed', 'closed', '', '258-revision-v1', '', '', '2019-01-05 09:19:34', '2019-01-05 09:19:34', '', 258, 'http://xuatkhaulaodong99.vn/258-revision-v1/', 0, 'revision', '', 0),
(260, 1, '2019-01-05 09:19:50', '2019-01-05 09:19:50', '', 'Nhật Bản', '', 'publish', 'closed', 'closed', '', 'nhat-ban', '', '', '2019-01-05 09:19:50', '2019-01-05 09:19:50', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=260', 0, 'page', '', 0),
(261, 1, '2019-01-05 09:19:50', '2019-01-05 09:19:50', '', 'Nhật Bản', '', 'inherit', 'closed', 'closed', '', '260-revision-v1', '', '', '2019-01-05 09:19:50', '2019-01-05 09:19:50', '', 260, 'http://xuatkhaulaodong99.vn/260-revision-v1/', 0, 'revision', '', 0),
(262, 1, '2019-01-05 09:20:07', '2019-01-05 09:20:07', '', 'Kinh nghiệm đi Nhật', '', 'publish', 'closed', 'closed', '', 'kinh-nghiem-di-nhat', '', '', '2019-01-05 09:20:07', '2019-01-05 09:20:07', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=262', 0, 'page', '', 0),
(263, 1, '2019-01-05 09:20:07', '2019-01-05 09:20:07', '', 'Kinh nghiệm đi Nhật', '', 'inherit', 'closed', 'closed', '', '262-revision-v1', '', '', '2019-01-05 09:20:07', '2019-01-05 09:20:07', '', 262, 'http://xuatkhaulaodong99.vn/262-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(264, 1, '2019-01-05 09:20:20', '2019-01-05 09:20:20', '', 'Học tiếng Nhật', '', 'publish', 'closed', 'closed', '', 'hoc-tieng-nhat', '', '', '2019-01-05 09:20:20', '2019-01-05 09:20:20', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=264', 0, 'page', '', 0),
(265, 1, '2019-01-05 09:20:20', '2019-01-05 09:20:20', '', 'Học tiếng Nhật', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2019-01-05 09:20:20', '2019-01-05 09:20:20', '', 264, 'http://xuatkhaulaodong99.vn/264-revision-v1/', 0, 'revision', '', 0),
(266, 1, '2019-01-05 09:20:32', '2019-01-05 09:20:32', '', 'Văn hóa Nhật Bản', '', 'publish', 'closed', 'closed', '', 'van-hoa-nhat-ban', '', '', '2019-01-05 09:20:32', '2019-01-05 09:20:32', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=266', 0, 'page', '', 0),
(267, 1, '2019-01-05 09:20:32', '2019-01-05 09:20:32', '', 'Văn hóa Nhật Bản', '', 'inherit', 'closed', 'closed', '', '266-revision-v1', '', '', '2019-01-05 09:20:32', '2019-01-05 09:20:32', '', 266, 'http://xuatkhaulaodong99.vn/266-revision-v1/', 0, 'revision', '', 0),
(268, 1, '2019-01-05 09:20:45', '2019-01-05 09:20:45', '', 'Việc làm và nhà ở tại Nhật', '', 'publish', 'closed', 'closed', '', 'viec-lam-va-nha-o-tai-nhat', '', '', '2019-01-05 09:20:45', '2019-01-05 09:20:45', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=268', 0, 'page', '', 0),
(269, 1, '2019-01-05 09:20:45', '2019-01-05 09:20:45', '', 'Việc làm và nhà ở tại Nhật', '', 'inherit', 'closed', 'closed', '', '268-revision-v1', '', '', '2019-01-05 09:20:45', '2019-01-05 09:20:45', '', 268, 'http://xuatkhaulaodong99.vn/268-revision-v1/', 0, 'revision', '', 0),
(270, 1, '2019-01-05 09:21:04', '2019-01-05 09:21:04', '', 'Thực tập sinh', '', 'publish', 'closed', 'closed', '', 'thuc-tap-sinh', '', '', '2019-01-05 09:21:04', '2019-01-05 09:21:04', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=270', 0, 'page', '', 0),
(271, 1, '2019-01-05 09:21:04', '2019-01-05 09:21:04', '', 'Thực tập sinh', '', 'inherit', 'closed', 'closed', '', '270-revision-v1', '', '', '2019-01-05 09:21:04', '2019-01-05 09:21:04', '', 270, 'http://xuatkhaulaodong99.vn/270-revision-v1/', 0, 'revision', '', 0),
(272, 1, '2019-01-05 09:21:22', '2019-01-05 09:21:22', '', 'Văn bản', '', 'publish', 'closed', 'closed', '', 'van-ban', '', '', '2019-01-05 09:21:22', '2019-01-05 09:21:22', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=272', 0, 'page', '', 0),
(273, 1, '2019-01-05 09:21:22', '2019-01-05 09:21:22', '', 'Văn bản', '', 'inherit', 'closed', 'closed', '', '272-revision-v1', '', '', '2019-01-05 09:21:22', '2019-01-05 09:21:22', '', 272, 'http://xuatkhaulaodong99.vn/272-revision-v1/', 0, 'revision', '', 0),
(274, 1, '2019-01-05 09:21:40', '2019-01-05 09:21:40', '', 'Thủ tục và hồ sơ', '', 'publish', 'closed', 'closed', '', 'thu-tuc-va-ho-so', '', '', '2019-01-05 09:21:40', '2019-01-05 09:21:40', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=274', 0, 'page', '', 0),
(275, 1, '2019-01-05 09:21:40', '2019-01-05 09:21:40', '', 'Thủ tục và hồ sơ', '', 'inherit', 'closed', 'closed', '', '274-revision-v1', '', '', '2019-01-05 09:21:40', '2019-01-05 09:21:40', '', 274, 'http://xuatkhaulaodong99.vn/274-revision-v1/', 0, 'revision', '', 0),
(276, 1, '2019-01-05 09:21:56', '2019-01-05 09:21:56', '', 'Hướng dẫn đăng ký', '', 'publish', 'closed', 'closed', '', 'huong-dan-dang-ky', '', '', '2019-01-05 09:21:56', '2019-01-05 09:21:56', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=276', 0, 'page', '', 0),
(277, 1, '2019-01-05 09:21:56', '2019-01-05 09:21:56', '', 'Hướng dẫn đăng ký', '', 'inherit', 'closed', 'closed', '', '276-revision-v1', '', '', '2019-01-05 09:21:56', '2019-01-05 09:21:56', '', 276, 'http://xuatkhaulaodong99.vn/276-revision-v1/', 0, 'revision', '', 0),
(278, 1, '2019-01-05 09:22:10', '2019-01-05 09:22:10', '', 'Tuyển dụng', '', 'publish', 'closed', 'closed', '', 'tuyen-dung', '', '', '2019-01-05 09:22:10', '2019-01-05 09:22:10', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=278', 0, 'page', '', 0),
(279, 1, '2019-01-05 09:22:10', '2019-01-05 09:22:10', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '278-revision-v1', '', '', '2019-01-05 09:22:10', '2019-01-05 09:22:10', '', 278, 'http://xuatkhaulaodong99.vn/278-revision-v1/', 0, 'revision', '', 0),
(280, 1, '2019-01-05 09:22:25', '2019-01-05 09:22:25', '', 'Dịch vụ', '', 'publish', 'closed', 'closed', '', 'dich-vu', '', '', '2019-01-05 09:22:25', '2019-01-05 09:22:25', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=280', 0, 'page', '', 0),
(281, 1, '2019-01-05 09:22:25', '2019-01-05 09:22:25', '', 'Dịch vụ', '', 'inherit', 'closed', 'closed', '', '280-revision-v1', '', '', '2019-01-05 09:22:25', '2019-01-05 09:22:25', '', 280, 'http://xuatkhaulaodong99.vn/280-revision-v1/', 0, 'revision', '', 0),
(282, 1, '2019-01-05 09:22:50', '2019-01-05 09:22:50', 'Mô tả trang tin tức', 'Tin Tức', '', 'publish', 'closed', 'closed', '', 'tin-tuc', '', '', '2019-01-23 04:43:56', '2019-01-23 04:43:56', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=282', 0, 'page', '', 0),
(283, 1, '2019-01-05 09:22:50', '2019-01-05 09:22:50', '', 'Tin Tức', '', 'inherit', 'closed', 'closed', '', '282-revision-v1', '', '', '2019-01-05 09:22:50', '2019-01-05 09:22:50', '', 282, 'http://xuatkhaulaodong99.vn/282-revision-v1/', 0, 'revision', '', 0),
(284, 1, '2019-01-05 09:23:04', '2019-01-05 09:23:04', '', 'Liên Hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2019-01-05 09:23:04', '2019-01-05 09:23:04', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=284', 0, 'page', '', 0),
(285, 1, '2019-01-05 09:23:04', '2019-01-05 09:23:04', '', 'Liên Hệ', '', 'inherit', 'closed', 'closed', '', '284-revision-v1', '', '', '2019-01-05 09:23:04', '2019-01-05 09:23:04', '', 284, 'http://xuatkhaulaodong99.vn/284-revision-v1/', 0, 'revision', '', 0),
(286, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '286', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=286', 24, 'nav_menu_item', '', 0),
(287, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '287', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=287', 23, 'nav_menu_item', '', 0),
(288, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '288', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=288', 22, 'nav_menu_item', '', 0),
(289, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '289', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=289', 21, 'nav_menu_item', '', 0),
(290, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '290', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=290', 20, 'nav_menu_item', '', 0),
(291, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '291', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=291', 19, 'nav_menu_item', '', 0),
(292, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '292', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=292', 18, 'nav_menu_item', '', 0),
(293, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '293', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=293', 17, 'nav_menu_item', '', 0),
(294, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '294', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=294', 16, 'nav_menu_item', '', 0),
(295, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '295', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=295', 15, 'nav_menu_item', '', 0),
(296, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '296', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=296', 14, 'nav_menu_item', '', 0),
(297, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '297', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=297', 13, 'nav_menu_item', '', 0),
(298, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '298', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=298', 12, 'nav_menu_item', '', 0),
(299, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '299', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=299', 11, 'nav_menu_item', '', 0),
(300, 1, '2019-01-05 09:28:37', '2019-01-05 09:28:37', ' ', '', '', 'publish', 'closed', 'closed', '', '300', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=300', 10, 'nav_menu_item', '', 0),
(301, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '301', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=301', 6, 'nav_menu_item', '', 0),
(302, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '302', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=302', 9, 'nav_menu_item', '', 0),
(303, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '303', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=303', 7, 'nav_menu_item', '', 0),
(304, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '304', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=304', 8, 'nav_menu_item', '', 0),
(305, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '305', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=305', 5, 'nav_menu_item', '', 0),
(306, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '306', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=306', 3, 'nav_menu_item', '', 0),
(307, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '307', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=307', 1, 'nav_menu_item', '', 0),
(308, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '308', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=308', 4, 'nav_menu_item', '', 0),
(309, 1, '2019-01-05 09:28:36', '2019-01-05 09:28:36', ' ', '', '', 'publish', 'closed', 'closed', '', '309', '', '', '2019-01-05 09:29:22', '2019-01-05 09:29:22', '', 0, 'http://xuatkhaulaodong99.vn/?p=309', 2, 'nav_menu_item', '', 0),
(310, 1, '2019-01-05 10:04:59', '2019-01-05 10:04:59', '', 'Giỏ hàng', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2019-01-05 10:04:59', '2019-01-05 10:04:59', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=310', 0, 'page', '', 0),
(311, 1, '2019-01-05 10:02:26', '2019-01-05 10:02:26', '', 'Giỏ hàng', '', 'publish', 'closed', 'closed', '', 'gio-hang', '', '', '2019-01-05 10:05:12', '2019-01-05 10:05:12', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=311', 0, 'page', '', 0),
(312, 1, '2019-01-05 10:02:26', '2019-01-05 10:02:26', '', 'Giỏ hàng', '', 'inherit', 'closed', 'closed', '', '311-revision-v1', '', '', '2019-01-05 10:02:26', '2019-01-05 10:02:26', '', 311, 'http://xuatkhaulaodong99.vn/311-revision-v1/', 0, 'revision', '', 0),
(313, 1, '2019-01-05 10:04:59', '2019-01-05 10:04:59', '', 'Giỏ hàng', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', '2019-01-05 10:04:59', '2019-01-05 10:04:59', '', 310, 'http://xuatkhaulaodong99.vn/310-revision-v1/', 0, 'revision', '', 0),
(315, 1, '2019-01-06 02:06:27', '2019-01-06 02:06:27', '<label> Tên của bạn (bắt buộc)\n    [text* your-name] </label>\n\n<label> Địa chỉ Email (bắt buộc)\n    [email* your-email] </label>\n\n<label> Tiêu đề:\n    [text your-subject] </label>\n\n<label> Thông điệp\n    [textarea your-message] </label>\n\n[submit \"Gửi đi\"]\nCÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX \"[your-subject]\"\nCÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX <wordpress@xuatkhaulaodong99.vn>\nGửi đến từ: [your-name] <[your-email]>\nTiêu đề: [your-subject]\n\nNội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX (http://xuatkhaulaodong99.vn)\nnvthiet95@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nCÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX \"[your-subject]\"\nCÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX <wordpress@xuatkhaulaodong99.vn>\nNội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX (http://xuatkhaulaodong99.vn)\n[your-email]\nReply-To: nvthiet95@gmail.com\n\n0\n0\nXin cảm ơn, form đã được gửi thành công.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nCó một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nBạn phải chấp nhận điều khoản trước khi gửi form.\nMục này là bắt buộc.\nNhập quá số kí tự cho phép.\nNhập ít hơn số kí tự tối thiểu.', 'Form liên hệ 1', '', 'publish', 'closed', 'closed', '', 'form-lien-he-1', '', '', '2019-01-06 02:06:27', '2019-01-06 02:06:27', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=wpcf7_contact_form&p=31.html', 0, 'wpcf7_contact_form', '', 0),
(317, 1, '2019-01-06 04:43:44', '2019-01-06 04:43:44', '', 'Cảm ơn', '', 'publish', 'closed', 'closed', '', 'cam-on', '', '', '2019-01-06 04:43:44', '2019-01-06 04:43:44', '', 0, 'http://xuatkhaulaodong99.vn/?page_id=317', 0, 'page', '', 0),
(318, 1, '2019-01-06 04:43:44', '2019-01-06 04:43:44', '', 'Cảm ơn', '', 'inherit', 'closed', 'closed', '', '317-revision-v1', '', '', '2019-01-06 04:43:44', '2019-01-06 04:43:44', '', 317, 'http://xuatkhaulaodong99.vn/317-revision-v1/', 0, 'revision', '', 0),
(334, 1, '2019-01-06 07:51:21', '2019-01-06 07:51:21', 'Chúc mừng đến với WordPress. Đây là bài viết đầu tiên của bạn. Hãy chỉnh sửa hay xóa bài viết này, và bắt đầu viết blog!', 'Chào tất cả mọi người!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-06 07:51:21', '2019-01-06 07:51:21', '', 1, 'http://xuatkhaulaodong99.vn/1-revision-v1/', 0, 'revision', '', 0),
(340, 1, '2019-01-06 16:01:36', '2019-01-06 16:01:36', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"banner\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Liên kết', 'lien-ket', 'publish', 'closed', 'closed', '', 'group_5c3225ff29b10', '', '', '2019-01-06 16:03:41', '2019-01-06 16:03:41', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=acf-field-group&#038;p=34.html', 0, 'acf-field-group', '', 0),
(341, 1, '2019-01-06 16:01:36', '2019-01-06 16:01:36', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:29:\"Ví dụ: https://youtube.com\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '', 'lien_ket', 'publish', 'closed', 'closed', '', 'field_5c32262e42a8b', '', '', '2019-01-06 16:03:41', '2019-01-06 16:03:41', '', 340, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&#038;p=34.html', 0, 'acf-field', '', 0),
(343, 1, '2019-01-18 03:58:54', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-01-18 03:58:54', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=343', 0, 'post', '', 0),
(344, 1, '2019-01-18 03:59:04', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-18 03:59:04', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=don_hang&p=34.html', 0, 'don_hang', '', 0),
(345, 1, '2019-01-18 04:44:13', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-18 04:44:13', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=34.html', 0, 'tin_tuc', '', 0),
(346, 1, '2019-01-18 04:47:04', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-18 04:47:04', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=34.html', 0, 'tin_tuc', '', 0),
(347, 1, '2019-01-18 04:50:18', '2019-01-18 04:50:18', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:14:\"tin_khuyen_mai\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:2:\"id\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', '', '', 'publish', 'closed', 'closed', '', 'field_5c415aab38641', '', '', '2019-01-18 04:50:18', '2019-01-18 04:50:18', '', 101, 'http://xuatkhaulaodong99.vn/?post_type=acf-field&p=34.html', 7, 'acf-field', '', 0),
(348, 1, '2019-01-18 04:52:15', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-18 04:52:15', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=34.html', 0, 'tin_tuc', '', 0),
(349, 1, '2019-01-18 04:53:25', '2019-01-18 04:53:25', '', 'Test', '', 'trash', 'closed', 'closed', '', 'test__trashed', '', '', '2019-01-19 01:26:03', '2019-01-19 01:26:03', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=34.html', 0, 'tin_tuc', '', 0),
(350, 1, '2019-01-19 00:54:07', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-01-19 00:54:07', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=350', 0, 'post', '', 0),
(351, 1, '2019-01-19 00:55:53', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-01-19 00:55:53', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?p=351', 0, 'post', '', 0),
(352, 1, '2019-01-19 00:56:04', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2019-01-19 00:56:04', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=35.html', 0, 'tin_tuc', '', 0),
(353, 1, '2019-01-19 00:56:18', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 00:56:18', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=banner&p=35.html', 0, 'banner', '', 0),
(354, 1, '2019-01-19 00:56:28', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2019-01-19 00:56:28', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=35.html', 0, 'tin_tuc', '', 0),
(355, 1, '2019-01-19 01:00:37', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:00:37', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=danh_muc&p=35.html', 0, 'danh_muc', '', 0),
(356, 1, '2019-01-19 01:01:42', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2019-01-19 01:01:42', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=35.html', 0, 'tin_tuc', '', 0),
(357, 1, '2019-01-19 01:01:56', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:01:56', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=35.html', 0, 'tin_tuc', '', 0),
(358, 1, '2019-01-19 01:04:16', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:04:16', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=danh_muc&p=35.html', 0, 'danh_muc', '', 0),
(359, 1, '2019-01-19 01:06:09', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:06:09', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=35.html', 0, 'tin_tuc', '', 0),
(360, 1, '2019-01-19 01:07:38', '2019-01-19 01:07:38', 'Tin tức', 'Hello mot hai', '', 'trash', 'closed', 'closed', '', 'hello-mot-hai__trashed', '', '', '2019-01-19 01:26:03', '2019-01-19 01:26:03', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=36.html', 0, 'tin_tuc', '', 0),
(361, 1, '2019-01-19 01:12:19', '2019-01-19 01:12:19', 'Mot ha ba', 'HIHI', '', 'trash', 'closed', 'closed', '', 'hihi__trashed', '', '', '2019-01-19 01:26:03', '2019-01-19 01:26:03', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&#038;p=36.html', 0, 'tin_tuc', '', 0),
(362, 1, '2019-01-19 01:16:25', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:16:25', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=36.html', 0, 'tin_tuc', '', 0),
(363, 1, '2019-01-19 01:27:30', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-01-19 01:27:30', '0000-00-00 00:00:00', '', 0, 'http://xuatkhaulaodong99.vn/?post_type=tin_tuc&p=36.html', 0, 'tin_tuc', '', 0),
(364, 1, '2019-01-23 04:43:56', '2019-01-23 04:43:56', 'Mô tả trang tin tức', 'Tin Tức', '', 'inherit', 'closed', 'closed', '', '282-revision-v1', '', '', '2019-01-23 04:43:56', '2019-01-23 04:43:56', '', 282, 'http://xuatkhaulaodong99.vn/282-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Tin mới nhất', 'tin-moi-nhat', 0),
(2, 'Menu Chính', 'menu-chinh', 0),
(3, 'Danh mục 1', 'danh-muc-1', 0),
(5, 'Tin nổi bật', 'tin-noi-bat', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(240, 5, 0),
(241, 1, 0),
(242, 5, 0),
(243, 1, 0),
(244, 1, 0),
(244, 5, 0),
(286, 2, 0),
(287, 2, 0),
(288, 2, 0),
(289, 2, 0),
(290, 2, 0),
(291, 2, 0),
(292, 2, 0),
(293, 2, 0),
(294, 2, 0),
(295, 2, 0),
(296, 2, 0),
(297, 2, 0),
(298, 2, 0),
(299, 2, 0),
(300, 2, 0),
(301, 2, 0),
(302, 2, 0),
(303, 2, 0),
(304, 2, 0),
(305, 2, 0),
(306, 2, 0),
(307, 2, 0),
(308, 2, 0),
(309, 2, 0),
(349, 3, 0),
(360, 1, 0),
(361, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'nav_menu', '', 0, 24),
(3, 3, 'danh_muc_tin_tuc', '', 0, 1),
(5, 5, 'category', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'nvthiet95'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"77acb51596fd26f78c70c74a67dc81307e53bcfa945c4deb324c00ec2b4f616b\";a:4:{s:10:\"expiration\";i:1549426403;s:2:\"ip\";s:8:\"10.0.0.9\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\";s:5:\"login\";i:1548216803;}}'),
(17, 1, 'wp_user-settings', 'editor_expand=on&libraryContent=browse&editor=tinymce&hidetb=1&galcols=3&galfile=1'),
(18, 1, 'wp_user-settings-time', '1547859974'),
(19, 1, 'wp_dashboard_quick_press_last_post_id', '343'),
(20, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"10.255.0.0\";}'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(23, 1, 'meta-box-order_don_hang', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:31:\"acf-group_5c301f04a3371,slugdiv\";s:8:\"advanced\";s:16:\"10q-post-gallery\";}'),
(24, 1, 'screen_layout_don_hang', '2'),
(25, 1, 'meta-box-order_banner', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:44:\"acf-group_5c301f04a3371,slugdiv,postimagediv\";s:8:\"advanced\";s:0:\"\";}'),
(26, 1, 'screen_layout_banner', '2'),
(27, 1, 'closedpostboxes_tin_tuc', 'a:0:{}'),
(28, 1, 'metaboxhidden_tin_tuc', 'a:4:{i:0;s:23:\"acf-group_5c301f04a3371\";i:1;s:23:\"acf-group_5c3225ff29b10\";i:2;s:7:\"slugdiv\";i:3;s:16:\"10q-post-gallery\";}'),
(29, 1, 'nav_menu_recently_edited', '2'),
(30, 2, 'nickname', 'sonstar'),
(31, 2, 'first_name', 'Son'),
(32, 2, 'last_name', 'Star'),
(33, 2, 'description', ''),
(34, 2, 'rich_editing', 'true'),
(35, 2, 'syntax_highlighting', 'true'),
(36, 2, 'comment_shortcuts', 'false'),
(37, 2, 'admin_color', 'fresh'),
(38, 2, 'use_ssl', '0'),
(39, 2, 'show_admin_bar_front', 'true'),
(40, 2, 'locale', ''),
(41, 2, 'wp_capabilities', 'a:1:{s:6:\"editor\";b:1;}'),
(42, 2, 'wp_user_level', '7'),
(43, 2, 'dismissed_wp_pointers', 'wp496_privacy'),
(45, 2, 'wp_dashboard_quick_press_last_post_id', '338'),
(46, 2, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"10.255.0.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'nvthiet95', '$P$BPRkbTBMC/yRnO4LJBVJWYkGjkzA6O/', 'nvthiet95', 'nvthiet95@gmail.com', '', '2019-01-05 01:42:18', '', 0, 'nvthiet95'),
(2, 'sonstar', '$P$Bzmgk/FmuWUnto4HbMNp4xZjvCu8ls.', 'sonstar', 'sonstar@gmail.com', 'http://sonstar@gmail.com', '2019-01-06 11:31:59', '1546774319:$P$Bj4C.7nVbgfNGJ88Mt8NcPUlEnss4s.', 0, 'Star Son');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_customer`
--
ALTER TABLE `wp_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_customer`
--
ALTER TABLE `wp_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=414;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=984;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
