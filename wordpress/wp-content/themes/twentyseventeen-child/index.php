<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <section id="container">

        <section id="slider" class="jcs4">
            <div class="jcs4-viewport">
                <?php
                $args = array(
                    'post_type'      => 'banner',
                    'posts_per_page' => 8
                );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                    echo ('
                                <a class="slide jcs4-slide" href="'.get_post_meta($loop->post->ID, 'lien_ket', true).'">
                                    <div data-effect-in="fadeIn" data-effect-out="fadeOut">
                                        <img property="og:image" itemprop="image" src="'.get_the_post_thumbnail_url($loop->post->ID, 'full').'" alt ="'.get_the_title($loop->post->ID).'" />
                                    </div>
                                </a>
                        ');
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <a class="jcs4-prev"></a>
            <a class="jcs4-next"></a>
            <div class="jcs4-pages"></div>
        </section>
        <section class="main">
            <article id="content-index">
                <h1 style="position: absolute;left:-9999px;">CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX</h1>
                <div class="block-news featured">
                    <h2><a href="/san-pham-noi-bat.html" title="Đơn hàng nổi bật">Đơn hàng nổi bật</a></h2>
                    <?php
                    $args = array(
                        'post_type'      => 'don_hang',
                        'posts_per_page' => 8
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                        echo ('
                                    <div class="product-block-inner">
                                        <div class="product-info">
                                            <h3><a href="'.get_permalink($loop->post->ID).'">'.get_the_title().'</a></h3>
                                            <p><span>Số lượng: </span><strong>'.get_post_meta($loop->post->ID, 'gioi_tinh', true).'</strong></p>
                                            <p><span>Nơi làm việc: </span><strong>'.get_post_meta($loop->post->ID, 'noi_lam_viec', true).'</strong></p>
                                            <p><span>Lương cơ bản: </span><strong>'.get_post_meta($loop->post->ID, 'luong', true).'</strong></p>
                                            <p><span>Ngày tuyển: </span><strong>'.get_post_meta($loop->post->ID, 'thoi_gian_tuyen', true).'</strong></p>
                                            <p><span>Liên hệ: </span><a href="tel:'.get_post_meta($loop->post->ID, 'so_dien_thoai', true).'" style="font-size:16px" class="color2"><strong>'.get_post_meta($loop->post->ID, 'so_dien_thoai', true).'</strong></a> <br>
                                                Để được tư vấn chi tiết</p>
                                        </div>
                                        <div class="image"> 
                                            <a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'"> 
                                                <img style="object-fit: cover; width: 100%;" src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'"> 
                                                <span class="hover-image"></span>
                                             </a> 
                                         </div>
                                        <div class="contact">
                                            <form action="/thong-tin-dich-vu/them/'.$loop->post->ID.'">
                                                <input type="submit" value="Đăng ký ngay" class="button" />
                                            </form>
                                        </div>
                                    </div>
                                ');
                    endwhile;

                    wp_reset_query();
                    ?>
                    <div class="clear"></div>
                </div>
                <div class="block-news">
                    <div class="list-article-news">
                        <div class="content-info"> <p>
                                <iframe frameborder="0" height="230" scrolling="no" src="https://www.youtube.com/embed/b-E_-_0bYn8" width="380"></iframe></p>
                            <h2>
                                Bạn quan tâm tới điều gì?</h2>
                            <ul>
                                <li>
                                    Bạn muốn đi&nbsp;xuất khẩu lao động Nhật Bản?</li>
                                <li>
                                    Đã&nbsp;thi tuyển&nbsp;qua nhiều đơn vị nhưng chưa đạt?</li>
                                <li>
                                    Mong muốn tìm được một&nbsp;công ty tư vấn uy tín&nbsp;tại hà nội cũng như miền bắc?</li>
                                <li>
                                    Một bản&nbsp;báo giá đơn hàng&nbsp;với chi phí rẻ nhất, chi tiết nhất?</li>
                                <li>
                                    Thủ tục&nbsp;hồ sơ&nbsp;đơn giản, tiện lợi?</li>
                                <li>
                                    Được&nbsp;thi tuyển&nbsp;thường xuyên và liên tục?</li>
                                <li>
                                    Đi nước ngoài để học hỏi, phát triển sự nghiệp?</li>
                                <li>
                                    Hay đơn giản là bạn đang muốn đi nước ngoài</li>
                            </ul>
                            <p>
                                Vậy hãy liên hệ ngay với chúng tôi theo số điện thoại: <span style="color:#000080;"><span style="font-size:18px;"><strong>0982.265.235</strong></span></span>&nbsp;để được tư vấn giải đáp thắc mắc.</p>
                            <h3>
                                CHÚNG TÔI CAM KẾT:</h3>
                            <ul>
                                <li>
                                    Công ty hợp tác trực tiếp với nghiệp đoàn bên Nhật, phong cách&nbsp;chuyên nghiệp&nbsp;và&nbsp;tin cậy</li>
                                <li>
                                    Liên tục tổ chức các đợt&nbsp;thi tuyển&nbsp;trong tháng</li>
                                <li>
                                    Người lao động được làm giấy tờ,&nbsp;thủ tục&nbsp;nhanh chóng và đầy đủ</li>
                                <li>
                                    Người lao động được&nbsp;đào tạo bài bản, tay nghề cao</li>
                                <li>
                                    Đặc biệt&nbsp;miễn giảm 100%&nbsp;chi phí đào tạo cho&nbsp;lao động nữ</li>
                            </ul>
                            <p style="text-align:center">
                                <img alt="Xuất khẩu lao động Nhật Bản" src="/wp-content/uploads/2019/01/bn_content.png" /></p>
                        </div>
                    </div>
                    <div class="table-product-news">
                        <h2 class="new"><span>Đơn hàng mới nhất</span></h2>
                        <div class="box-table">
                            <table class="name-product-new">
                                <tbody>
                                <tr>
                                    <th class="text-left">Ngành nghề</th>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Nơi làm việc</th>
                                    <th class="text-center">Lương</th>
                                    <th class="text-center">Ngày tuyển</th>
                                </tr>
                                <?php
                                $args = array(
                                    'post_type'      => 'don_hang',
                                    'posts_per_page' => 8
                                );
                                $loop = new WP_Query( $args );
                                while ( $loop->have_posts() ) : $loop->the_post();
                                    echo ('
                                            <tr>
                                                <td class="text-left uppercase"><a href="'.get_permalink($loop->post->ID).'" target="_self" title="'.get_the_title().'">'.get_the_title().'</a></td>
                                                <td class="text-center">'.get_post_meta($loop->post->ID, 'gioi_tinh', true).'</td>
                                                <td>'.get_post_meta($loop->post->ID, 'noi_lam_viec', true).'</td>
                                                <td class="nowrap">'.get_post_meta($loop->post->ID, 'luong', true).'</td>
                                                <td>'.get_post_meta($loop->post->ID, 'thoi_gian_tuyen', true).'</td>
                                            </tr>
                                        ');
                                endwhile;

                                wp_reset_query();
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="register-online">
                            <a href="/thong-tin-dich-vu/"><img src="/wp-content/uploads/2019/01/dang-ky-truc-tuyen.png" alt="đăng ký trực tuyến"></a>
                        </div>
                        <div class="new-article">
                            <div class="main">
                                <h3><a href="/tin-moi-nhat/" title="Tin tức mới nhất">Tin tức mới nhất</a></h3>
                                <ul class="news-item">
                                    <?php
                                    $args = array(
                                        'post_type'      => 'tin_tuc',
                                        'posts_per_page' => 6,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'category',
                                                'field' => 'id',
                                                'terms' => 1
                                            )
                                        )
                                    );
                                    $loop = new WP_Query( $args );
                                    while ( $loop->have_posts() ) : $loop->the_post();
                                        global $product;
                                        echo ('
                                                <li>
                                                    <div class="news-img"> <a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'"> 
                                                    <img style="object-fit: cover; width: 100%;" src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'" /> <span class="hover-img"></span> </a> </div>
                                                    <div class="news-title">
                                                        <h4><a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a></h4>
                                                    </div>
                                                </li>
                                        ');
                                    endwhile;

                                    wp_reset_query();
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="new-article featured">
                    <div class="main">
                        <h3><a href="/tin-noi-bat/" title="Tin tức mới nhất">Tin nổi bật</a></h3>
                        <ul class="news-item">
                            <?php
                            $args = array(
                                'post_type'      => 'tin_tuc',
                                'posts_per_page' => 6,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'category',
                                        'field' => 'id',
                                        'terms' => 5
                                    )
                                )
                            );
                            $loop = new WP_Query( $args );
                            while ( $loop->have_posts() ) : $loop->the_post();
                                global $product;
                                echo ('
                                        <li>
                                            <div class="news-img" style="float:left;"> 
                                                <a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'"> 
                                                    <img style="object-fit: cover; width: 100%;" src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'" /> 
                                                    <span class="hover-img"></span> 
                                                </a> 
                                            </div>
                                            <div class="news-title">
                                                <h4><a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a></h4>
                                            </div>
                                            <div class="news-details">
                                                <p>'.get_the_excerpt().'</p>
                                            </div>
                                        </li>
                                        ');
                            endwhile;

                            wp_reset_query();
                            ?>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
            </article>
        </section>

    </section>

<?php get_footer();
