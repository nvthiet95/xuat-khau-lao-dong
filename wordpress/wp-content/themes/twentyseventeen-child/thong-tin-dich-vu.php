<?php
/*
Template Name: Trang thông tin dịch vụ
*/
?>
<?php get_header(); ?>
    <section id="container">
        <div class="main">
            <nav id="breadcumds">
                <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                            <span itemprop="title">Trang chủ</span>
                        </a>
                    </li>
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Thông tin dịch vụ" href="http://xuatkhaulaodong99.com/thong-tin-dich-vu.html"
                           itemprop="url">
                            <span itemprop="title">Thông tin dịch vụ</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <article id="div-shopping">
                <section id="cart">
                    <div id="heading">
                        <h2 class="info-product">Thông tin đăng ký</h2>
                    </div>
            <?php
            global $product_prefix;
            $action = get_query_var('action'); //thêm|xóa
            $pro_id = get_query_var('pro_id'); //id sản phẩm
            $quantity =  1; //số lượng sản phẩm
            if(isset($_GET['quantity'])) {
                $quantity = $_GET['quantity'];
            }
            if ($action){
                switch ($action) {
                    case 'them':
                        if (isset($_SESSION['cart'][$pro_id])) //nếu đã có thì cập nhật số lượng thêm 1
                            $quantity = $_SESSION['cart'][$pro_id] + $quantity;
                        else
                            $_SESSION['cart'][$pro_id] = $quantity; //cập nhật lại
                        wp_redirect(get_bloginfo('url') . '/thong-tin-dich-vu/');
                        exit();
                        break;
                    case 'sua':
                        if(isset($_POST['cart'])) {
                            foreach ($_POST['cart'] as $pro_id => $quantity) {
                                if ($quantity == 0) unset($_SESSION['cart'][$pro_id]);
                                else $_SESSION['cart'][$pro_id] = $quantity;
                            }
                        }
                        wp_redirect(get_bloginfo('url') . '/thong-tin-dich-vu/');
                        exit(); //trả về trang hiển thị giỏ hàng
                        break;
                    case 'xoa':
                        if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) { //kiểm tra và xóa sản phẩm dựa vào id
                            unset($_SESSION['cart'][$pro_id]);
                            wp_redirect(get_bloginfo('url') . '/thong-tin-dich-vu/');
                            exit();
                        } else {
                            unset($_SESSION['cart']);
                            echo "<h3>Hiện chưa có sản phẩm nào trong giỏ hàng! <a href='" . get_bloginfo('url') . "'>Bấm vào đây</a> để xem và mua hàng.</h3>";
                        }
                        break;
                }
            }else{ //không có thao tác thêm hoặc xóa thì sẽ hiển thị sản phẩm trong giỏ hàng
            ?>
            <?php
            if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) { //kiểm tra số lượng sản phẩm trước khi hiển thị

            ?>
                <form action="/thong-tin-dich-vu/sua/" method="POST">
                    <table id="table" border="0" cellpadding="10px" cellspacing="1px">
                        <tbody>
                        <tr id="main_heading">
                            <td>STT</td>
                            <td>Đơn hàng</td>
                            <td>Lương cơ bản</td>
                            <td>Số lượng</td>
                            <td align="center">Xóa</td>
                        </tr>
                        <?php
                        $total = 0;
                        foreach ( $_SESSION['cart'] as $pro_id => $quantity ){ //lặp qua mảng cart session lấy ra id và show thông tin sản phẩm theo id đó
                            $product = get_post((int)$pro_id );
                            if(get_field('price', $pro_id)!=0){
                                $price = get_field('price', $pro_id);}else{
                                $price = get_field('price_old', $pro_id);
                            }
                            ?>
                            <tr class="content-cart">
                                <td style="width:50px;"><?php if ( has_post_thumbnail( $pro_id ) ) echo get_the_post_thumbnail( $pro_id, array( 50, 50 ) ); else echo "<img src='".get_bloginfo('template_url')."/images/no_img.png' style='width:50px;height:50px;' />"; ?></td>
                                <td><a target="_blank" href="<?php echo get_permalink($pro_id);?>"><?php echo $product->post_title; ?></a></td>
                                <td>
                                    <?php echo get_post_meta($pro_id, 'luong', true) ?>
                                </td>
                                <td align="center">
                                    <span class="add-to-cart">
                                        <span class="dec add">-</span>
                                        <input type="text" name="cart[<?php echo $pro_id; ?>]" value="<?php echo $quantity; ?>" maxlength="4" size="1" id="qty" style="text-align: right">
                                        <span class="add">+</span>
                                    </span>
                                </td>

                                <?php  $total += $price * $quantity;   $_POST['tien'] = number_format($total).' đ'; ?>
                                <td align="center">
                                    <a href="<?php echo get_bloginfo( 'url' ) . '/thong-tin-dich-vu/xoa/' . $pro_id; //link xóa sản phẩm trong giỏ ?>">
                                        <img src="http://xuatkhaulaodong99.com/public/upload/img/cart_cross.jpg"
                                             width="25px" height="20px">
                                    </a>
                                </td>
                                </td>
                            </tr>
                            <?php
                            $check .= $product->post_title. " " ."(".$quantity.")". " ,";
                            $_POST['name'] = $check;
                        }
                        ?>
                        <tr>

                            <td colspan="3"></td>
                            <td colspan="1" align="center"><input class="button teal" type="submit" value="Cập nhật">
                            </td>
                            <td><input class="button teal" type="button" value="Xóa" onclick="clear_cart()"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
                <?php
                } else {
                echo '<div id="text">Giỏ hàng của bạn hiện đang trống !  </div>';
                }
                ?>
                <?php } ?>
                </section>
                <section id="bill_info">
                    <h2>Thông tin đăng ký</h2>
                    <div class="error">
                        <ul>
                        </ul>
                    </div>
                    <form id="billing" action="<?php echo esc_attr( admin_url('admin-post.php') ); ?>" method="POST" novalidate="novalidate">
                        <input type="hidden" name="product" value='<?php echo json_encode($_SESSION['cart']) ?>' />
                        <input type="hidden" name="action" value="add_customer" />
                        <div class="cols">
                            <table border="0" cellpadding="2px">
                                <tbody>
                                <tr>
                                    <td>Người đăng ký: <span style="color: red;font-size:12px">(*)</span></td>
                                    <td><input type="text" name="name" value="" arequired=""></td>
                                </tr>
                                <tr>
                                    <td>Số điện thoại: <span style="color: red;font-size:12px">(*)</span></td>
                                    <td><input type="text" name="phone" value="" required="" aria-required="true"></td>
                                </tr>
                                <tr>
                                    <td>Địa chỉ: <span style="color: red;font-size:12px">(*)</span></td>
                                    <td><textarea name="address" rows="5" required="" aria-required="true"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cols-details">
                            <b>Thêm ghi chú </b><i>(nếu có)</i>: <br>
                            <textarea id="supercheckout-details_order" name="note" rows="8"></textarea>
                            <br>
                            <input class="button" type="submit" value="Xác nhận">
                        </div>
                        <div class="clear"></div>
                    </form>
                </section>
            </article>
        </div>
        <script type="text/javascript">
            function clear_cart() {
                var result = confirm('Bạn muốn xóa giỏ hàng ?');
                if (result) {
                    window.location = "/gio-hang/xoahet/";
                }
                else {
                    return false;
                }
            }
        </script>
    </section>
<?php get_footer() ?>