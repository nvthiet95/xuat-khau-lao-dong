function scrollMouse(classFirst, nameClassAdd) {
	$(classFirst).addClass('hidden');
	$(classFirst).each(function() {
		var spaceTop = $(this).offset().top;
		var chieucao = $(this).height();
		var spaceBottom = $(document).height() - $(this).offset().top - chieucao;
		var scrollBottom = $(document).height() + $(window).height() - $(window).scrollTop();
		var scrollSpaceTop = $(window).scrollTop();
		if ((spaceBottom < scrollBottom - $(window).height()) && (spaceTop < scrollSpaceTop + $(window).height())) {
			$(this).addClass(nameClassAdd);
		}
	});
}
var width = $(window).width();
if (width > 767) {
	$(window).scroll(function() {
		scrollMouse('#div-article h2, #div-article h3, #div-article h4, #div-article li', 'show1');
	});
}