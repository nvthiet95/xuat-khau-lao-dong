$().ready(function() {
    $("#billing").validate({
        rules: {
			info: {
                required: true,
            },
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 6
            },
            address: {
                required: true,
                minlength: 6
            },
        },
        messages: {
			info: {
                required: "Vui lòng chọn dịch vụ !",
            },
            name: {
                required: "Vui lòng điền họ và tên !",
            },
            email: {
                required: "Vui lòng nhập email",
                email: "Địa chỉ nhập vào không phải là địa chỉ email"
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Dãy nhập vào không phải là số",
                minlength: "Số điện thoại tối thiểu 6 ký tự"
            },
            address: {
                required: "Vui lòng nhập địa chỉ",
                minlength: "Địa chỉ quá ngắn !"
            },
        }
    });
	$("#test-reg").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 6
            },
            address: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            name: {
                required: "Vui lòng điền họ và tên !",
            },
            email: {
                required: "Vui lòng nhập email",
                email: "Địa chỉ nhập vào không phải là địa chỉ email"
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Dãy nhập vào không phải là số",
                minlength: "Số điện thoại tối thiểu 6 ký tự"
            },
            address: {
                required: "Vui lòng nhập địa chỉ",
                minlength: "Địa chỉ quá ngắn !"
            },
        }
    });
	$("#reg-bg").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 6
            },
            address: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            name: {
                required: "Vui lòng điền họ và tên !",
            },
            email: {
                required: "Vui lòng nhập email",
                email: "Địa chỉ nhập vào không phải là địa chỉ email"
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Dãy nhập vào không phải là số",
                minlength: "Số điện thoại tối thiểu 6 ký tự"
            },
            address: {
                required: "Vui lòng nhập địa chỉ",
                minlength: "Địa chỉ quá ngắn !"
            },
        }
    });
	$("#reg-tg").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 6
            },
            address: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            name: {
                required: "Vui lòng điền họ và tên !",
            },
            email: {
                required: "Vui lòng nhập email",
                email: "Địa chỉ nhập vào không phải là địa chỉ email"
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Dãy nhập vào không phải là số",
                minlength: "Số điện thoại tối thiểu 6 ký tự"
            },
            address: {
                required: "Vui lòng nhập địa chỉ",
                minlength: "Địa chỉ quá ngắn !"
            },
        }
    });
	$("#reg-lt").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true,
                minlength: 6
            },
            address: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            name: {
                required: "Vui lòng điền họ và tên !",
            },
            email: {
                required: "Vui lòng nhập email",
                email: "Địa chỉ nhập vào không phải là địa chỉ email"
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Dãy nhập vào không phải là số",
                minlength: "Số điện thoại tối thiểu 6 ký tự"
            },
            address: {
                required: "Vui lòng nhập địa chỉ",
                minlength: "Địa chỉ quá ngắn !"
            },
        }
    });
});