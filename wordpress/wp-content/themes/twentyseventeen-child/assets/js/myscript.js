jQuery(function() {
    jQuery(".add").click(function() {
        var jQueryadd = jQuery(this);
        var oldValue = jQueryadd.parent().find("#qty").val();
        var newVal = 0;

        if (jQueryadd.text() == "+") {
            newVal = parseFloat(oldValue) + 1;
            // AJAX save would go here
        } else {
            // Don't allow decrementing below zero
            if (oldValue >= 1) {
                newVal = parseFloat(oldValue) - 1;
                // AJAX save would go here
            }
            if(oldValue == 0){
                newVal = parseFloat(oldValue);
            }
        }
        jQueryadd.parent().find("#qty").val(newVal);
    });


});
$('.category-list .list').find('.active').parent().parent().parent().css('display','block');
$('.category-list .list').find('.active').parent().parent().parent().prev().addClass('active');
var a = $(window).width();
function t(t) {
    timer = setTimeout(function() {
        1 == $(t).next().is(":hidden") ?
            ($(t).next().stop().slideDown(300), $(t).addClass("active"), $(t).parent().addClass("active")) :
            ($(t).next().stop().slideUp(300), $(t).removeClass("active"), $(t).parent().removeClass("active"))
    }, 200)
}
a > 1024 ? $(".category-list .category-title").hover(function() {
    t(this)
}, function() {
    clearTimeout(timer)
}) : $(".category-list .category-title").click(function() {
    t(this)
});
$(".fired-popup").click(function(){
    $(".modal").hide();
    $(".fired-popup").hide();
});


function closenotifi() {
    $('.success, .warning, .attention, information, .error').hide();
}

var article = {
    url: typeof article_url != "undefined" ? article_url : window.location.href.replace(window.location.hash, ""),
    title: typeof article_title != "undefined" ? article_title : "",
    description: typeof article_description !=
    "undefined" ? article_description : "",
    image: typeof article_image != "undefined" ? article_image : ""
};
function urlEncode(str) {
    str = (str + "").toString();
    return encodeURIComponent(str).replace(/#!/g, "%23").replace(/!/g, "%21").replace(/'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29").replace(/\*/g, "%2A").replace(/%20/g, "+")
}
$(document).ready(function() {
    $('.link-item').click(function(){
        var url = $(this).attr("data-link");
        window.location.replace(url);
    });
    $(window).scroll(function(){
        120<=$(window).scrollTop()?$("#htop, #htop2").css('position','fixed'):$("#htop, #htop2").css('position','inherit');
    });
    // Colorlist
    var $color = $('.colorlist'),
        $img = $('.imagelist');
    $img.find('img').hide();
    $img.find('span').hide();
    $color.find('li').click(function(){
        var id = $(this).data('id');
        $img.find('img').hide();
        $img.find('img[color='+id+']').show();
        $img.find('span').hide();
        $img.find('span[color='+id+']').show();
    });
    $color.find('li:first').click();

// Floating Sidebar

    var $floatbar = $('#menu-top');
    var $sidebar = $('#side_bar .sales');
    $footer = $('footer');
    $sidebar.width($sidebar.width());
    var sbWatcher = scrollMonitor.create($sidebar, {
        top: $floatbar.height()
    });
    sbWatcher.lock();
    sbWatcher.stateChange(function() {
        //$sidebar.toggleClass('fixed', this.isAboveViewport)
        if(this.isAboveViewport) {
            if(!ftWatcher.isAboveViewport) {
                $sidebar.addClass('fixed');
            }
        }
        else {
            $sidebar.removeClass('fixed');
        }
    });

// Watch an area above the footer as tall as the sidebar
    var ftWatcher = scrollMonitor.create($footer, {
        top: $floatbar.height() + $sidebar.height()
    });
    ftWatcher.fullyEnterViewport(function() {
        if (ftWatcher.isAboveViewport) {
            $sidebar.removeClass('fixed');
        }
    });
    ftWatcher.partiallyExitViewport(function() {
        if (!ftWatcher.isAboveViewport) {
            $sidebar.addClass('fixed');
        }
    });

    if(sbWatcher.isAboveViewport) {
        $sidebar.addClass('fixed');
    }
    if (ftWatcher.isAboveViewport) {
        $sidebar.removeClass('fixed');
    }


    var galyid = 'galy';
    $('a[rel="'+galyid+'"]').fancybox({
        padding : 0,
        margin : [20, 16, 20, 16],
        openEffect : 'fade',
        closeEffect : 'elastic',
        nextEffect:'fade',
        prevEffect:'fade',
        closeBtn: true,
        nextClick: true,
        helpers:  {
            title : {
                type : 'outside'
            },
            overlay : {
                css : {'background':'rgba(0,0,0,0.5)'}
            }
        }
    });

    setTimeout(function(){
        $('.contact-last').show();
    }, 10000);


    $('button#popup-bg').click(function() {
        var form = $("#reg-bg");
        form.validate();
        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: "/shopping/test_reg_ajax",
                data: {
                    reginfo: $('#reginfo_id_bg').val(),
                    name: $('#name-bg').val(),
                    phone: $('#phone-bg').val(),
                    infoproduct: $('#infoproduct-bg').val(),
                    email: $('#email-bg').val(),
                    address: $('#address-bg').val(),
                    plan: $('#plan-bg').val(),
                    formality: $('#formality-bg').val(),
                    infocustomer: $('#infocustomer-bg').val()
                },
                beforeSend: function() {
                    $(".thongbao-lienhe").html("<span class='loading'></span>");
                },
                success: function(html) {
                    $(".thongbao-lienhe").html("");
                    if (html == false) {
                        alert('ÄÃ£ cÃ³ lá»—i xáº£y ra !');
                    } else {
                        $('.modal.modal-fixed-footer .modal-content').animate({scrollTop:0},0);
                        $(".thongbao-lienhe").html(html);
                        $('.form-data input').val("");

                    }
                }
            });
        }
    });
    $('button#popup-tg').click(function() {
        var form = $("#reg-tg");
        form.validate();
        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: "/shopping/test_reg_ajax",
                data: {
                    reginfo: $('#reginfo_id_tg').val(),
                    name: $('#name-tg').val(),
                    phone: $('#phone-tg').val(),
                    infoproduct: $('#infoproduct-tg').val(),
                    email: $('#email-tg').val(),
                    address: $('#address-tg').val(),
                    plan: $('#plan-tg').val(),
                    formality: $('#formality-tg').val(),
                    infocustomer: $('#infocustomer-tg').val()
                },
                beforeSend: function() {
                    $(".thongbao-lienhe1").html("<span class='loading'></span>");
                },
                success: function(html) {
                    $(".thongbao-lienhe1").html("");
                    if (html == false) {
                        alert('ÄÃ£ cÃ³ lá»—i xáº£y ra !');
                    } else {
                        $('.modal.modal-fixed-footer .modal-content').animate({scrollTop:0},0);
                        $(".thongbao-lienhe1").html(html);
                        $('.form-data input').val("");

                    }
                }
            });
        }
    });
    $('button#popup-lt').click(function() {
        var form = $("#reg-lt");
        form.validate();
        if (!form.valid()) {
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: "/shopping/test_reg_ajax",
                data: {
                    reginfo: $('#reginfo_id_lt').val(),
                    name: $('#name-lt').val(),
                    phone: $('#phone-lt').val(),
                    infoproduct: $('#infoproduct-lt').val(),
                    email: $('#email-lt').val(),
                    address: $('#address-lt').val(),
                    plan: $('#plan-lt').val(),
                    formality: $('#formality-lt').val(),
                    infocustomer: $('#infocustomer-lt').val()
                },
                beforeSend: function() {
                    $(".thongbao-lienhe2").html("<span class='loading'></span>");
                },
                success: function(html) {
                    $(".thongbao-lienhe2").html("");
                    if (html == false) {
                        alert('ÄÃ£ cÃ³ lá»—i xáº£y ra !');
                    } else {
                        $('.modal.modal-fixed-footer .modal-content').animate({scrollTop:0},0);
                        $(".thongbao-lienhe2").html(html);
                        $('.form-data input').val("");

                    }
                }
            });
        }
    });

    $(window).scroll(function(){
        240<=$(window).scrollTop()?(
            $("#menu-top").css({'position':'fixed'})
        ):(
            $("#menu-top").css({'position':'inherit'})
        );
    });
    function t(t) {
        timer = setTimeout(function() {
            1 == $(t).parent().next().is(":hidden") ? ($(t).parent().next().stop().slideDown(300), $(t).parent().addClass("active")) : ($(t).parent().next().stop().slideUp(300), $(t).parent().removeClass("active"))
        }, 200)
    }
    var a = $(window).width();
    a > 1024 ? $(".category-list .category-title > span").hover(function() {
        t(this)
    }, function() {
        clearTimeout(timer)
    }) : $(".category-list .category-title > span").click(function() {
        t(this)
    });
    if (a > 1024) {
        $banner = $('.banner_doc a');
        $window = $(window);
        $topDefault = parseFloat($banner.css('top'), '1');
        $window.scroll(function() {
            $top = $(this).scrollTop();
            $banner.stop().animate({
                top: $top + $topDefault
            }, 800, 'easeOutCirc');
        });
    }
    $('#wrapper').append('<span href="javascript:;" id="to_top" style="display: inline;" rel="nofollow" >'
        +'<img src="/wp-content/uploads/2019/01/back-top.png" alt="Về đầu trang">'
        +'</span>');
    $(window).scroll(function(){
        200<=$(window).scrollTop()?$("#to_top, .tick_normal").fadeIn():$("#to_top, .tick_normal").fadeOut();
    });
    $(function(){
        $("#to_top").click(function(){
            $("body,html").animate({scrollTop:0},"normal");
        });
    });


    var is_plag = false;
    $('.btn_menu').click(function(){


        if(is_plag == false){
            //$('body,html').animate({scrollTop:0},0);
            $('.fixed-mb').show();
            $('#wrapLoader').animate({left: 220}, 300);
            $(this).parent().animate({left: 220}, 300);
            is_plag = true;
        }else{
            $('.fixed-mb').hide();
            $('#wrapLoader').animate({left: 0}, 300);
            $(this).parent().animate({left: "0"}, 300);
            is_plag = false;
        }
    });
    $('.fixed-mb').click(function(){
        $(this).hide();
        $('#wrapLoader').animate({left: 0}, 300);
        $('#htop').animate({left: "0"}, 300);
    });

    $("a.btn_facebook").click(function(e) {
        var url = "https://www.facebook.com/sharer/sharer.php?u=" + urlEncode(article.url) + "&p[images][0]=" + article.image;
        var newwindow = window.open(url, "_blank", "menubar=no,toolbar=no,resizable=no,scrollbars=no,height=450,width=710");
        if (window.focus) newwindow.focus();
        e.preventDefault()
    });
    $("a.btn_google").click(function(e) {
        var url = "https://plus.google.com/share?url=" +
            urlEncode(article.url);
        var newwindow = window.open(url, "_blank", "menubar=no,toolbar=no,resizable=no,scrollbars=no,height=450,width=520");
        if (window.focus) newwindow.focus();
        e.preventDefault()
    });
    $("a.btn_twitter").click(function(e) {
        var url = "https://twitter.com/intent/tweet?source=webclient&text=" + article.title + "+" + urlEncode(article.url);
        var newwindow = window.open(url, "_blank", "menubar=no,toolbar=no,resizable=no,scrollbars=no,height=450,width=710");
        if (window.focus) newwindow.focus();
        e.preventDefault()
    });
    $('header .search').addClass('nodisplay');
    $('.search-icond').addClass('nodisplay').fadeIn(1000).removeClass('nodisplay');
    $('.search-icond').click(function() {
        if ($('header .search').hasClass('nodisplay') == true) {
            $('header .search').fadeIn(400).removeClass('nodisplay');
            $(this).css('display', 'none');
        } else {
            $('header .search').fadeOut(400).addClass('nodisplay');
            $('#banner').css('margin-top', '0');
        }
    });
    $("#pagination-shopping a").click(function() {
        var t = $(this).attr("href");
        return $.ajax({
            type: "POST",
            url: t,
            data: "ajax",
            async: !0,
            beforeSend: function() {
            },
            success: function(t) {
                $("#div-product").html(t)
            }
        }), !1
    })
    $(window).scroll(function() {
        $('header .search').each(function() {
            var abcd = $(this).offset().right;
            var aaaa = $(window).scrollTop();
            if (aaaa > 10) {
                $('header .search').addClass('nodisplay');
                $('.search-icond').css('display', 'block');
                $(this).css('display', 'none');
            }
        });
    });
    $(".ratingblock li a").click(function(){
        var url = $(this).attr("vote-item");
        $.ajax({
            type: "POST",
            url: url,
            data: "ajax",
            async: true,
            beforeSend: function(){
                $(".unit-rating").html("<div class='loading'></div>");
            },
            success: function(kq){
                $(".blog-cate-vote").html(kq);
                alert("Thanks for voting !");
            }
        })
        return false;
    });
    jQuery.scrollSpeed = function(step, speed, easing) {

        var $document = $(document),
            $window = $(window),
            $body = $('html, body'),
            option = easing || 'default',
            root = 0,
            scroll = false,
            scrollY,
            scrollX,
            view;

        if (window.navigator.msPointerEnabled)

            return false;

        $window.on('mousewheel DOMMouseScroll', function(e) {

            var deltaY = e.originalEvent.wheelDeltaY,
                detail = e.originalEvent.detail;
            scrollY = $document.height() > $window.height();
            scrollX = $document.width() > $window.width();
            scroll = true;

            if (scrollY) {

                view = $window.height();

                if (deltaY < 0 || detail > 0)

                    root = (root + view) >= $document.height() ? root : root += step;

                if (deltaY > 0 || detail < 0)

                    root = root <= 0 ? 0 : root -= step;

                $body.stop().animate({

                    scrollTop: root

                }, speed, option, function() {

                    scroll = false;

                });
            }

            if (scrollX) {

                view = $window.width();

                if (deltaY < 0 || detail > 0)

                    root = (root + view) >= $document.width() ? root : root += step;

                if (deltaY > 0 || detail < 0)

                    root = root <= 0 ? 0 : root -= step;

                $body.stop().animate({

                    scrollLeft: root

                }, speed, option, function() {

                    scroll = false;

                });
            }

            return false;

        }).on('scroll', function() {

            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();

        }).on('resize', function() {

            if (scrollY && !scroll) view = $window.height();
            if (scrollX && !scroll) view = $window.width();

        });
    };

    jQuery.easing.default = function(x, t, b, c, d) {

        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    };
    //jQuery.scrollSpeed(120, 1000);
    $(document).ready(function () {
        $('.jcs4-controls').click(() => {
            alert('a');
        });
    })
});
function continues() {
    location.href = '/gio-hang/';
}