<?php
/*
Template Name: Trang danh sách đơn hàng
*/
?>
<?php get_header();?>
    <section id="container">
        <section id="container">
            <div class="main">
                <nav id="breadcumds">
                    <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                                <span itemprop="title">Trang chủ</span>
                            </a>
                        </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Giới thiệu" href="<?php get_permalink() ?>"  itemprop="url">
                                <span itemprop="title"><?php echo get_the_title() ?></span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <aside id="side_bar">
                    <div class="category-list">
                        <ul>
                            <?php
                            $menu_name = 'top';
                            $locations = get_nav_menu_locations();
                            $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                            $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
                            $count = 0;
                            $submenu = false;

                            foreach( $menuitems as $item ):
                                $title = $item->title;
                                $link = $item->url;
                                if ( !$item->menu_item_parent ):
                                    $parent_id = $item->ID;
                                    ?>
                                    <li>
                                    <div class="category-title">
                                        <a href="<?php echo $link; ?>">
                                            <span><?php echo $title; ?></span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $parent_id == $item->menu_item_parent ): ?>
                                <?php if ( !$submenu ): $submenu = true; ?>
                                    <div class="list"><ul>
                                <?php endif; ?>
                                <li>
                                    <a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
                                </li>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                    </ul></div>
                                    <?php $submenu = false; endif; ?>
                            <?php endif; ?>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                                </li>
                                <?php $submenu = false; endif; ?>
                                <?php $count++; endforeach; ?>


                            <?php $product_categories_1 = get_categories( ['taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] ); ?>
                            <?php foreach ($product_categories_1 as $item_1) {
                                if($item_1->cat_ID !== 15) {
                                    echo '<li><div class="category-title"><a href="'.get_category_link($item_1->cat_ID).'"  title="'.$item_1->cat_name.'"><span>'.$item_1->cat_name.'</span></a></div>';
                                    $product_categories_2 = get_categories( ['taxonomy' => 'product_cat', 'parent' => $item_1->cat_ID, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] );
                                    if(count($product_categories_2) > 0) {
                                        echo '<div class="list"><ul>';
                                        foreach ($product_categories_2 as $item_2) {
                                            echo '<li><a title="'.$item_2->cat_name.'" href="'.get_category_link($item_2->cat_ID).'"><span>'.$item_2->cat_name.'</span></a></li>';
                                        }
                                        echo '</ul></div>';
                                    }
                                    echo '</li>';
                                }
                            } ?>
                        </ul>
                    </div>
                </aside>
                <article id="blog-list-product" >
                    <div class="categories_desc">
                        <div class="h1pt">
                            <div class="catalog_title"><h1><?php echo get_the_title() ?></h1></div>
                            <div class="share">
                                <div class="like-share">
                                    <a class="btn_facebook" rel="nofollow" href="javascript:;" title="Chia sẻ lên facebook"><i class="fa fa-facebook-square"></i></a>
                                    <a class="btn_twitter" rel="nofollow" href="javascript:;" id="twitter" title="Chia sẻ lên twitter"><i class="fa fa-twitter-square"></i></a>
                                    <a class="btn_pinterest" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên pinterest"><i class="fa fa-pinterest-square"></i></a>
                                    <a class="btn_google" rel="nofollow" href="javascript:;" title="Chia sẻ lên google+"><i class="fa fa-google-plus-square"></i></a>
                                    <fb:like style="top:-4px;" data-layout="button_count" href="http://xuatkhaulaodong99.com/gioi-thieu.html" send="false" width="138" height="24" data-show-faces="False" font=""></fb:like>
                                    <a class="g-plusone" data-size="medium"></a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="list-product-block">
                        <?php
                        $args = array(
                            'post_type'      => 'don_hang',
                            'posts_per_page' => 9,
                            'paged' => get_query_var('page')
                        );
                        $loop = new WP_Query( $args );
                        $total = $loop->found_posts;
                        $max_num_pages = $loop->max_num_pages;
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            echo ('
									<div class="product-blog">
										<div class="product_img">
											<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
												<img style="width: 100%" src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'" />
												<span class="hover-image"></span>
											</a>
										</div>
										<div class="price">
											<span class="price-new">'.get_post_meta($loop->post->ID, 'luong', true).'</span>
										</div>
										<div class="name">
											<h2><a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a></h2>
										</div>
									</div>
                                ');
                        endwhile;

                        wp_reset_query();
                        ?>
                    </div>
                    <div id="pagination" style="padding-top: 20px">
                        <?php
                        $page = !empty(get_query_var('page')) ? get_query_var('page') : 1;
                        if($total > 0) {
                            if ($page > 1) {
                                echo '<a href="/' . get_post()->post_name . '/">«</a>';
                                echo '<a href="/' . get_post()->post_name . '/' . ($page > 2 ? '?page=' . ($page - 1) : '') . '">' . ($page - 1) . '</a>';
                            }
                            ?>
                            <strong><?php echo $page ?></strong>
                            <?php
                            if ($page < $max_num_pages) {
                                echo '<a href="/' . get_post()->post_name . '/?page=' . ($page + 1) . '">' . ($page + 1) . '</a>';
                                echo '<a href="/' . get_post()->post_name . '/?page=' . ($max_num_pages) . '">»</a>';
                            }
                        } else {
                            echo 'Không tìm thấy kết quả nào.';
                        }
                        ?>
                    </div>
                </article>
            </div>
            <div class="clear"></div>
        </section>
    </section>

<?php get_footer()?>