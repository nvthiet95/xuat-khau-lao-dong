<?php

add_action('init', 'my_rem_editor_from_post_type');
function my_rem_editor_from_post_type() {
    remove_post_type_support( 'banner', 'editor' );
}

add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' ); function my_enqueue_assets() { wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' ); }

function insert_query_vars($vars) {
    array_push($vars, 'pro_id'); //lưu id sản phẩm
    array_push($vars, 'action'); //lưu thao tác (thêm, xóa)
    array_push($vars, 'quantity'); //số lượng sản phẩm
    return $vars;
}
add_filter('query_vars', 'insert_query_vars');

function rewrite_rules($rules) {
    $new_rules = array();
    $new_rules['(thong-tin-dich-vu)/(them|xoa)/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]';
    $new_rules['(gio-hang)/(them|xoa)/([0-9]+)/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]';
    $new_rules['(gio-hang)/(them|xoa)/([0-9]+)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]&pro_id=$matches[3]&quantity=$matches[4]';
    $new_rules['(gio-hang)/(xoahet)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]';
    $new_rules['(gio-hang)/(sua)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]';
    $new_rules['(thong-tin-dich-vu)/(sua)/?'] = 'index.php?pagename=$matches[1]&action=$matches[2]';
    return $new_rules + $rules;
    echo $matches[2];

}
add_action('rewrite_rules_array', 'rewrite_rules');
add_action('init', 'mySessionStart', 1);
add_action('wp_logout', 'mySessionEnd');
add_action('wp_login', 'mySessionEnd');

function mySessionStart() {
    ob_start();
    if(!session_id()) {
        session_start();
    }
}
function mySessionEnd() {
    session_destroy ();
}
function cptui_register_my_cpts_don_hang() {

    /**
     * Post Type: Đơn hàng.
     */

    $labels = array(
        "name" => __( "Đơn hàng", "custom-post-type-ui" ),
        "singular_name" => __( "Đơn hàng", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Đơn hàng", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "don_hang", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
        "menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1664 1344v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45zm0-512v128q0 26-19 45t-45 19h-1408q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h1408q26 0 45 19t19 45z" fill="#fff"/></svg>')
    );

    register_post_type( "don_hang", $args );
}

add_action( 'init', 'cptui_register_my_cpts_don_hang' );

function cptui_register_my_cpts_tin_tuc() {

    /**
     * Post Type: Tin tức.
     */

    $labels = array(
        "name" => __( "Tin tức", "custom-post-type-ui" ),
        "singular_name" => __( "Tin tức", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Tin tức", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "tin_tuc", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
        "taxonomies" => array( "category" ),
        "menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg width="2048" height="1792" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1024 512h-384v384h384v-384zm128 640v128h-640v-128h640zm0-768v640h-640v-640h640zm640 768v128h-512v-128h512zm0-256v128h-512v-128h512zm0-256v128h-512v-128h512zm0-256v128h-512v-128h512zm-1536 960v-960h-128v960q0 26 19 45t45 19 45-19 19-45zm1664 0v-1088h-1536v1088q0 33-11 64h1483q26 0 45-19t19-45zm128-1216v1216q0 80-56 136t-136 56h-1664q-80 0-136-56t-56-136v-1088h256v-128h1792z" fill="#fff"/></svg>')
    );

    register_post_type( "tin_tuc", $args );
}

add_action( 'init', 'cptui_register_my_cpts_tin_tuc' );

function cptui_register_my_cpts_banner() {

    /**
     * Post Type: Banner.
     */

    $labels = array(
        "name" => __( "Banner", "custom-post-type-ui" ),
        "singular_name" => __( "Banner", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Banner", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "banner", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "thumbnail" ),
        "menu_icon" => 'data:image/svg+xml;base64,' . base64_encode('<svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1596 380q28 28 48 76t20 88v1152q0 40-28 68t-68 28h-1344q-40 0-68-28t-28-68v-1600q0-40 28-68t68-28h896q40 0 88 20t76 48zm-444-244v376h376q-10-29-22-41l-313-313q-12-12-41-22zm384 1528v-1024h-416q-40 0-68-28t-28-68v-416h-768v1536h1280zm-128-448v320h-1024v-192l192-192 128 128 384-384zm-832-192q-80 0-136-56t-56-136 56-136 136-56 136 56 56 136-56 136-136 56z" fill="#fff"/></svg>')
    );

    register_post_type( "banner", $args );
}

add_action( 'init', 'cptui_register_my_cpts_banner' );