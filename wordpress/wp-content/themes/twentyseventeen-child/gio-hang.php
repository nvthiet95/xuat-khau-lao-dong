<?php
/*
Template Name: Trang giỏ hàng
*/
?>
<?php get_header(); ?>
    <section id="container">
        <div class="main">
            <nav id="breadcumds">
                <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                            <span itemprop="title">Trang chủ</span>
                        </a>
                    </li>
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Thông tin dịch vụ" href="http://xuatkhaulaodong99.com/gio-hang.html"
                           itemprop="url">
                            <span itemprop="title">Thông tin dịch vụ</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <article id="div-shopping"><section id="cart">
                    <div id="heading">
                        <h2 class="title-cart">GIỎ HÀNG CỦA BẠN </h2>
                    </div>
            <?php
            global $product_prefix;
            $action = get_query_var('action'); //thêm|xóa
            $pro_id = get_query_var('pro_id'); //id sản phẩm
            if ($action){ //nếu có thao tác (thêm hoặc xóa)
                switch ($action) {
                    case 'them':
                        if (isset($_SESSION['cart'][$pro_id])) //nếu đã có thì cập nhật số lượng thêm 1
                            $quantity = $_SESSION['cart'][$pro_id] + 1;
                        else
                            $quantity = 1; //ngược lại tạo 1 item mới với số lượng là 1
                        $_SESSION['cart'][$pro_id] = $quantity; //cập nhật lại
                        wp_redirect(get_bloginfo('url') . '/gio-hang');
                        exit(); //trả về trang hiển thị giỏ hàng
                        break;
                    case 'xoahet':
                        unset($_SESSION['cart']);
                        wp_redirect(get_bloginfo('url') . '/gio-hang');
                        exit(); //trả về trang hiển thị giỏ hàng
                        break;
                    case 'sua':
                        if(isset($_POST['cart'])) {
                            foreach ($_POST['cart'] as $pro_id => $quantity) {
                                if ($quantity == 0) unset($_SESSION['cart'][$pro_id]);
                                else $_SESSION['cart'][$pro_id] = $quantity;
                            }
                        }
                        wp_redirect(get_bloginfo('url') . '/gio-hang');
                        exit(); //trả về trang hiển thị giỏ hàng
                        break;
                    case 'xoa':
                        if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) { //kiểm tra và xóa sản phẩm dựa vào id
                            unset($_SESSION['cart'][$pro_id]);
                            wp_redirect(get_bloginfo('url') . '/gio-hang/');
                            exit();
                        } else {
                            unset($_SESSION['cart']);
                            echo "<h3>Hiện chưa có sản phẩm nào trong giỏ hàng! <a href='" . get_bloginfo('url') . "'>Bấm vào đây</a> để xem và mua hàng.</h3>";
                        }
                        break;
                }
            }else{ //không có thao tác thêm hoặc xóa thì sẽ hiển thị sản phẩm trong giỏ hàng
            ?>
            <?php
            if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) { //kiểm tra số lượng sản phẩm trước khi hiển thị

            ?>
                    <table id="table" border="0" cellpadding="10px" cellspacing="1px">
                        <tbody>
                        <tr id="main_heading">
                            <td>STT</td>
                            <td>Đơn hàng</td>
                            <td>Lương cơ bản</td>
                            <td>Số lượng</td>
                            <td>Thành tiền</td>
                            <td align="center">Xóa</td>
                        </tr>
                        <form action="/gio-hang/sua/" method="POST">
                            <?php
                            $total = 0;
                            foreach ( $_SESSION['cart'] as $pro_id => $quantity ){ //lặp qua mảng cart session lấy ra id và show thông tin sản phẩm theo id đó
                                $product = get_post((int)$pro_id );
                                if(get_field('price', $pro_id)!=0){
                                    $price = get_field('price', $pro_id);}else{
                                    $price = get_field('price_old', $pro_id);
                                }
                                ?>
                                <tr class="content-cart">
                                    <td style="width:50px;"><?php if ( has_post_thumbnail( $pro_id ) ) echo get_the_post_thumbnail( $pro_id, array( 50, 50 ) ); else echo "<img src='".get_bloginfo('template_url')."/images/no_img.png' style='width:50px;height:50px;' />"; ?></td>
                                    <td><a target="_blank" href="<?php echo get_permalink($pro_id);?>"><?php echo $product->post_title; ?></a></td>
                                    <td>
                                        <?php echo get_post_meta($pro_id, 'luong', true) ?>
                                    </td>
                                    <td align="center">
                                    <span class="add-to-cart">
                                        <span class="dec add">-</span>
                                        <input type="text" name="cart[<?php echo $pro_id; ?>]" value="<?php echo $quantity; ?>" maxlength="4" size="1" id="qty" style="text-align: right">
                                        <span class="add">+</span>
                                    </span>
                                    </td>
                                    <td></td>
                                    <?php  $total += $price * $quantity;   $_POST['tien'] = number_format($total).' đ'; ?>
                                    <td align="center">
                                        <a href="<?php echo get_bloginfo( 'url' ) . '/gio-hang/xoa/' . $pro_id; //link xóa sản phẩm trong giỏ ?>">
                                            <img src="http://xuatkhaulaodong99.com/public/upload/img/cart_cross.jpg"
                                                 width="25px" height="20px">
                                        </a>
                                    </td>
                                    </td>
                                </tr>
                                <?php
                                $check .= $product->post_title. " " ."(".$quantity.")". " ,";
                                $_POST['name'] = $check;
                            }
                            ?>
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="1" align="center"><input class="button teal" type="submit" value="Cập nhật"></td>
                                <td colspan="1" align="center">
                                    <a href="/thong-tin-dich-vu/" style="padding: 11px 20px; font-size: 11px;" class="button teal" type="submit">Thanh toán</a>
                                </td>
                                <td><input class="button teal" type="button" value="Xóa" onclick="clear_cart()"></td>
                            </tr>
                        </form>
                        </tbody>
                    </table>
                <?php
                } else {
                    echo '<div id="text">Giỏ hàng của bạn hiện đang trống !  </div>';
                }
                ?>
                <?php
                if (isset($_POST['sua'])) { //xử lý cập nhật giỏ hàng
                    if (isset($_POST['quantity'])) {
                        if (count($_SESSION['cart']) == 0) unset($_SESSION['cart']); //nếu không còn sản phẩm trong giỏ thì xóa giỏ hàng
                        foreach ($_POST['quantity'] as $pro_id => $quantity) { //lặp mảng số lượng mới và cập nhật mới cho giỏ hàng
                            if ($quantity == 0) unset($_SESSION['cart'][$pro_id]);
                            else $_SESSION['cart'][$pro_id] = $quantity;
                        }
                        wp_redirect(get_bloginfo('url') . '/gio-hang/'); //cập nhật xong trả về trang hiển thị sản phẩm trong giỏ
                    }
                }
                }
                ?>
    </section><section class="allproduct">
                    <h2>Chọn đơn hàng</h2>
                    <?php
                    $args = array(
                        'post_type'      => 'don_hang',
                        'posts_per_page' => 12
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                        echo ('
                            <div class="product-block-inner">
        
                                <div class="image">
                                    <a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
                                        <img src="'.get_the_post_thumbnail_url($loop->post->ID).'" style="width: 100%;" alt="'.get_the_title().'">
                                        <span class="hover-image"></span>
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
                                </div>
                                <div class="price">
                                    <span class="price-new">'.get_post_meta($loop->post->ID, 'luong', true).'</span>
                                </div>
                                <div class="contact">
                                    <form action="/thong-tin-dich-vu/them/'.$loop->post->ID.'">
                                        <p class="add_button">
                                            <input type="submit" value="Đăng ký ngay" class="button">
                                        </p>
                                    </form>
                                </div>
                            </div>
                                ');
                    endwhile;

                    wp_reset_query();
                    ?>
                    <div class="clear"></div>
                    <div id="pagination-shopping">  </div>
                </section>
            </article>
        </div>
        <script type="text/javascript">
            function clear_cart() {
                var result = confirm('Bạn muốn xóa giỏ hàng ?');
                if (result) {
                    window.location = "/gio-hang/xoahet/";
                }
                else {
                    return false;
                }
            }
        </script>
    </section>
<?php get_footer() ?>