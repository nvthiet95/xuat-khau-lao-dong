<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$menu_name = 'top';
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
?>

<div id="menu-top">
    <div class="main">
        <ul>
            <li class="home active"><a class="home active" title="Trang chủ" href="/">Trang chủ</a></li>
            <?php
            $count = 0;
            $submenu = false;

            foreach( $menuitems as $item ):
            $title = $item->title;
            $link = $item->url;
            if ( !$item->menu_item_parent ):
            $parent_id = $item->ID;
            ?>
            <li class="item">
                <a href="<?php echo $link; ?>" class="title">
                    <?php echo $title; ?>
                </a>
                <?php endif; ?>
                <?php if ( $parent_id == $item->menu_item_parent ): ?>
                <?php if ( !$submenu ): $submenu = true; ?>
                <ul>
                    <?php endif; ?>
                    <li>
                        <a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a>
                    </li>
                    <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                </ul>
                <?php $submenu = false; endif; ?>
                <?php endif; ?>
                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
            </li>
            <?php $submenu = false; endif; ?>
            <?php $count++; endforeach; ?>
        </ul>
    </div>
</div>