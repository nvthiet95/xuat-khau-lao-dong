<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<section id="container">
    <section id="container">
        <div class="main">
            <nav id="breadcumds">
                <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                            <span itemprop="title">Trang chủ</span>
                        </a>
                    </li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/tin-tuc/" itemprop="url">
				<span itemprop="title">Tin tức</span>
                        </a>
                    </li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="<?php echo get_permalink() ?>" itemprop="url">
                            <span itemprop="title"><?php echo get_the_title() ?></span>
                        </a>
                    </li>
                </ul>
            </nav>
            <aside id="side_bar">	<div class="category-list">
                    <ul>
                        <?php
                        $menu_name = 'top';
                        $locations = get_nav_menu_locations();
                        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                        $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
                        $count = 0;
                        $submenu = false;

                        foreach( $menuitems as $item ):
                            $title = $item->title;
                            $link = $item->url;
                            if ( !$item->menu_item_parent ):
                                $parent_id = $item->ID;
                                ?>
                                <li>
                                <div class="category-title">
                                    <a href="<?php echo $link; ?>">
                                        <span><?php echo $title; ?></span>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if ( $parent_id == $item->menu_item_parent ): ?>
                            <?php if ( !$submenu ): $submenu = true; ?>
                                <div class="list"><ul>
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
                            </li>
                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                </ul></div>
                                <?php $submenu = false; endif; ?>
                        <?php endif; ?>
                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                            </li>
                            <?php $submenu = false; endif; ?>
                            <?php $count++; endforeach; ?>


                        <?php $product_categories_1 = get_categories( ['taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] ); ?>
                        <?php foreach ($product_categories_1 as $item_1) {
                            if($item_1->cat_ID !== 15) {
                                echo '<li><div class="category-title"><a href="'.get_category_link($item_1->cat_ID).'"  title="'.$item_1->cat_name.'"><span>'.$item_1->cat_name.'</span></a></div>';
                                $product_categories_2 = get_categories( ['taxonomy' => 'product_cat', 'parent' => $item_1->cat_ID, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] );
                                if(count($product_categories_2) > 0) {
                                    echo '<div class="list"><ul>';
                                    foreach ($product_categories_2 as $item_2) {
                                        echo '<li><a title="'.$item_2->cat_name.'" href="'.get_category_link($item_2->cat_ID).'"><span>'.$item_2->cat_name.'</span></a></li>';
                                    }
                                    echo '</ul></div>';
                                }
                                echo '</li>';
                            }
                        } ?>
                    </ul>
                </div>
                <div class="box-news">
                    <h3>Tin mới cập nhật</h3>
                    <ul class="news-item">
                        <?php
                        $args = array(
                            'post_type'      => 'tin_tuc',
                            'posts_per_page' => 6
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            echo ('
										<li>
											<div class="news-img" style="border: 1px #e9e9e9 solid;float:left;">
												<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
													<img style="width: 90px" src="'.get_the_post_thumbnail_url($loop->post->ID, '90x90').'" alt="'.get_the_title().'" />
													<div class="hover"></div>
												</a>
											</div>
											<div class="news-title">
												<h4>
													<a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
												</h4>
											</div>
											<div class="clear"></div>
										</li>
                                        ');
                        endwhile;

                        wp_reset_query();
                        ?>
                    </ul>
                </div>
                <div class="box-news">
                    <h3>Tin được quan tâm</h3>
                    <ul class="news-item">
                        <?php
                        $args = array(
                            'post_type'      => 'tin_tuc',
                            'posts_per_page' => 6
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            echo ('
										<li>
											<div class="news-img" style="border: 1px #e9e9e9 solid;float:left;">
												<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
													<img style="width: 90px" src="'.get_the_post_thumbnail_url($loop->post->ID, '90x90').'" alt="'.get_the_title().'" />
													<div class="hover"></div>
												</a>
											</div>
											<div class="news-title">
												<h4>
													<a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
												</h4>
											</div>
											<div class="clear"></div>
										</li>
                                        ');
                        endwhile;
                        wp_reset_query();
                        ?>
                    </ul>
                </div></aside>
            <article id="div-article" >
                <div class="blog-share-title">
                    <div class="article_title">
                        <h1><?php echo get_the_title() ?></h1>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="like-share">
                    <a class="btn_facebook" rel="nofollow" href="javascript:;" title="Chia sẻ lên facebook"><i class="fa fa-facebook-square"></i></a>
                    <a class="btn_twitter" rel="nofollow" href="javascript:;" id="twitter" title="Chia sẻ lên twitter"><i class="fa fa-twitter-square"></i></a>
                    <a class="btn_pinterest" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên pinterest"><i class="fa fa-pinterest-square"></i></a>
                    <a class="btn_google" rel="nofollow" href="javascript:;" title="Chia sẻ lên google+"><i class="fa fa-google-plus-square"></i></a>
                </div>
                <p class="article-sapo" style="padding:1% 0;"><b><?php echo get_the_excerpt() ?></b></p>


                <div class="content_article">
                    <?php echo the_content() ?>
                </div>
            </article>
            <article id="related-news" >
                <p class="title-lienquan">Bài viết liên quan</p>
                <ul class="blog-related">
                    <?php
                    $args = array(
                        'post_type'      => 'tin_tuc',
                        'posts_per_page' => 4
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        echo ('
                            <li>
                                <a class="pic" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
                                    <img src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'" />
                                </a>
                                <h4>
                                    <a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
                                </h4>
                                <p class="desc">'.get_the_excerpt().'</p>
                            </li>
                        ');
                    endwhile;

                    wp_reset_query();
                    ?>
                </ul>
            </article>
        </div>
        <div class="clear"></div>
    </section>
</section>
