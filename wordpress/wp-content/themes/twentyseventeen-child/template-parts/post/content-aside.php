<?php
/**
 * Template part for displaying audio posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<section id="container">
    <section id="container">
        <div class="main">
            <nav id="breadcumds">
                <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                            <span itemprop="title">Trang chủ</span>
                        </a>
                    </li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="/don-hang-xuat-khau-lao-dong/" itemprop="url">
				<span itemprop="title">
                Đơn hàng xuất khẩu lao động                </span>
                        </a>
                    </li>
                    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="<?php echo get_permalink() ?>" itemprop="url">
                            <span itemprop="title"><?php echo get_the_title() ?></span>
                        </a>
                    </li>
                </ul>
            </nav>
            <aside id="side_bar">	<div class="category-list">
                    <ul>
                        <?php
                        $menu_name = 'top';
                        $locations = get_nav_menu_locations();
                        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                        $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
                        $count = 0;
                        $submenu = false;

                        foreach( $menuitems as $item ):
                            $title = $item->title;
                            $link = $item->url;
                            if ( !$item->menu_item_parent ):
                                $parent_id = $item->ID;
                                ?>
                                <li>
                                <div class="category-title">
                                    <a href="<?php echo $link; ?>">
                                        <span><?php echo $title; ?></span>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if ( $parent_id == $item->menu_item_parent ): ?>
                            <?php if ( !$submenu ): $submenu = true; ?>
                                <div class="list"><ul>
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
                            </li>
                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                </ul></div>
                                <?php $submenu = false; endif; ?>
                        <?php endif; ?>
                            <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                            </li>
                            <?php $submenu = false; endif; ?>
                            <?php $count++; endforeach; ?>


                        <?php $product_categories_1 = get_categories( ['taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] ); ?>
                        <?php foreach ($product_categories_1 as $item_1) {
                            if($item_1->cat_ID !== 15) {
                                echo '<li><div class="category-title"><a href="'.get_category_link($item_1->cat_ID).'"  title="'.$item_1->cat_name.'"><span>'.$item_1->cat_name.'</span></a></div>';
                                $product_categories_2 = get_categories( ['taxonomy' => 'product_cat', 'parent' => $item_1->cat_ID, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] );
                                if(count($product_categories_2) > 0) {
                                    echo '<div class="list"><ul>';
                                    foreach ($product_categories_2 as $item_2) {
                                        echo '<li><a title="'.$item_2->cat_name.'" href="'.get_category_link($item_2->cat_ID).'"><span>'.$item_2->cat_name.'</span></a></li>';
                                    }
                                    echo '</ul></div>';
                                }
                                echo '</li>';
                            }
                        } ?>
                    </ul>
                </div>
                <div class="box-news">
                    <h3>Tin mới cập nhật</h3>
                    <ul class="news-item">
                        <?php
                        $args = array(
                            'post_type'      => 'tin_tuc',
                            'posts_per_page' => 6
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            echo ('
										<li>
											<div class="news-img" style="border: 1px #e9e9e9 solid;float:left;">
												<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
													<img style="width: 90px" src="'.get_the_post_thumbnail_url($loop->post->ID, '90x90').'" alt="'.get_the_title().'" />
													<div class="hover"></div>
												</a>
											</div>
											<div class="news-title">
												<h4>
													<a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
												</h4>
											</div>
											<div class="clear"></div>
										</li>
                                        ');
                        endwhile;

                        wp_reset_query();
                        ?>
                    </ul>
                </div>
                <div class="box-news">
                    <h3>Tin được quan tâm</h3>
                    <ul class="news-item">
                        <?php
                        $args = array(
                            'post_type'      => 'tin_tuc',
                            'posts_per_page' => 6
                        );
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            echo ('
										<li>
											<div class="news-img" style="border: 1px #e9e9e9 solid;float:left;">
												<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
													<img style="width: 90px" src="'.get_the_post_thumbnail_url($loop->post->ID, '90x90').'" alt="'.get_the_title().'" />
													<div class="hover"></div>
												</a>
											</div>
											<div class="news-title">
												<h4>
													<a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
												</h4>
											</div>
											<div class="clear"></div>
										</li>
                                        ');
                        endwhile;
                        wp_reset_query();
                        ?>
                    </ul>
                </div></aside>
            <article id="div-article" >
                <div class="product-info">
                    <div class="left">
                        <div class="image">
                            <a href="<?php echo the_post_thumbnail_url() ?>" title="<?php echo get_the_title() ?>" rel="galy">
                                <img class="small" src="<?php echo the_post_thumbnail_url() ?>" alt="<?php echo get_the_title() ?>" />
                            </a>
                        </div>
                        <?php
                            $postGalary = call_user_func_array(
                                [apply_filters( 'post_gallery_model_class', '\PostGallery\Models\Post' ), 'find'],
                                [get_the_ID()]
                            );
                            if(count($postGalary->gallery) > 0) {
                                echo '<div class="list-icon-image">';
                                foreach ($postGalary->gallery as $image) {
                                    $image_url = $image->guid;
                                    echo ('
                                                <a href="'.$image_url.'" title="" rel="galy">
                                                    <img src="'.$image_url.'" alt="" width="76px" style="object-fit: cover;" />
                                                </a>
                                            ');
                                }
                                echo '</div>';
                            }
                        ?>
                        <div class="like-share">
                            <a class="btn_facebook" rel="nofollow" href="javascript:;" title="Chia sẻ lên facebook"><i class="fa fa-facebook-square"></i></a>
                            <a class="btn_twitter" rel="nofollow" href="javascript:;" id="twitter" title="Chia sẻ lên twitter"><i class="fa fa-twitter-square"></i></a>
                            <a class="btn_pinterest" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên pinterest"><i class="fa fa-pinterest-square"></i></a>
                            <a class="btn_google" rel="nofollow" href="javascript:;" title="Chia sẻ lên google+"><i class="fa fa-google-plus-square"></i></a>
                            <a class="g-plusone" data-size="medium"></a>
                        </div>
                    </div>
                    <div class="right">
                        <div class="description" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <table class="product-description">

                                <tr>
                                    <td colspan="2">
                                        <h1 class="item name fn" itemprop="name"><?php echo get_the_title() ?></h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td-first"><b>Lương: </b></td>
                                    <td>
                                        <span class="donvi" itemprop="priceCurrency" content="Triệu"><?php echo get_post_meta(get_the_ID(), 'luong', true) ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Mã đơn hàng:</b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'ma_don_hang', true) ?></td>
                                </tr>
                                <tr>
                                    <td><b>Nơi làm việc:</b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'noi_lam_viec', true) ?></td>
                                </tr>

                                <tr>
                                    <td><b><span>Tình trạng:</span></b></td>
                                    <td class="description-right"><span>
                                            <?php echo get_post_meta(get_the_ID(), 'tinh_trang', true) ?>
                                    </span></td>
                                </tr>
                                <tr>
                                    <td><b><span>Tư vấn 24/7:</span></b></td>
                                    <td class="description-right">
                                        <a href="tel:<?php echo get_post_meta(get_the_ID(), 'so_dien_thoai', true) ?>" style="font-size:18px" class="color2"><span class="number"><?php echo get_post_meta(get_the_ID(), 'so_dien_thoai', true) ?> </span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <form action="/index.php">
                                        <td colspan="2">
                                            <b style="padding-right:12px;">Số lượng:</b>
                                            <span class="add-to-cart">
                                            <input type="hidden" name="pagename" value="thong-tin-dich-vu" />
                                            <input type="hidden" name="action" value="them" />
                                            <input type="hidden" name="pro_id" value="<?php echo get_the_ID() ?>" />
                                            <span class="dec add">-</span>
                                            <input type="text" name="quantity" id="qty" size="4" value="1" />
                                            <span class="add">+</span>
                                            </span>
                                            <input type="submit" value="Đăng ký" class="button" id="button-cart" />
                                        </td>
                                    </form>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="content_article">
                    <?php the_content(); ?>
                </div>
            </article>
            <article id="related-news" >
                <p class="title-lienquan">đơn hàng bạn quan tâm</p>
                <div class="list-product-block">
                    <?php
                    $args = array(
                        'post_type'      => 'don_hang',
                        'posts_per_page' => 3
                    );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        echo ('
									<div class="product-blog">
										<div class="product_img">
											<a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
												<img style="width: 100%" src="'.get_the_post_thumbnail_url($loop->post->ID).'" alt="'.get_the_title().'" />
												<span class="hover-image"></span>
											</a>
										</div>
										<div class="price">
											<span class="price-new">'.get_post_meta($loop->post->ID, 'luong', true).'</span>
										</div>
										<div class="name">
											<h4><a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a></h4>
										</div>
									</div>
                                ');
                    endwhile;

                    wp_reset_query();
                    ?>
                </div>
            </article>
        </div>
        <div class="clear"></div>
    </section>
</section>

