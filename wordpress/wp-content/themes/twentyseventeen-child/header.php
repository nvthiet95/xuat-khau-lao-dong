<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<!DOCTYPE html>
<html xmlns:og="http://opengraphprotocol.org/schema/" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head prefix="og: http://ogp.me/ns# fb:http://ogp.me/ns/fb# article:http://ogp.me/ns/article#">
    <title itemprop="headline"><?php echo get_the_title() ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" itemprop="inLanguage" content="vi"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
    <link  rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/style.css" media="screen" />
    <link  rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/jcs4.css" />
    <link  rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/page-style.css" media="screen" />
    <link href="/wp-content/uploads/2019/01/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>

<body >
<div id="wrapper">
    <div id="wrapLoader">
        <span class="fixed-mb"></span>
        <header id="header">
            <div class="alert-notifi">
                <div id="notification">
                </div>
            </div>
            <div id="welcome-top">
                <div class="share-top">
                    <a class="btn_facebook" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn_twitter" rel="nofollow" href="javascript:;" id="twitter" title="Chia sẻ bài viết lên twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn_pinterest" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên pinterest">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a class="btn_google" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên google+">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </div>
                <h4>Chào mừng bạn đã đến với xuất khẩu lao động quốc tế BATIMEX</h4>
                <div class="top-cart">
                    <a title="Đơn hàng" href="/gio-hang/"><img atl="đơn hàng" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon_cart.png" alt="đơn hàng" />
                        <?php
                        $total = 0;
                        if ( isset( $_SESSION['cart'] ) && count( $_SESSION['cart'] ) > 0 ) {
                            foreach ( $_SESSION['cart'] as $pro_id => $quantity ) {
                                $total += $quantity;
                            }
                        }
                        ?>
                        <b class="number-product">Đơn hàng(<?php echo $total ?>)</b>
                    </a>
                </div>
                <div class="top-search">
                    <form action="/index.php" method="GET">
                        <input class="input" name="s" type="text" required placeholder="Nhập nội dung cần tìm kiếm">
                        <input class="button" type="submit" name="" value="Tìm kiếm">
                    </form>
                </div>
                <div class="clear"></div>
            </div>
            <div id="header-info">
                <div class="in">
                    <div class="logo"> <a href="/" title="Xuất khẩu lao động Nhật Bản"><img  src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo.png" alt="Xuất khẩu lao động Nhật Bản" /></a> </div>
                    <div class="info-right">
                        <div class="top-contact">
                            <ul>
                                <li>
                                    <h4>CÔNG TY CỔ PHẦN XUẤT NHẬP KHẨU BATIMEX</h4>
                                </li>

                                <li class="item-location">
                                    <img alt="Địa chỉ" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png" />
                                    <span><strong>Địa chỉ VP:</strong>  Số 68 Nguyễn Cơ Thạch, Mỹ Đình, TP.Hà Nội</span>
                                </li>
                            </ul>
                        </div>
                        <div class="hotline"> <a rel="nofollow" href="tel:0982265235" title="Gọi để được tư vấn"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/top_hotline.png" alt="Hotline tư vấn"></a> </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <?php if ( has_nav_menu( 'top' ) ) : ?>
                <div class="navigation-top">
                    <div class="wrap">
                        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                    </div><!-- .wrap -->
                </div><!-- .navigation-top -->
            <?php endif; ?>
        </header>