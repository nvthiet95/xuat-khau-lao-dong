<?php
/*
Template Name: Trang danh mục
*/
?>
<?php get_header();?>
    <section id="container">
        <section id="container">
            <div class="main">
                <nav id="breadcumds">
                    <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                                <span itemprop="title">Trang chủ</span>
                            </a>
                        </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Giới thiệu" href="<?php get_permalink() ?>"  itemprop="url">
                                <span itemprop="title"><?php echo get_queried_object()->name ?></span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <aside id="side_bar">
                    <div class="category-list">
                        <ul>
                            <?php
                            $menu_name = 'top';
                            $locations = get_nav_menu_locations();
                            $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                            $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
                            $count = 0;
                            $submenu = false;

                            foreach( $menuitems as $item ):
                                $title = $item->title;
                                $link = $item->url;
                                if ( !$item->menu_item_parent ):
                                    $parent_id = $item->ID;
                                    ?>
                                    <li>
                                    <div class="category-title">
                                        <a href="<?php echo $link; ?>">
                                            <span><?php echo $title; ?></span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $parent_id == $item->menu_item_parent ): ?>
                                <?php if ( !$submenu ): $submenu = true; ?>
                                    <div class="list"><ul>
                                <?php endif; ?>
                                <li>
                                    <a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
                                </li>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                    </ul></div>
                                    <?php $submenu = false; endif; ?>
                            <?php endif; ?>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                                </li>
                                <?php $submenu = false; endif; ?>
                                <?php $count++; endforeach; ?>


                            <?php $product_categories_1 = get_categories( ['taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] ); ?>
                            <?php foreach ($product_categories_1 as $item_1) {
                                if($item_1->cat_ID !== 15) {
                                    echo '<li><div class="category-title"><a href="'.get_category_link($item_1->cat_ID).'"  title="'.$item_1->cat_name.'"><span>'.$item_1->cat_name.'</span></a></div>';
                                    $product_categories_2 = get_categories( ['taxonomy' => 'product_cat', 'parent' => $item_1->cat_ID, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] );
                                    if(count($product_categories_2) > 0) {
                                        echo '<div class="list"><ul>';
                                        foreach ($product_categories_2 as $item_2) {
                                            echo '<li><a title="'.$item_2->cat_name.'" href="'.get_category_link($item_2->cat_ID).'"><span>'.$item_2->cat_name.'</span></a></li>';
                                        }
                                        echo '</ul></div>';
                                    }
                                    echo '</li>';
                                }
                            } ?>
                        </ul>
                    </div>
                </aside>
                <article id="blog-list-article">
                    <?php
                    $args = array(
                        'post_type'      => 'tin_tuc',
                        'posts_per_page' => 10,
                        'paged' => get_query_var('page'),
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field' => 'id',
                                'terms' => get_queried_object()->term_id
                            )
                        )
                    );
                    $loop = new WP_Query( $args );
                    $total = $loop->found_posts;
                    $max_num_pages = $loop->max_num_pages;
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        echo ('
                                <div class="article-blog">
                                    <a class="link-image" href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">
                                        <img style="width: 150px" itemprop="image" src="'.get_the_post_thumbnail_url($loop->post->ID).'">
                                        <span class="hover-image"></span>
                                    </a>
                                    <h2 class="title">
                                        <a href="'.get_permalink($loop->post->ID).'" title="'.get_the_title().'">'.get_the_title().'</a>
                                    </h2>
                                    <p class="details">'.get_the_excerpt().'</p>
                                    <p class="read-more"> <i class="link-item" data-link="'.get_permalink($loop->post->ID).'">Xem thêm...</i> </p>
                                </div>
                        ');
                    endwhile;

                    wp_reset_query();
                    ?>
                    <div id="pagination" style="padding-top: 20px">
                        <?php
                        $page = !empty(get_query_var('page')) ? get_query_var('page') : 1;
                        if($total > 0) {
                            if ($page > 1) {
                                echo '<a href="/' . get_queried_object()->slug . '/">«</a>';
                                echo '<a href="/' . get_queried_object()->slug . '/' . ($page > 2 ? '?page=' . ($page - 1) : '') . '">' . ($page - 1) . '</a>';
                            }
                            ?>
                            <strong><?php echo $page ?></strong>
                            <?php
                            if ($page < $max_num_pages) {
                                echo '<a href="/' . get_queried_object()->slug . '/?page=' . ($page + 1) . '">' . ($page + 1) . '</a>';
                                echo '<a href="/' . get_queried_object()->slug . '/?page=' . ($max_num_pages) . '">»</a>';
                            }
                        }
                        ?>
                    </div>
                </article>
            </div>
            <div class="clear"></div>
        </section>
    </section>
<?php get_footer()?>