<?php
/*
Template Name: Trang danh sách đơn hàng
*/
?>
<?php
    get_header();
    unset($_SESSION['cart']);
?>
    <section id="container">
        <section id="container">
            <div class="main">
                <nav id="breadcumds">
                    <ul itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Trang chủ" href="http://xuatkhaulaodong99.com/" itemprop="url">
                                <span itemprop="title">Trang chủ</span>
                            </a>
                        </li>
                        <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a title="Giới thiệu" href="<?php get_permalink() ?>"  itemprop="url">
                                <span itemprop="title"><?php echo get_the_title() ?></span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <aside id="side_bar">
                    <div class="category-list">
                        <ul>
                            <?php
                            $menu_name = 'top';
                            $locations = get_nav_menu_locations();
                            $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                            $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC', 'menu_item_parent' => 0 ) );
                            $count = 0;
                            $submenu = false;

                            foreach( $menuitems as $item ):
                                $title = $item->title;
                                $link = $item->url;
                                if ( !$item->menu_item_parent ):
                                    $parent_id = $item->ID;
                                    ?>
                                    <li>
                                    <div class="category-title">
                                        <a href="<?php echo $link; ?>">
                                            <span><?php echo $title; ?></span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $parent_id == $item->menu_item_parent ): ?>
                                <?php if ( !$submenu ): $submenu = true; ?>
                                    <div class="list"><ul>
                                <?php endif; ?>
                                <li>
                                    <a href="<?php echo $link; ?>"><span><?php echo $title; ?></span></a>
                                </li>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                    </ul></div>
                                    <?php $submenu = false; endif; ?>
                            <?php endif; ?>
                                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                                </li>
                                <?php $submenu = false; endif; ?>
                                <?php $count++; endforeach; ?>


                            <?php $product_categories_1 = get_categories( ['taxonomy' => 'product_cat', 'parent' => 0, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] ); ?>
                            <?php foreach ($product_categories_1 as $item_1) {
                                if($item_1->cat_ID !== 15) {
                                    echo '<li><div class="category-title"><a href="'.get_category_link($item_1->cat_ID).'"  title="'.$item_1->cat_name.'"><span>'.$item_1->cat_name.'</span></a></div>';
                                    $product_categories_2 = get_categories( ['taxonomy' => 'product_cat', 'parent' => $item_1->cat_ID, 'hide_empty' => false, 'orderby' => 'cat_ID', 'order' => 'DESC'] );
                                    if(count($product_categories_2) > 0) {
                                        echo '<div class="list"><ul>';
                                        foreach ($product_categories_2 as $item_2) {
                                            echo '<li><a title="'.$item_2->cat_name.'" href="'.get_category_link($item_2->cat_ID).'"><span>'.$item_2->cat_name.'</span></a></li>';
                                        }
                                        echo '</ul></div>';
                                    }
                                    echo '</li>';
                                }
                            } ?>
                        </ul>
                    </div>
                </aside>
                <article id="div-product">
                    <style type="text/css">
                        #ss{
                            margin: 0 auto;
                            background-color: white;
                            text-align: center;
                        }
                        #ss h1{
                            padding: 10px 10px;
                            font-size: 18px;
                        }
                        #ss h2{font-style:italic}
                        #ss a{
                            color: blue;
                            text-decoration: none;
                        }
                        #ss .continue{text-align:center;margin-top:40px;}
                    </style>
                    <section id="ss">
                        <h1>Cảm ơn bạn đã gửi thông tin đăng ký Online</h1>
                        <h2>(Chúng tôi sẽ kiểm tra, liên hệ và xử lý thông tin trong thời gian sớm nhất !)</h2>
                        <div class="continue">
                            <p><input class="button teal" type="button" value="Tiếp tục" onclick="continues()"></p>
                        </div>
                    </section>
                </article>
            </div>
            <div class="clear"></div>
        </section>
    </section>

<?php get_footer()?>