<footer id="footer">
    <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
            <meta itemprop="url" content="http://xuatkhaulaodong99.com/public/frontend/images/logo.png">
            <meta itemprop="width" content="208">
            <meta itemprop="height" content="176">
        </div>
        <meta itemprop="name" content="Chevrolet GM">
    </div>
    <div class="footerbg2">
        <div class="main">
            <div class="left-footer">
                <h4>CÔNG TY CỔ PHẦN XUẤT XUẤT NHẬP KHẨU BATIMEX</h4>
                <div class="social-links">
                    <ul class="list">
                        <li><a class="email" href="mailto:#" target="_blank"><i class="fa fa-envelope"></i></a></li>
                        <li><a class="btn_twitter" rel="nofollow" href="javascript:;" id="twitter" title="Chia sẻ bài viết lên twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="btn_facebook" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="btn_google" rel="nofollow" href="javascript:;" title="Chia sẻ bài viết lên google+"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="youtube" href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
                <h4 class="bestBookingSystemSpan">© Copyright 2014 -
                    2019          / <span>Xuất Khẩu Lao Động BATIMEX</span> </h4>
                <div class="vspacingtop15">
                    <p> <i class="fa fa-map-marker"></i> <b>Địa chỉ:</b>  Số 68 Nguyễn Cơ Thạch, Mỹ Đình, TP.Hà Nội</p>
                    <p> <i class="fa fa-phone"></i> <b>Điện thoại:</b> 0982 265 235 - (024)63 283 518</p>
                    <p> <i class="fa fa-envelope"></i> <b>Email:</b> xuatkhaulaodongnb.info@gmail.com</p>
                    <p> <i class="fa fa-globe"></i> <b>Website:</b> http://xuatkhaulaodong99.com</p>
                </div>
            </div>
            <div class="right-footer">
                <div class="row">
                    <div class="footerPanel">
                        <ul>
                            <li>
                                <h4>Về Chúng tôi</h4>
                            </li>
                            <li> <a href="/gioi-thieu/" target="_blank">Giới thiệu</a> </li>
                            <li><a href="/tin-tuc/" target="_blank">Tin tức</a></li>
                            <li><a href="/lien-he/" target="_blank">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="footerPanel">
                        <ul>
                            <li>
                                <h4>Xuất khẩu lao động</h4>
                            </li>
                            <li> <a href="/don-hang-xuat-khau-lao-dong/" target="_blank">Đơn hàng xuất khẩu</a> </li>
                            <li> <a href="/tu-nghiep-sinh/" target="_blank">Tu nghiệp sinh</a> </li>
                            <li><a href="/chuong-trinh-ky-su/" target="_blank">Chương trình kỹ sư</a> </li>
                        </ul>
                    </div>
                    <div class="footerPanel">
                        <ul>
                            <li>
                                <h4>Thông Tin Cần Biết</h4>
                            </li>
                            <li> <a href="#" target="_blank">Điều kiện &amp; Điều khoản</a> </li>
                            <li><a href="/van-ban/" target="_blank">Văn bản</a> </li>
                            <li><a href="#" target="_blank">Câu hỏi thường gặp</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="footer-info-last">
                    <h4 style="text-transform:uppercase">Chuyên mục</h4>
                    <p>Chúng tôi, với đội ngũ giáo viên Người Nhật Bản dày dặn kinh nghiệm, đam mê với nghề. Mặt khác họ đã sinh ra và lớn lên tại Nhật nên đây là một thế mạnh lớn giành cho người lao động được trực tiếp học hỏi kinh nghiệm sống cũng như học hỏi về văn hóa và con người Nhật. Đặc biệt, để giúp cho lao động có cơ hội giao lưu với người bản ngữ, mạnh dạn trong các hoạt động tập thể, hoạt động sinh hoạt giao lưu văn hóa</p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="copyright">© Copyright 2010 - 2019 /
        <span>Bản quyền thuộc về xuatkhaulaodong88.com</span>
        <p>Nghiêm cấm sao lưu hình ảnh, nội dung trên website. Mọi hành vi sao lưu sẽ chịu trách nhiệm trước pháp luật</p>
    </div>
</footer>
<?php wp_footer(); ?>
<link rel='stylesheet' href='<?php echo get_stylesheet_directory_uri() ?>/assets/css/fontawesome-webfont.css' type='text/css' media='all' />
<link  rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/animate.css" type="text/css" />
<link  rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/animations.min.css" />
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery-1.11.1.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery-ui.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/scrollMonitor.js"></script>
<link href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/jquery.fancybox.css" rel="stylesheet">
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.fancybox.pack.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/myscript.js"></script>
<script type="text/javascript">
    $("#menu-top .home").addClass('active');
</script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/animate/myanimate.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.jcs4.min.js"></script>
<script type="text/javascript">
    $('.jcs4').jCS4({
        slideDuration: 4000,
        slideOutWait: 500,
        slideWidth: 1366,
        slideHeight: 380,
    });
</script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.validate.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/validate.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.easing.1.3.js"></script>
</body>
</html>
