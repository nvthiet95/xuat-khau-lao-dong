<?php
/**
 * Plugin Name: Danh sách khách hàng
 * Plugin URI: http://baovn24h.com
 * Description: Không có mô tả.
 * Version: 1.0
 * Author: Thiết Nguyễn
 * Author URI: http://baovn24h.com
 * License: GPLv2
 */

function register_customer_list()
{
    register_setting('mfpd-settings-group', 'mfpd_option_name');
}
function customer_list_create_menu()
{
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
    add_menu_page('Danh sách khách hàng', 'Khách hàng', 'publish_posts', 'customer-list', 'customer_list_page', 'data:image/svg+xml;base64,' . base64_encode('<svg width="2048" height="1792" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg"><path d="M657 896q-162 5-265 128h-134q-82 0-138-40.5t-56-118.5q0-353 124-353 6 0 43.5 21t97.5 42.5 119 21.5q67 0 133-23-5 37-5 66 0 139 81 256zm1071 637q0 120-73 189.5t-194 69.5h-874q-121 0-194-69.5t-73-189.5q0-53 3.5-103.5t14-109 26.5-108.5 43-97.5 62-81 85.5-53.5 111.5-20q10 0 43 21.5t73 48 107 48 135 21.5 135-21.5 107-48 73-48 43-21.5q61 0 111.5 20t85.5 53.5 62 81 43 97.5 26.5 108.5 14 109 3.5 103.5zm-1024-1277q0 106-75 181t-181 75-181-75-75-181 75-181 181-75 181 75 75 181zm704 384q0 159-112.5 271.5t-271.5 112.5-271.5-112.5-112.5-271.5 112.5-271.5 271.5-112.5 271.5 112.5 112.5 271.5zm576 225q0 78-56 118.5t-138 40.5h-134q-103-123-265-128 81-117 81-256 0-29-5-66 66 23 133 23 59 0 119-21.5t97.5-42.5 43.5-21q124 0 124 353zm-128-609q0 106-75 181t-181 75-181-75-75-181 75-181 181-75 181 75 75 181z" fill="#fff"/></svg>'), 30);
    add_action('admin_init', 'register_customer_list');
}
add_action('admin_menu', 'customer_list_create_menu');
function customer_list_page()
{
    $actual_link = $_SERVER['REQUEST_URI'];
    global $wpdb;
    $per_page = 20;
    $paged = isset($_GET['paged']) ? (integer)$_GET['paged'] : 1;
    $results = $wpdb->get_results("SELECT * FROM wp_customer ORDER BY id DESC LIMIT ".(($paged-1)*$per_page).",".$per_page, OBJECT);
    $total = $wpdb->get_results("SELECT count(*) as total FROM wp_customer", OBJECT)[0]->total;
    $total_page = ceil($total/$per_page);
?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Khách hàng</h1>
        <table class="wp-list-table widefat fixed striped posts">
            <thead>
            <tr>
                <th scope="col" class="manage-column">Tên khách hàng</th>
                <th scope="col" class="manage-column">Số điện thoại</th>
                <th scope="col" class="manage-column">Địa chỉ</th>
                <th scope="col" class="manage-column">Đơn hàng</th>
                <th scope="col" class="manage-column">Ghi chú</th>
                <th scope="col" class="manage-column">Thời gian đăng ký</th>
            </tr>
            </thead>

            <tbody id="the-list">
            <?php foreach ($results as $item) : $products = json_decode(stripslashes($item->product), true); ?>
                <tr id="post-1"
                    class="iedit author-self level-0 post-1 type-post status-publish format-standard hentry category-khong-phan-loai">
                    <td class="title column-title has-row-actions column-primary page-title" data-colname="Tiêu đề">
                        <div class="locked-info">
                            <span class="locked-avatar"></span>
                            <span class="locked-text"></span>
                        </div>
                        <strong>
                            <?php echo $item->name ?>
                        </strong>
                        <div class="row-actions">
                        <span class="trash">
                            <a href="<?php echo esc_attr(admin_url('admin-post.php')); ?>?action=delete_customer&customer=<?php echo $item->id ?>&back=<?php echo urlencode($actual_link) ?>"
                               class="submitdelete" aria-label="Bỏ “Chào tất cả mọi người!” vào thùng rác">Xóa khách hàng</a></span>
                        </div>
                    </td>
                    <td><?php echo $item->phone ?></td>
                    <td><?php echo $item->address ?></td>
                    <td>
                        <?php
                        foreach ($products as $product => $quantity) {
                            echo('<div><a target="_blank" href="' . get_permalink($product) . '">' . get_the_title($product) . '</a> <strong>(' . $quantity . ' người)</strong></div>');
                        }
                        ?>
                    </td>
                    <td><?php echo $item->note ?></td>
                    <td>
                        <abbr title="<?php echo $item->time ?>"><?php echo $item->time ?></abbr></td>
                </tr>
            <?php endforeach; ?>
            <?php
            if (count($results) === 0) {
                echo(
                '<tr><td colspan="5" style="text-align: center">Chưa có khách hàng nào.</td></tr>'
                );
            }
            ?>
            </tbody>

            <tfoot>

            </tfoot>
        </table>
        <div class="tablenav bottom">
            <div class="alignleft actions">
            </div>
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $total ?> mục</span>
                <span class="pagination-links">
                    <?php
                        if($paged <= 1) {
                            echo '<span class="tablenav-pages-navspan" aria-hidden="true">‹</span>';
                        } else {
                            echo ('
                                <a class="next-page" href="/wp-admin/admin.php?page=customer-list&paged='.($paged-1).'">
                                    <span aria-hidden="true">‹</span>
                                </a>                                
                            ');
                        }
                    ?>
                    <span id="table-paging" class="paging-input">
                        <span class="tablenav-paging-text">
                            Trang 1 trên <span class="total-pages"><?php echo $total_page ?></span>
                        </span>
                    </span>
                    <?php
                    if($paged == $total_page) {
                        echo '<span class="tablenav-pages-navspan" aria-hidden="true">›</span>';
                    } else {
                        echo ('
                                <a class="next-page" href="/wp-admin/admin.php?page=customer-list&paged='.($paged+1).'">
                                    <span aria-hidden="true">›</span>
                                </a>                                
                            ');
                    }
                    ?>
                </span>
            </div>
            <br class="clear">
        </div>
    </div>
    <?php
}

function delete_customer()
{
    global $wpdb;
    $customer = isset($_GET['customer']) ? $_GET['customer'] : 0;
    $back = isset($_GET['back']) ? urldecode($_GET['back']) : '/wp-admin/admin.php?page=customer-list';
    $wpdb->delete(
        'wp_customer',
        array('ID' => $customer)
    );
    wp_redirect(site_url($back));
    die;
}

add_action('admin_post_delete_customer', 'delete_customer');


function add_customer()
{
    global $wpdb;
    $data = [];
    $data['product'] = isset($_POST['product']) ? $_POST['product'] : '';
    $data['name'] = isset($_POST['name']) ? $_POST['name'] : '';
    $data['phone'] = isset($_POST['phone']) ? $_POST['phone'] : '';
    $data['address'] = isset($_POST['address']) ? $_POST['address'] : '';
    $data['note'] = isset($_POST['note']) ? $_POST['note'] : '';
    print_r($data);
    $wpdb->insert('wp_customer', $data);
    wp_redirect(site_url('/cam-on/'));
    die;
}

add_action('admin_post_nopriv_add_customer', 'add_customer');
add_action('admin_post_add_customer', 'add_customer');

